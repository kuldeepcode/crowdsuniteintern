<?php
//---------------------------------------------------
//--------------AJAX CALLPOINTS----------------------
//---------------------------------------------------

if (strstr($_SERVER['PHP_SELF'],WPA_PLUGIN_NAME) && isset($_GET['requesttobid'])):

  // check security
  check_ajax_referer( "WPA-nonce" );

	// process posted values here
	$auction_id = intval($_POST['auction_id']);
	$bidder_id = intval($_POST['bidder_id']);

  $autoapprove = get_option('wp_auctions_autoapprove');

  if ($auction_id > 0 && $bidder_id > 0) {
  
  	 // check if we already have an entry like this
     $table_name = $wpdb->prefix . "wpa_bidderrequests";
  	 $strSQL = "SELECT * FROM $table_name WHERE auction_id=$auction_id and bidder_id=$bidder_id";
  	 $row = $wpdb->get_row ($strSQL);  

     if ($row == null) {
      $valuetoassign = RTB_State::requested;
      if ($autoapprove == "on") {
        $valuetoassign = RTB_State::approved;
      }
  
      $sql = "INSERT INTO ".$table_name." (id, auction_id, bidder_id, RTB_Status) VALUES (NULL, ".$auction_id.", '".$bidder_id."',  ".$valuetoassign." );";
      $result = $wpdb->query($sql);
      
      if ($result) {
       $return = __("Access to this auction has been requested",'wpauctions');
      } else {
       $return = __("Error while requesting access",'wpauctions');      
     }
    } else {
       $return = __("Access already requested",'wpauctions');          
    }
  } else {    
     $return =  "Incorrect parameters: auction_id = $auction_id / bidder_id = $bidder_id";
  }
	
	echo $return;
	exit;
	
endif;


if (strstr($_SERVER['PHP_SELF'],WPA_PLUGIN_NAME) && isset($_GET['form_save'])):

  $form_data = isset($_POST['frmb']) ? $_POST : false;
  $form = new Formbuilder($form_data);
  $for_db = $form->get_encoded_form_array();

  $table_name = $wpdb->prefix . "wpa_auctiontemplates";

  if($for_db['form_id']){
    $sql = "UPDATE $table_name SET postdesc = '".$for_db['form_structure']."' WHERE id=" . $for_db['form_id'];
	} else {
		$sql = "INSERT INTO $table_name (id, templatename, postdesc) VALUES (NULL, 'that', '".$for_db['form_structure']."' );";
  }

  $result = $wpdb->query($sql);

endif;

if (strstr($_SERVER['PHP_SELF'],WPA_PLUGIN_NAME) && isset($_GET['form_load'])):

  $form = isset($_GET['form_id']) ? $_GET['form_id'] : false;
  if ($form) {
  	$table_name = $wpdb->prefix . "wpa_auctiontemplates";
  	$strSQL = "SELECT * FROM $table_name WHERE id=".$form;
  	$row = $wpdb->get_row ($strSQL);
  	$data = Array( 'form_id'=> $form, 'form_structure' => $row->postdesc);
  	
    $form = new Formbuilder( $data );
    $form->render_json();
  }  
  
endif;

if (strstr($_SERVER['PHP_SELF'],WPA_PLUGIN_NAME) && isset($_GET['update_price'])):

  global $wpdb;

  $options = get_option('wp_auctions');
  $currencysymbol = $options['currencysymbol'];

  $auction_id = isset($_POST['auction_id']) ? $_POST['auction_id'] : false;
  if ($auction_id) {

  	// prepare result
  	$table_name = $wpdb->prefix . "wpa_auctions";
  	$strSQL = "SELECT current_price,start_price, BIN_price FROM $table_name WHERE id=".$auction_id;
  	$row = $wpdb->get_row ($strSQL);

    if ($row != null) {
       $result = wpa_get_price($row->current_price,$row->start_price,$row->BIN_price,$currencysymbol," ");
    } else {
        $result = 0;
    }

    echo $result;
  } else {
     echo "AJAX ERROR: No auction specified";
  }  
endif;


if (strstr($_SERVER['PHP_SELF'],WPA_PLUGIN_NAME) && isset($_GET['update_bids'])):

  global $wpdb;

  $options = get_option('wp_auctions');
  $currencysymbol = $options['currencysymbol'];

  $auction_id = isset($_POST['auction_id']) ? $_POST['auction_id'] : false;
  if ($auction_id) {

    // get bids
    $table_name = $wpdb->prefix . "wpa_bids";
    $strSQL = "SELECT bidder_name, bidder_url ,date,current_bid_price, bid_type FROM $table_name WHERE auction_id=".$auction_id." ORDER BY current_bid_price DESC, bid_type";
    $rows = $wpdb->get_results ($strSQL);

    $p = "";
    foreach ($rows as $bid) {
      $p .= '<li>';
       if ($bid->bidder_url != "" && $customcontact == "URL") {
          $p .= '<a href="'.$bid->bidder_url.'" rel="nofollow">'.$bid->bidder_name.'</a>';
       } else {
          $p .= $bid->bidder_name;
       }
       $p .= ' '.__('bid','wpauctions').' '.$currencysymbol.number_format($bid->current_bid_price, 2, '.', ',').' '.__('on','wpauctions').' '.wpa_date(get_date_from_gmt($bid->date));
       if ($bid->bid_type == "auto") $p .= ' ['.__('auto','wpauctions').']';
       if ($bid->bid_type == "advance") $p .= ' ['.__('increased max bid','wpauctions').']';
       $p .= '</li>';
     }	

     $result = "<ol>".$p."</ol>";

    echo $result;
  } else {
     echo "AJAX ERROR: No auction specified";
  }  
endif;


if (strstr($_SERVER['PHP_SELF'],WPA_PLUGIN_NAME) && isset($_GET['debug'])):
   echo "<h1>WP Auctions Remote Debug Screen</h1>";
   echo "Version Number: ".$wpa_version;
   echo "<p>";

   $options = get_option('wp_auctions');
   if ($options['remotedebug'] != "" ) {   
      phpinfo();
   } else {
      echo "Remote Debug disabled - you can turn this on in your Administration console";
   }
endif;

if (strstr($_SERVER['PHP_SELF'],WPA_PLUGIN_NAME) && isset($_GET['postwatch'])):

  // check security
  check_ajax_referer( "WPA-nonce" );

	// process posted values here
	$auction_id = $_POST['auction_id'];
	$watch_email = strip_tags(stripslashes($_POST['watch_email']));

  // and to make doubly sure
  $watch_email = mysql_real_escape_string($watch_email);
  $auction_id = mysql_real_escape_string($auction_id);
 
  $table_name = $wpdb->prefix . "wpa_watchlist";
  $sql = "INSERT INTO ".$table_name." (id, auction_id, watch_email) VALUES (NULL, ".$auction_id.", '".$watch_email."' );";
  $result = $wpdb->query($sql);

  if ($result)
      _e("You will be notified of any changes to this auction",'wpauctions');
	exit;
endif;

if (strstr($_SERVER['PHP_SELF'],WPA_PLUGIN_NAME) && isset($_GET['postauction'])):

  // check security
  check_ajax_referer( "WPA-nonce" );

	// process posted values here
	$auction_id = $_POST['auction_id'];
	$bidder_name = htmlspecialchars(strip_tags(stripslashes($_POST['bidder_name'])), ENT_QUOTES);
	$bidder_email = strip_tags(stripslashes($_POST['bidder_email']));
	$bidder_url = htmlspecialchars(strip_tags(stripslashes($_POST['bidder_url'])), ENT_QUOTES);
	$max_bid = $_POST['max_bid'];
	$BIN_amount = $_POST['BIN_amount'];

   $result = wpa_process_bid( $auction_id, $bidder_name, $bidder_email, $bidder_url, $max_bid, $BIN_amount );
   
    echo $result;
	exit;
endif;

if (strstr($_SERVER['PHP_SELF'],WPA_PLUGIN_NAME) && isset($_GET['queryauction'])):

 // 0 = id
 // 1 = name
 // 2 = description
 // 3 = current_price
 // 4 = date_create
 // 5 = date_end
 // 6 = start_price 
 // 7 = image_url
 // 8 = "auction ended?"
 // 9 = winner
 // 10= winning_price
 // 11= BIN_price
 // 12= extraimage1 
 // 13= extraimage2
 // 14= extraimage3
 // 15= "next_bid"
 // 16= shipping_price
 // 17= shipping_to
 // 18= "otherimages"
 // 19= reserve_price
 // 20= engine
 // 21= shipping_from
 // 22= "auction started?"
 // 23= variable_shipping
 // 24= terms
 // 25= postdesc
 // 26= customincrement
 // 27= RTB status

	global $wpdb;
  $options = get_option('wp_auctions');
  $requesttobid = $options['requesttobid'];
  
  // thumbnail size is set here
  $thumbnail_size = 50;
  $image_size = 300;
  $image_height = 250;
 
	function fail($s) { header('HTTP/1.0 406 Not Acceptable'); die($s);}

  // check security
  check_ajax_referer( "WPA-nonce" );

	// process query string here
	$auction_id = $_POST['auction_ID'];

	// validate input
	if (!is_numeric($auction_id)) // ID not numeric
		fail(__('Invalid Auction ID specified','wpauctions'));
		
    // confirm if auction has ended or not
    check_auction_end($auction_id);

  	// prepare result
  	$table_name = $wpdb->prefix . "wpa_auctions";
  	$strSQL = "SELECT id, name,description,current_price,date_create,date_end,start_price,image_url, '".current_time('mysql',"1")."' < date_end, winner, winning_price, BIN_price, extraimage1, extraimage2, extraimage3, 0.00 as 'next_bid', shipping_price, shipping_to, 'placeholder' as 'otherimages', reserve_price, engine, shipping_from, '".current_time('mysql',"1")."' > date_start, variable_shipping, terms, postdesc,customincrement, 0 as RTB_status FROM $table_name WHERE id=".$auction_id;
  	$rows = $wpdb->get_row ($strSQL, ARRAY_N);

  	// send back result
    if (!($rows)) // no records found
       fail(__('Cannot locate auction','wpauctions'));

    // handle template fields
    if ($rows[25] != "") {
      $template = unserialize($rows[25]);
      $strTemplateData = $template["data"];
      
      if (is_array($strTemplateData)) {
        foreach ($strTemplateData as $key => $value)
        {
           // strip out _s
           $newkey = str_replace("_", " ", $key);
           $rows[2] .= "<p class='wpa-description-template'><strong>$newkey :</strong> $value</p>";
        }
      }
    }

    // pass image through resizer    
    $temp = $rows[7];
    $rows[7] = wpa_resize ($rows[7],$image_size,$image_height);
    
    $rows[18] = "";
    // other images could be blank .. in which case, don't resize
    if ($rows[12] != "") {
       $rows[18] = $rows[18].'^'.wpa_resize ($rows[12],$thumbnail_size);
       $rows[12] = wpa_resize ($rows[12],$image_size,$image_height);
    }
    if ($rows[13] != "") {
       $rows[18] = $rows[18].'^'.wpa_resize ($rows[13],$thumbnail_size);
       $rows[13] = wpa_resize ($rows[13],$image_size,$image_height);
    }       
    if ($rows[14] != "") { 
       $rows[18] = $rows[18].'^'.wpa_resize ($rows[14],$thumbnail_size);
       $rows[14] = wpa_resize ($rows[14],$image_size,$image_height);
    }
           
    // v4 - always append    
    $rows[18] = $rows[18] . '^'.wpa_resize ($temp,$thumbnail_size);    
        
    // normalise dates
    $rows[4] = date('dS M Y h:i A',strtotime(get_date_from_gmt($rows[4])));
    $rows[5] = date('dS M Y h:i A',strtotime(get_date_from_gmt($rows[5])));

    // insert next increment if not starting price
    if ($rows[20] == 3) { // handle reverse 
      if ( $rows[3] > 0 ) {
         $rows[15] = number_format($rows[3] - wpa_get_increment($rows[3], $rows[26]), 2, '.', ',');
      } else {
         $rows[15] = $rows[6];
      }
    } else {
      if ($rows[3] >= $rows[6]) {
         $rows[15] = number_format($rows[3] + wpa_get_increment($rows[3], $rows[26]), 2, '.', ',');
      } else {
         $rows[15] = $rows[6];
      }    
    }
  
    // process RTB - we only need to do this if RTB is on
    if ($requesttobid == "Yes") {
    	    // check if we have a request
	     if (is_user_logged_in()) {
	        global $current_user;
	        get_currentuserinfo();
	     
	        $bidder_id = $current_user->ID;
	      } else {
	         $bidder_id = 0;
	      }
	      
	      $table_name = $wpdb->prefix . "wpa_bidderrequests";
        $strSQL = "SELECT * FROM $table_name WHERE auction_id=".$auction_id." AND bidder_id=".$bidder_id;
        $row = $wpdb->get_row ($strSQL);

        if ($row != null) {
           $rows[27] = $row->RTB_Status;
        }
    } 
  
	// prepare results   	
  $result_set = implode("|", $rows);  
        	
    echo $result_set;
	exit;
endif;

if (strstr($_SERVER['PHP_SELF'],WPA_PLUGIN_NAME) && isset($_GET['querybids'])):

	global $wpdb;

	function fail($s) { header('HTTP/1.0 406 Not Acceptable'); die($s);}

  // check security
  check_ajax_referer( "WPA-nonce" );

	// process query string here
	$auction_id = $_POST['auction_ID'];
	$engine = $_POST['engine'];

	// validate input
	if (!is_numeric($auction_id)) // ID not numeric
		fail(__('Invalid Auction ID specified','wpauctions'));
		
	// prepare result
	$table_name = $wpdb->prefix . "wpa_bids";
	
  if ($engine != REVERSE_ENGINE) {
	   $strSQL = "SELECT bidder_name, bidder_url ,date,current_bid_price, bid_type FROM $table_name WHERE auction_id=".$auction_id." ORDER BY current_bid_price DESC, bid_type";
	} else {
	   $strSQL = "SELECT bidder_name, bidder_url ,date,current_bid_price, bid_type FROM $table_name WHERE auction_id=".$auction_id." ORDER BY current_bid_price, bid_type";	
	}
	$rows = $wpdb->get_results ($strSQL, ARRAY_N);

	// send back result
    if (!($rows)) // no records found
       $result_set="";
    else {
//       foreach ($rows as &$row) {
//          $row[2] = date('dS M Y h:i A',strtotime(get_date_from_gmt($row[2]))); // convert dates to WP timezone
//       }

// change above code as it didn't work in PHP 4

         foreach($rows as $i=>$row){
            $row[2] = wpa_date(strtotime(get_date_from_gmt($row[2]))); // convert dates to WP timezone
            // replace the row in the table
            $rows[$i]=$row;
         }
       $result_set = wpa_implode_r("|",$rows);
    }
      	
    echo $result_set;
	exit;
endif;


if (strstr($_SERVER['PHP_SELF'],WPA_PLUGIN_NAME) && isset($_GET['queryother'])):

	global $wpdb;

	function fail($s) { header('HTTP/1.0 406 Not Acceptable'); die($s);}

  // check security
  check_ajax_referer( "WPA-nonce" );

	// process query string here
	$auction_id = $_POST['auction_ID'];

	// validate input
	if (!is_numeric($auction_id)) // ID not numeric
		fail(__('Invalid Auction ID specified','wpauctions'));
		
	// prepare result
	$table_name = $wpdb->prefix . "wpa_auctions";
	$strSQL = "SELECT id,name,image_url,current_price,start_price,0.00 as 'next_bid', BIN_price FROM $table_name WHERE id <> ".$auction_id." AND '".current_time('mysql',"1")."' < date_end AND '".current_time('mysql',"1")."' > date_start ORDER BY RAND() LIMIT 4";
	$rows = $wpdb->get_results ($strSQL, ARRAY_N);

      foreach($rows as $i=>$row){

        $row[2] = wpa_resize($row[2],50);

            // insert current price
           if ($row[3] >= $row[4]) {
              $row[5] = $row[3];
           } else {
              $row[5] = $row[4];
           }            

         // replace the row in the table
         $rows[$i]=$row;
      }

	// send back result
    if (!($rows)) // no records found
       $result_set="";
    else
       $result_set = wpa_implode_r("|",$rows);
      	
    echo $result_set;
	exit;
endif;
?>