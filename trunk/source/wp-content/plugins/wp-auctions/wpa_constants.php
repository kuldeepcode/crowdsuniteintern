<?php
// Plugin Constants
if (!defined('WPA_PLUGIN_NAME')) {
 
  define ('WPA_PLUGIN_NAME', trim(dirname(plugin_basename(__FILE__)),'/'));
  define ('WPA_PLUGIN_DIR', dirname( plugin_basename( __FILE__ ) ));
  define ('WPA_PLUGIN_URL', plugins_url() . '/' . WPA_PLUGIN_NAME);
   
  define ('WPA_PLUGIN_FILE', 'wp_auctions.php');
  define ('WPA_PLUGIN_FULL_PATH', WPA_PLUGIN_URL . "/" . WPA_PLUGIN_FILE );
  define ('WPA_PLUGIN_RSS', WPA_PLUGIN_FULL_PATH . "?rss" );
  define ('WPA_PLUGIN_STYLE', WPA_PLUGIN_URL . "/style" );

  define('POPUP_SIZE', "&height=549&width=651&modal=true");

  // Auction engine types
  define('STANDARD_ENGINE', 1);
  define('SIMPLE_ENGINE', 2);
  define('REVERSE_ENGINE', 3);

  $BID_WIN         = __('Congratulations, you are the highest bidder on this item.','wpauctions' );
  $BID_LOSE        = __( "I'm sorry, but a preceeding bidder has outbid you.",'wpauctions' );
  $BIN_WIN         = __( "Thanks for buying! Payment instructions have been emailed.",'wpauctions' );
  $BID_WIN_REVERSE = __('Congratulations, you are the lowest bidder on this item.','wpauctions' );
  
class RTB_State
{
    const notassigned = 0;
    const requested = 1;
    const approved = 2;
    const denied = 3;
}
  
  
}

?>