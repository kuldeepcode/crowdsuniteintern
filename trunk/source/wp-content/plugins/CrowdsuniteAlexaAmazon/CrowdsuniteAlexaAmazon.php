<?php
/*
Plugin Name: CrowdsuniteAlexaAmazon
Description: CrowdsuniteAlexaAmazon
Version: 0.0.1
Author: Vasilega Ivan
*/
if (!class_exists("CrowdsuniteAlexaAmazon")) {
	class CrowdsuniteAlexaAmazon {

		protected static $ActionName        = 'UrlInfo';
		protected static $ResponseGroupName = 'Rank';
		protected static $ServiceHost      = 'awis.amazonaws.com';
		protected static $NumReturn         = 10;
		protected static $StartNum          = 1;
		protected static $SigVersion        = '2';
		protected static $HashAlgorithm     = 'HmacSHA256';
		protected $accessKeyId;
		protected $secretAccessKey;
		protected $site;

		static $urlInfo;

			 function cron_add_min( $schedules ) {
			 	// Adds once weekly to the existing schedules.
			 	$schedules['AlexaAmazonMonth'] = array(
			 		'interval' => 2678400,
			 		'display' => __( 'AlexaAmazonMonth' )
			 	);
			 	return $schedules;
			 }

		public function __construct() {

			if ( is_admin() ){
				add_action( 'admin_menu', array( &$this, 'AlexaAmazon_add_menu' ) );
				register_activation_hook(__FILE__,array(&$this, 'activate'));
			}
			add_filter( 'cron_schedules', array( &$this, 'cron_add_min') );
			if ( ! wp_next_scheduled( 'AlexaAmazonCron_hook' ) ) {
				wp_schedule_event( time(), 'AlexaAmazonMonth', 'AlexaAmazonCron_hook' );
			}

			add_action( 'AlexaAmazonCron_hook', array( &$this, 'AlexaAmazon_request_' ) );

		}
		
		public function activate () {}

		public function AlexaAmazon_add_menu() {
			add_menu_page('Alexa Amazon', 'Alexa Amazon', 'manage_options', 'AlexaAmazon', array( $this, 'AlexaAmazonBox' ));		
		}

		public function AlexaAmazonBox() {
			if ( $_POST ) {

				if ( $_POST ['accessKeyId'] ) {
					update_option( 'AlexaAmazon_accessKeyId', $_POST ['accessKeyId'] );
				}
				if ( $_POST ['secretAccessKey'] ) {
					update_option( 'AlexaAmazon_secretAccessKey', $_POST ['secretAccessKey'] );
				}
			}
			include plugin_dir_path( __FILE__ ) . '/template/settings.php';
		}

		public function AlexaAmazon_request_(){
			if ( preg_match( '|'.$_SERVER['HTTP_HOST'].'|', 'http://crowdsunite.com/' ) ) {
				$products = get_posts( array( 'post_type' => 'product', 'posts_per_page' => -1, 'post_status' => 'publish', ) );
				foreach ($products as $key => $product) {
					$tgt_product_url = get_post_meta( $product->ID,'tgt_product_url', true );
					if ( !empty( $tgt_product_url ) ) {
						$response = $this->getUrlInfo( $tgt_product_url );
						if ( empty( $response ) || empty( $response[0]['Rank'] )){
							delete_post_meta( $product->ID, 'CrowdsuniteAlexaAmazonRank' );
						} else {
							update_post_meta( $product->ID, 'CrowdsuniteAlexaAmazonRank', $response[0]['Rank'] );
						}
					} else {
						delete_post_meta( $product->ID, 'CrowdsuniteAlexaAmazonRank' );
					}
				}
				$AlexaAmazon_request_count = get_option('AlexaAmazon_request_count') + 1;
				update_option( 'AlexaAmazon_request_count', $AlexaAmazon_request_count );
				update_option( 'AlexaAmazon_last_request', time() );
			}
		}
		
		/**
		 * Get site info from AWIS.
		 */ 
		public function getUrlInfo( $site ) {
			$this->accessKeyId = get_option('AlexaAmazon_accessKeyId');
			$this->secretAccessKey = get_option('AlexaAmazon_secretAccessKey');
			$this->site = $site;
			if ( !empty($this->accessKeyId) && !empty($this->secretAccessKey) && !empty($this->site) ) {
				$queryParams = $this->buildQueryParams();
				$sig = $this->generateSignature($queryParams);
				$url = 'http://'.self::$ServiceHost.'/?'.$queryParams.'&Signature='.$sig;
				$ret = self::makeRequest($url);
				$result =  self::parseResponse($ret);
			} else {
				$result = false;
			}
			return $result;
		}

		/**
		 * Builds current ISO8601 timestamp.
		 */
		protected static function getTimestamp() {
			return gmdate("Y-m-d\TH:i:s.\\0\\0\\0\\Z", time()); 
		}

		/**
		 * Builds query parameters for the request to AWIS.
		 * Parameter names will be in alphabetical order and
		 * parameter values will be urlencoded per RFC 3986.
		 * @return String query parameters for the request
		 */
		protected function buildQueryParams() {
			$params = array(
				'Action'			=> self::$ActionName,
				'AWSAccessKeyId'	=> $this->accessKeyId,
				'Timestamp'		 => self::getTimestamp(),
				'SignatureVersion'  => self::$SigVersion,
				'SignatureMethod'   => self::$HashAlgorithm,
			);
			if ( is_array( $this->site ) ) {
				$params[ self::$ActionName.'.Shared.ResponseGroup' ] = self::$ResponseGroupName;

				$i = 1;
				foreach ($this->site as $url) {
					$params[ self::$ActionName.'.'.( $i ).'.'.'Url' ] = $url;
					++$i;
				}
			} else {
				$params['ResponseGroup'] = self::$ResponseGroupName;
				$params['Url'] = $this->site;
			}
			ksort($params);
			$keyvalue = array();
			foreach($params as $k => $v) {
				$keyvalue[] = $k . '=' . rawurlencode($v);
			}
			return implode('&',$keyvalue);
		}

		/**
		 * Makes request to AWIS
		 * @param String $url   URL to make request to
		 * @return String	   Result of request
		 */
		protected static function makeRequest($url) {
			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_TIMEOUT, 4);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$result = curl_exec($ch);
			curl_close($ch);
			return $result;
		}

		/**
		 * Parses XML response from AWIS and displays selected data
		 * @param String $response	xml response from AWIS
		 */
		public static function parseResponse($response) {
			$xml = new SimpleXMLElement( $response, null, false, 'http://awis.amazonaws.com/doc/2005-07-11' );
			if( $xml->count() && $xml->Response->UrlInfoResult->Alexa->count() ) {
				$i = 0;
				while ( isset( $xml->Response[$i] ) ) {
					$info = $xml->Response[$i]->UrlInfoResult->Alexa;
					$nice_array[$i]['Rank'] = (int)$info->TrafficData->Rank;
					$nice_array[$i]['DataUrl'] = (string)$info->TrafficData->DataUrl;
					++$i;
				}
			} else {
				$nice_array = array();
			}

			return $nice_array;
		}

		/**
		 * Generates an HMAC signature per RFC 2104.
		 *
		 * @param String $url	   URL to use in createing signature
		 */
		protected function generateSignature($url) {
			$sign = "GET\n" . strtolower(self::$ServiceHost) . "\n/\n". $url;
			$sig = base64_encode(hash_hmac('sha256', $sign, $this->secretAccessKey, true));
			return rawurlencode($sig);
		}
	}

	$CrowdsuniteAlexaAmazon = new CrowdsuniteAlexaAmazon();
}