<form action="" method="post">
    <?php wp_nonce_field( $namespace . "_options", $namespace . '_update_wpnonce' ); 
    $args_posts = array(
	    'posts_per_page'  => 100,
	    'offset'          => 0,
	    'orderby'         => 'title',
	    'order'           => 'ASC',
	    'post_type'       => 'post',
	    'post_status'     => 'publish',
	    'suppress_filters' => true );
    ?>
    <input type="hidden" name="form_action" value="update_options" />
    <div class="wrap" id="notifysnack_container">
		<img class="not_title" src="<?php echo $this->url_path; ?>/images/basic.png" />
        <h2><?php echo $page_title; ?></h2>

		<div class="clear"></div>
        <div>
            <h3 class="title">Embed code:</h3>
			<textarea rows="10" cols="70" name="notifyscript"><?php echo $notifyscript; ?></textarea>
            <p style="font-style: italic; margin: 0px;">Grab your notification code from <a href="http://www.notifysnack.com">www.notifysnack.com</a> and paste it above.</p>

			<h3>Insert embed code in:</h3>
			<input id="notify_header" type="radio" name="notifyscript_location" value="header" <?php echo $notifyscript_location == 'header' ? ' checked="checked"' : ''; ?> />
			<label for="notify_header">Header (before &lt;/head&gt;)</label>
			<br />
			<input id="notify_footer" type="radio" name="notifyscript_location" value="footer" <?php echo $notifyscript_location == 'footer' ? ' checked="checked"' : ''; ?> />
			<label for="notify_footer">Footer (before &lt;/body&gt;)</label>
			<br />
			
			<h3>Show notification on:</h3>
			<input id="notify_showon_all" type="radio" name="notifyscript_showon" value="all" <?php echo $notifyscript_showon == 'all' ? ' checked="checked"' : ''; ?> />
			<label for="notify_showon_all">On all pages and posts</label>
			<br />
			<input id="notify_showon_custom" type="radio" name="notifyscript_showon" value="custom" <?php echo $notifyscript_showon == 'custom' ? ' checked="checked"' : ''; ?> />
			<label for="notify_showon_custom">Only on selected pages and posts</label>
			<br />
			
			<div id="custom_page_checkboxes" <?php echo $notifyscript_showon == 'all' ? ' style="display:none"' : ''; ?> >
				<input id="notify_all_pages" type="checkbox" name="notifyscript_allpages" value="true" <?php echo $notifyscript_allpages == 'true' ? ' checked="checked"' : ''; ?> />
				<label for="notify_all_pages">All pages</label>
				<br />
				<?php foreach ( $pages as $page ) { ?>
					<input id="notify_page_<?php echo $page->ID; ?>" type="checkbox" class="pages_checkbox" name="notifyscript_pages[]" value="<?php echo $page->ID; ?>" <?php if($this->isChecked($notifyscript_pages, $page->ID) || $notifyscript_allpages == 'true') echo 'checked="checked"'; ?> />
					<label for="notify_page_<?php echo $page->ID; ?>">Page: <?php echo $page->post_title; ?></label>
					<br />
				<?php }	?>
			</div>
			
			<div id="custom_cat_checkboxes" <?php echo $notifyscript_showon == 'all' ? ' style="display:none"' : ''; ?> >
				<input id="notify_all_cats" type="checkbox" name="notifyscript_allcats" value="true" <?php echo $notifyscript_allcats == 'true' ? ' checked="checked"' : ''; ?> />
				<label for="notify_all_cats">All posts</label>
				<br />
				<?php foreach ( $categories as $cat ) { ?>
					<input id="notify_cat_<?php echo $cat->term_id; ?>" type="checkbox" class="cats_checkbox" name="notifyscript_cats[]" value="<?php echo $cat->term_id; ?>" <?php if($this->isChecked($notifyscript_cats, $cat->term_id) || $notifyscript_allcats == 'true') echo 'checked="checked"'; ?> />
					<label for="notify_cat_<?php echo $cat->term_id; ?>">Category: <?php echo $cat->name; ?></label><div class="arrows rotated" id="<?php echo $cat->term_id; ?>" rel="posts_from_<?php echo $cat->term_id; ?>"></div>
					<div id="posts_from_<?php echo $cat->term_id; ?>"> 						
					<?php 
						$args_posts['category'] = $cat->term_id;
						$posts_array = get_posts( $args_posts );
						foreach($posts_array as $p) { ?>
							<input id="notify_post_<?php echo $p->ID; ?>" type="checkbox" class="posts_checkbox" name="notifyscript_posts[]" value="<?php echo $p->ID; ?>" <?php if($this->isChecked($notifyscript_posts, $p->ID) || $this->isChecked($notifyscript_cats, $cat->term_id)) echo 'checked="checked"'; ?> />
							<label for="notify_post_<?php echo $p->ID; ?>">Post: <?php echo $p->post_title; ?></label>					
							<br />
					<?php } ?>
					</div>
					<br />
				<?php }	?>
			</div>
			<div id="notify_save">
				<input type="submit" name="Submit" class="button-primary" value="<?php esc_attr_e('Save Changes') ?>" />
			</div>
        </div>
    </div>
</form>