<?php
/*
Plugin Name: CrowdsuniteRatingAPI
Description: CrowdsuniteRatingAPI
Version: 0.0.1
Author: Vasilega Ivan
*/

include_once (dirname(__FILE__)."/classes/crowdsunite_make_table.class.php");

class CrowdsuniteRatingAPI{
	public function __construct() {
		add_action( 'wp_ajax_apiRating', array( &$this, 'apiRating' ) );
		add_action( 'wp_ajax_nopriv_apiRating', array( &$this, 'apiRating' ) );
		if ( is_admin() ){
			add_action( 'admin_menu', array( &$this, 'apiRating_add_menu' ) );
			register_activation_hook(__FILE__,array(&$this, 'activate'));
		}
	}
	
	public function activate () {
		global $wpdb;
		$wpdb->query("
		CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}api_rating_referer` (
			`id` bigint(20) NOT NULL AUTO_INCREMENT,
			`HTTP_REFERER` varchar(255) NOT NULL,
			`user_id` bigint(20) NOT NULL DEFAULT '0',
			`product_id` bigint(20) NOT NULL DEFAULT '0',
			`metod` varchar(10) NOT NULL,
			`date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
			PRIMARY KEY (`id`),
			KEY `HTTP_REFERER` (`HTTP_REFERER`)
		) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;");
	}

	public function apiRating_add_menu() {
		add_menu_page('Rating Api', 'Rating Api', 'manage_options', 'ratingApi', array( $this, 'ratingApiBox' ));		
	}

	public function ratingApiBox() {
		global $wpdb;

		$columns = array(
			'user_id'=>__('User'),
			'product_id' => __('Product'),
			'HTTP_REFERER'=>__('Url referer'),
			'rating_type' => 'Rating type',
			'rating_value' => 'Rating value',
			'reviews_amount' => 'Reviews amount',
			'metod'=>'Metod',
			'count'=>'Number used'
			);
		$sortable_columns = array(
			'count'=>array('count')
			);

		$query ="SELECT *, COUNT(HTTP_REFERER) as count FROM {$wpdb->prefix}api_rating_referer GROUP BY HTTP_REFERER,user_id,product_id,metod,rating_type,rating_value,reviews_amount";

		$wp_list_table = new credits_make_table($columns, $sortable_columns, 40, $query, true, 'date', 'DESC');
		$wp_list_table->prepare_items();

		include plugin_dir_path( __FILE__ ) . '/template/ratingApiBox.php';

	}
	public function apiRating() {

		$metod = isset( $_REQUEST['m'] )?trim( $_REQUEST['m'] ):'';

		$url = isset( $_SERVER['HTTP_REFERER'] )?$_SERVER['HTTP_REFERER']:'';

		$d = isset( $_REQUEST['d'] )?$_REQUEST['d']:0;

		$user = false;
		if ( isset( $_REQUEST['api_key'] ) ) {
			$user_id = base64_decode( urldecode( $_REQUEST['api_key'] ) );

			if ( (int)$user_id == $user_id ) {
				if ( $user_id == 0 ) {
					$user = get_user_by( 'login', 'Crowdsunite' );
					$user_id = $user->id;
				} else {
					$user = get_userdata( $user_id );
				}
			}
		}

		if ( empty( $url ) || empty( $user ) ) {
			die;
		} else {

		}

		switch ( (string)$metod ) {

			case '1':
				$images_dir_uri = plugins_url( 'images/' , __FILE__ );
				header('Content-type: text/javascript');
				include plugin_dir_path( __FILE__ ) . 'template/scriptApiRating.php';
				break;

			case '2':

				header('Content-type: text/javascript');
				extract($_REQUEST);
				
				$value_for_insert = array();
				$flag_url = ( preg_match( '|^'.get_bloginfo('url').'|', $url ) )?false:true;

				$response = array();
				$product_arr = array();
				$arrayOptinType = array('r_4','r_5','r_6');

				foreach( $i as $key => $value){

					if ( isset( $i[ $key ] ) && isset( $o[ $key ] ) ) {

						$product_id = (int)$i[ $key ];
						$product = get_post( $product_id );

						if ( empty( $product ) || $product->post_status !== 'publish' || $product->post_type !== 'product' ) {
							continue;
						}

						$result_ar = array();

						$option = base_convert( (int)$o[$key], 10, 2 );

						if ( strlen( $option ) >=  2 ) $l = 2; else $l = 1;
						$option_type = (int)base_convert( substr( $option, - $l ), 2, 10 ) - 1;

						if ( $option_type !== -1 ) {

							if ( !is_array( $sum_ar = get_post_meta( $product_id, 'tgt_review_rating_average',true ) ) ) {
								$sum_ar = update_post_rating_average( $product_id, true );
							}

							$round_ = $sum_ar[ $arrayOptinType[ $option_type ] ];

							$ratings = get_option('tgt_rating');
							$result_ar['rating_types'] = 'for '.$ratings[ $arrayOptinType[ $option_type ] ];

						} else {

							$round_ = get_post_meta( $product_id, PRODUCT_RATING, true );

						}

						$result_ar['rating'] = $round = intval( round ( $round_, 0 ) );
						$result_ar['url_prod'] = get_permalink( $product_id );

						if ( strlen( $option ) >=  3 && substr( $option, -3, 1 ) === '1') {
							$result_ar['text_rating'] = sprintf( "%01.1f", $round_ );
							$display_text_rating = 1;
						} else 
							$display_text_rating = 0;

						if ( strlen( $option ) >=  4 && substr( $option, -4, 1 ) === '1') {
							$result_ar['rating_count'] = (int)get_post_meta( $product_id, PRODUCT_RATING_COUNT, true );
							$result_ar['post_title'] = $product->post_title;
							$display_text_reviews = 1;
						} else 
							$display_text_reviews = 0;

						$response[$key] = $result_ar;
						if ( !in_array( $product_id, $product_arr ) ) $product_arr[] = $product_id;

						if ( $flag_url ) {
							$value_for_insert[] = "( '{$url}', '{$user_id}', '{$product_id}', 'js', '{$arrayOptinType[ $option_type ]}', '{$display_text_rating}', '{$display_text_reviews}' )";

						}
					}
				}
				if ( !empty( $value_for_insert ) && $flag_url && !$d ) {

					global $wpdb;

					/*for statistic*/
					$key_for_insert = '(`HTTP_REFERER`, `user_id`, `product_id`, `metod`, `rating_type`, `rating_value`, `reviews_amount`)';
					$value_for_insert = implode( ',' , $value_for_insert );
					$wpdb->query("INSERT INTO {$wpdb->prefix}api_rating_referer {$key_for_insert} VALUE {$value_for_insert}");

					/*for article*/
					$attr = array( 
						'post_type' => 'article',
						'posts_per_page' => 1,
						'post_status' => 'any',
						'meta_key' => 'AUTO_CREATE_ARTICLE_HTTP_REFERER',
						'meta_value' => $url
						);
					$article = get_posts( $attr );

					if ( empty( $article ) ){

						$title = $this->getTitle( $url );
						$tags = get_meta_tags( $url );
						$description = ' <a href="'.$url.'" target="_blank">Read More...</a>';
						if ( isset( $tags['description'] ) ) {
							$description = $tags['description'].$description;
						} else {
							foreach ($tags as $name => $content) {
								if ( strpos( $name, 'description' ) ) {
									$description = $content.$description;
									break;
								}
							}
						}

						$category = get_the_category($product_id);
						$post = array(
							'post_type' => 'article',
							'post_author' => $user_id,
							'post_title' => $title,
							'post_content' => $description,
							'post_category' => array($category[0]->cat_ID),
							'post_status' => 'pending'
							);

						if ( $article_id = wp_insert_post( $post ) ) {
							update_post_meta( $article_id,'AUTO_CREATE_ARTICLE_HTTP_REFERER', $url );
						}

					} else {
						$article_id = $article[0]->ID;
					}
					$product_arr_ = implode( ',', $product_arr );
					$product_id_selected = $wpdb->get_results("SELECT group_concat(`product_id`) as product_id_selected FROM {$wpdb->prefix}ref_product_article where article_id = {$article_id}  AND product_id in ({$product_arr_}) GROUP BY article_id;");
					$product_id_selected = ( empty( $product_id_selected ) )?array():explode(',', $product_id_selected[0]->product_id_selected );
					if ( count( $product_arr ) != count( $product_id_selected ) ){
						$value_for_insert = array();
						foreach ( $product_arr as $product_id_ ) {
							if ( in_array( $product_id_, $product_id_selected ) ) continue;
							$value_for_insert[] = "({$product_id_},{$article_id})";
							tgt_mailing_new_article_notif( $product_id_, $article_id );
						}

						if ( !empty( $value_for_insert ) ) {
							$key_for_insert = '(product_id,article_id)';
							$value_for_insert = implode( ',' , $value_for_insert );
							$wpdb->query("INSERT INTO {$wpdb->prefix}ref_product_article {$key_for_insert} VALUE {$value_for_insert}");
						}
					}
					
				}

				echo $_REQUEST["jsonp"]."(" . json_encode($response) . ");";
				break;
		}
		die;
	}

	static function ZeroClipboard( $enqueue_script = true ){
		if ( $enqueue_script ) {
			wp_register_script( 'zeroclipboard', plugins_url( 'lib/ZeroClipboard/ZeroClipboard.min.js' , __FILE__ ) );
			wp_enqueue_script( 'zeroclipboard' );
		}
		return str_replace( get_bloginfo('url'), '', plugins_url( 'lib/ZeroClipboard/ZeroClipboard.swf' , __FILE__ ) ) ;
	}

	public function getTitle($Url){
		$str = file_get_contents($Url);
		if(strlen($str)>0){
			preg_match("/\<title\>(.*)\<\/title\>/",$str,$title);
			return ( isset( $title[1] ) )?$title[1]:'No name';
		}
	}

	private function apiRating_old() {

		global $wpdb;

		$images_dir_uri = plugins_url( 'images/' , __FILE__ );

		$url = isset( $_SERVER['HTTP_REFERER'] )?$_SERVER['HTTP_REFERER']:'';
		$product_id = isset( $_REQUEST['product_id'] )?$_REQUEST['product_id']:'';
		$metod = isset( $_REQUEST['metod'] )?$_REQUEST['metod']:'';
		$rating_types = isset( $_REQUEST['rating_types'] )?$_REQUEST['rating_types']:0;
		$display_text_rating = isset( $_REQUEST['display_text_rating'] )?$_REQUEST['display_text_rating']:0;
		$display_text_reviews = isset( $_REQUEST['display_text_reviews'] )?$_REQUEST['display_text_reviews']:0;
		$display_text_rating_types = ( $rating_types != '0' )?1:0;
		$d = isset( $_REQUEST['d'] )?$_REQUEST['d']:0;

		$titles = array(
				__("There is no user's rating yet",'re'),
				__('Abysmal','re'),
				__('Terrible','re'),
				__('Poor','re'),
				__('Mediocre','re'),
				__('OK','re'),
				__('Good','re'),
				__('Very Good','re'),
				__('Excellent','re'),
				__('Outstanding','re'),
				__('Spectacular','re'));

		if ( isset( $_REQUEST['api_key'] ) ) {
			$user_id = (int)base64_decode( urldecode( $_REQUEST['api_key'] ) );
			$user = get_userdata( $user_id );
		} else
			$user = false;

		if ( empty( $url ) || empty( $metod ) || empty( $user ) ) {
			die;
		}
			
		if ( $metod !== "js" ) {

			$product = get_post( $product_id );
			
			if ( empty( $product ) || $product->post_status !== 'publish' || $product->post_type !== 'product' ) {
				die;
			}

			if ( $display_text_reviews == 1 ) {

				$product_rating_count = (int)get_post_meta( $product_id, PRODUCT_RATING_COUNT, true );
			}

			switch ( (string)$rating_types ) {
				case 'r_4':
				case 'r_5':
				case 'r_6':
					if ( !is_array( $sum_ar = get_post_meta( $product_id, 'tgt_review_rating_average',true ) ) ) {
						$sum_ar = update_post_rating_average( $product_id, true );
					}
					$round_ = $sum_ar[ $rating_types ];

					$ratings = get_option('tgt_rating');
					$text_rating_types = 'for '.$ratings[ $rating_types ];

					break;
				default:
					$round_ = get_post_meta( $product_id, PRODUCT_RATING, true );
					break;
			}
			$round = intval( round ( $round_, 0 ) );
		}

		switch ( $metod ) {
			case 'js':
			/*<script type="text/javascript" src="http://crowdsunite.loc/wp-admin/admin-ajax.php?action=apiRating&product_id=52&metod=js&api_key=MQ%3D%3D"></script> <div class="crowdsunite_rating_product_52"></div>*/

			header('Content-type: text/javascript');
			$first_step = true;

			$product_id_arr = explode(':',$product_id);

			foreach ( $product_id_arr as $product_id ):
				$product = get_post( $product_id );
				if ( empty( $product ) || $product->post_status !== 'publish' || $product->post_type !== 'product' ) {
					continue;
				}

				if ( $display_text_reviews == 1 ) {
					$product_rating_count = (int)get_post_meta( $product_id, PRODUCT_RATING_COUNT, true );
				}

				switch ( (string)$rating_types ) {
					case 'r_4':
					case 'r_5':
					case 'r_6':
						if ( !is_array( $sum_ar = get_post_meta( $product_id, 'tgt_review_rating_average',true ) ) ) {
							$sum_ar = update_post_rating_average( $product_id, true );
						}
						$round_ = $sum_ar[ $rating_types ];

						$ratings = get_option('tgt_rating');
						$text_rating_types = 'for '.$ratings[ $rating_types ];

						break;
					default:
						$round_ = get_post_meta( $product_id, PRODUCT_RATING, true );
						break;
				}
				$round = intval( round ( $round_, 0 ) );
?>
<?php if ( empty( $d ) && $first_step ): ?>
<?php $index_ = implode( '_' , $product_id_arr );?>
				if ( !api_crowdsunite_rating_<?php echo $index_;?> ){

				var api_crowdsunite_rating_<?php echo $index_;?> = 1;

				if( window.onload ) {
					api_crowdsunite_rating_<?php echo $index_;?> = window.onload;
				}

				window.onload = function () {
					if( api_crowdsunite_rating_<?php echo $index_;?> && api_crowdsunite_rating_<?php echo $index_;?> !== 1 ) {
						api_crowdsunite_rating_<?php echo $index_;?>();
					}
<?php endif;?>
					var div_gen_arr = document.getElementsByClassName('crowdsunite_rating_product_<?php echo $product_id;?>');

					if ( div_gen_arr.length > 0 ) {
						var i = 0;
						while ( div_gen_arr[i] ) {
							if ( div_gen_arr[i].innerHTML == '' ) {
								var div_gen = div_gen_arr[i];

							div_gen.setAttribute('style','line-height: 1px;display: inline-block;text-align: center;padding: 5px;');

							var br = document.createElement('br');
							var br1 = document.createElement('br');

							var a1 = document.createElement('a');
							a1.setAttribute('href','<?php echo get_permalink( $product_id );?>');
							a1.setAttribute('style','line-height: 1;display: inline-block;font-size: 20px;text-decoration: none;font-weight: bold;font-style: italic;font-family: Arial;color: #ffc948;');
							a1.setAttribute('target','_blank');

<?php if ( $display_text_rating == 1 ):?>
							a1.innerHTML = '<?php printf( "%01.1f", $round_ );?>/10 ';
<?php endif;?>

							var div1 = document.createElement('div');
							div1.setAttribute('style','display: inline-block;background: url(<?php echo $images_dir_uri;?>star.gif) repeat-x 0px 0px transparent;height: 16px;width: 80px;');
							div1.setAttribute('title','<?php echo $titles[$round];?>');

							var div2 = document.createElement('div');
							div2.setAttribute('style','width: <?php echo $round*10 ?>%;background:url(<?php echo $images_dir_uri;?>star.gif) repeat-x 0px -32px transparent;height: 100%;');

							div1.appendChild(div2);
							a1.appendChild(div1);
							div_gen.appendChild( a1 );

<?php if ( $display_text_reviews == 1 ):?>
							var a3 = document.createElement('a');
							a3.setAttribute('href','<?php echo get_permalink( $product_id );?>#tc_review');
							a3.setAttribute('style','line-height: 1;display: inline-block;font-size: 11px;text-decoration: none;font-weight: normal;font-style: italic;font-family: Arial;color: #000;');
							a3.setAttribute('target','_blank');
							a3.innerHTML = '<?php echo $product_rating_count.' '.$product->post_title.' Reviews';?>';

							div_gen.appendChild( br1 );
							div_gen.appendChild( a3 );
<?php endif;?>

<?php if ( $display_text_rating_types == 1 ):?>
							var a2 = document.createElement('a');
							a2.setAttribute('href','<?php echo get_permalink( $product_id );?>');
							a2.setAttribute('style','line-height: 1;display: inline-block;font-size: 11px;text-decoration: none;font-weight: bold;font-style: italic;font-family: Arial;color: #000;');
							a2.setAttribute('target','_blank');
							a2.innerHTML = '<?php echo $text_rating_types;?>';

							div_gen.appendChild( br );
							div_gen.appendChild( a2 );
<?php endif;?>
						}
						i++;
						}
					}
<?php 
	$first_step = false; 
	$value_for_insert[] = "( '{$url}', '{$user_id}', '{$product_id}', '{$metod}', '{$rating_types}', '{$display_text_rating}', '{$display_text_reviews}' )";
	endforeach;
?>
<?php if ( empty( $d ) && !$first_step ): ?>
				}
				}
<?php endif;?>
				<?php
				if ( !preg_match( '|^'.get_bloginfo('url').'|', $url ) && !$first_step ) {
					$key_for_insert = '(`HTTP_REFERER`, `user_id`, `product_id`, `metod`, `rating_type`, `rating_value`, `reviews_amount`)';
					foreach ($variable as $key => $value) {
						# code...
					}
					$value_for_insert = implode( ',' , $value_for_insert );
					$wpdb->query("INSERT INTO {$wpdb->prefix}api_rating_referer {$key_for_insert} VALUE {$value_for_insert}");
				}
				break;	
			case 'iframe':
			/*<iframe src="http://crowdsunite.loc/wp-admin/admin-ajax.php?action=apiRating&product_id=52&metod=iframe&api_key=MQ%3D%3D" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:16px; width:80px;" allowTransparency="true"></iframe>*/
				header('X-Frame-Options: ALLOWALL');
				?>
				<!doctype html>
				<html lang="en">
				<body style="margin:0;padding:0;text-align:center;">
					<a href="<?php echo get_permalink( $product_id );?>" style="display: inline-block;font-size: 20px;text-decoration: none;font-weight: bold;font-style: italic;font-family: Arial;color: #ffc948;" target="_blank" >
						<?php if ( $display_text_rating == 1 ) printf( "%01.1f", $round_ );?> <div style="background: url(<?php echo $images_dir_uri;?>star.gif) repeat-x 0px 0px transparent;height:16px;width:80px;display: inline-block;" title="<?php echo $titles[$round];?>">
							<div style="width: <?php echo $round*10 ?>%;background:url(<?php echo $images_dir_uri;?>star.gif) repeat-x 0px -32px transparent;height: 100%;"></div>
						</div>
					</a><br/>
					<?php if ( $display_text_rating_types == 1 ):?>
					<a href="<?php echo get_permalink( $product_id );?>" style="display: inline-block;font-size: 14px;text-decoration: none;font-weight: bold;font-style: italic;font-family: Arial;color: #000;" target="_blank" ><?php echo $text_rating_types;?></a><br/>
					<?php endif;?>
					<?php if ( $display_text_reviews == 1 ):?>
					<a href="<?php echo get_permalink( $product_id );?>#tc_review" style="display: inline-block;font-size: 14px;text-decoration: none;font-weight: normal;font-style: italic;font-family: Arial;color: #000;" target="_blank" ><?php echo $product_rating_count.' '.$product->post_title.' reviews';?></a>
					<?php endif;?>
				</body>
				</html>
				<?php
				break;
			case 'img':
			/*<a href="http://crowdsunite.loc/platforms/kickstarter/" target="_blank"><img src="http://crowdsunite.loc/wp-admin/admin-ajax.php?action=apiRating&product_id=52&metod=img&api_key=MQ%3D%3D" /></a>*/
				$im_rating = imagecreatefrompng($images_dir_uri.'Rating_'.$round.'.png');
				$im_rating_width = imagesx( $im_rating );
				$im_rating_height = imagesy( $im_rating );
				$black_im_rating = imagecolorallocate( $im_rating, 0, 0, 0 );
				imagecolortransparent( $im_rating, $black_im_rating );

				$border = 5;

				if ( $display_text_rating == 1 ) {

					$font_rating = plugin_dir_path( __FILE__ ).'/fonts/arialbi.ttf';
					$height_font_rating = 11;
					$text_rating = sprintf( "%01.1f", $round_ );
					$text_rating_color = array(
						'red'=>'0xff',
						'green'=>'0xc9',
						'blue'=>'0x48'
						);

					$px = imageftbbox( $height_font_rating, 0, $font_rating, $text_rating );
					$rating_width = $px[2] - $px[0] + 2;
					$rating_height = $px[3] - $px[5];

				} else {

					$rating_width = 0;
					$rating_height = 0;

				}

				if ( $display_text_rating_types == 1 ) {

					$font_rating_types = plugin_dir_path( __FILE__ ).'/fonts/arialbi.ttf';
					$height_font_rating_types = 9;
					$text_rating_types_color_types = array(
						'red'=>'0x00',
						'green'=>'0x00',
						'blue'=>'0x00'
						);

					$px = imageftbbox( $height_font_rating_types, 0, $font_rating_types, $text_rating_types );
					$rating_types_width = $px[2] - $px[0];
					$rating_types_height = $px[3] - $px[5];

				} else {

					$rating_types_width = 0;
					$rating_types_height = 0;

				}

				if ( $display_text_reviews == 1 ) {

					$font_reviews = plugin_dir_path( __FILE__ ).'/fonts/ariali.ttf';
					$height_font_reviews = 9;
					$text_reviews = $product_rating_count.' '.$product->post_title.' Reviews';
					$text_reviews_color = array(
						'red'=>'0x00',
						'green'=>'0x00',
						'blue'=>'0x00'
						);

					$px = imageftbbox( $height_font_reviews, 0, $font_reviews, $text_reviews );
					$reviews_width = $px[2] - $px[0];
					$reviews_height = $px[3] - $px[5];

				} else {

					$reviews_width = 0;
					$reviews_height = 0;

				}

				if ( $reviews_width > ( $im_rating_width + $rating_width ) && $reviews_width > $rating_types_width ) {

					$width = $reviews_width;
					$offset_text_reviews = $border;
					$offset_im_rating = ( $width + $rating_width - $im_rating_width ) / -2 - $border;
					$offset_im_rating_types = ( $width - $rating_types_width ) / 2 + $border;

				} elseif ( $reviews_width < ( $im_rating_width + $rating_width ) && ( $im_rating_width + $rating_width ) > $rating_types_width ) {

					$width = $im_rating_width + $rating_width;
					$offset_im_rating = -$border - $rating_width;
					$offset_text_reviews = ( $width - $reviews_width ) / 2 + $border;
					$offset_im_rating_types = ( $width - $rating_types_width ) / 2 + $border;

				} else {

					$width = $rating_types_width;
					$offset_im_rating = ( $width + $rating_width - $im_rating_width ) / -2 - $border;
					$offset_text_reviews = ( $width - $reviews_width ) / 2 + $border;
					$offset_im_rating_types = $border;

				}

				$width += $border * 2;
				$height = $im_rating_height + $reviews_height + $rating_types_height + ( $border * 2 );

				$im = imagecreate( $width, $height );

				$white = imagecolorallocate( $im, 255, 255, 255 );

				imagecopyresampled ( $im , $im_rating , 0, 0, ( $offset_im_rating ), -$border, $width, $height, $width, $height );

				if ( $display_text_rating == 1 ) {

					$text_rating_color_ = imagecolorallocate( $im, $text_rating_color['red'], $text_rating_color['green'], $text_rating_color['blue'] );
					imagettftext( $im, $height_font_rating, 0, -$offset_im_rating - $rating_width, ( $rating_height + $border ), $text_rating_color_, $font_rating, $text_rating );

				}

				if ( $display_text_rating_types == 1 ) {

					$text_rating_types_color_ = imagecolorallocate( $im, $text_rating_types_color_types['red'], $text_rating_types_color_types['green'], $text_rating_types_color_types['blue'] );
					imagettftext( $im, $height_font_rating_types, 0, $offset_im_rating_types, ( $height - $reviews_height - $border - 2), $text_rating_types_color_, $font_rating_types, $text_rating_types );

				}

				if ( $display_text_reviews == 1 ) {
					$text_reviews_color_ = imagecolorallocate( $im, $text_reviews_color['red'], $text_reviews_color['green'], $text_reviews_color['blue'] );
					imagettftext( $im, $height_font_reviews, 0, $offset_text_reviews, ( $height - $border ), $text_reviews_color_, $font_reviews, $text_reviews );
				}

				header('Content-Type: image/png');

				imagepng( $im );
				imagedestroy( $im );
				break;
			default:
				die;
		}

		if ( !preg_match( '|^'.get_bloginfo('url').'|', $url ) && $metod !== "js" ) {
			$wpdb->insert( 
				$wpdb->prefix.'api_rating_referer', 
				array( 
					'HTTP_REFERER' => $url, 
					'user_id' => $user_id,
					'product_id' => $product_id,
					'metod' => $metod,
					'rating_type' => $rating_types,
					'rating_value' => $display_text_rating,
					'reviews_amount' => $display_text_reviews
				)
			);
		}
		die;
	}	
}
$CrowdsuniteRatingAPI = new CrowdsuniteRatingAPI();