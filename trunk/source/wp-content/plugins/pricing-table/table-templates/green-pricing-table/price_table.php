
 <?php    
    $data = get_post_meta($pid, 'pricing_table_opt',true);
    $featured=  get_post_meta($pid, 'pricing_table_opt_feature',true);
    $feature_description =  get_post_meta($pid, 'pricing_table_opt_feature_description',true);  
    $data_des = get_post_meta($pid, 'pricing_table_opt_description',true); 
    //print_r($data_des);  
    $feature_name=  get_post_meta($pid, 'pricing_table_opt_feature_name',true);
    $package_name=  get_post_meta($pid, 'pricing_table_opt_package_name',true);
    $classes = array('1','2','3','4','1','2','3','4','1','2','3','4','1','2','3','4');
    $kc = 0; 
    /*echo "<pre>"; 
    print_r($data); 
    echo "</pre>";   
     */
     
?>
<link rel="stylesheet" type="text/css" href="<?php echo plugins_url(); ?>/pricing-table/table-templates/green-pricing-table/css/style.css">
<link rel="stylesheet" type="text/css" href="<?php echo plugins_url(); ?>/pricing-table/table-templates/green-pricing-table/css/960.css"> 
<link rel="stylesheet" type="text/css" href="<?php echo plugins_url(); ?>/pricing-table/table-templates/green-pricing-table/css/button.css">
<!--<link rel="stylesheet" type="text/css" href="<?php echo plugins_url(); ?>/pricing-table/table-templates/blue-pricing-table/css/960.css">-->


  <div class="pt_container_12 wppt-img-left  <?php echo  $responsive; ?>_cont" id="shaon-pricing-table">
  
    <div class="pt_grid_12 green-pricing-table" id="green-pricing-table">
    <div class="pricing-table" >

          <?php    
    foreach($data as $key=> $value){
?>
        <div class="col1 <?php echo  $responsive; ?>">
            <div class="priceTitle-1"><span style="border:1px solid  #d6d6d6; -webkit-border-top-right-radius: 5px;-moz-border-radius-topright: 5px;border-top-right-radius: 5px; border-bottom:none;"><?php echo $package_name[$key];?></span></div>
            
            <div class="price-content" style="	-webkit-box-shadow: 0px 0px 0px 0px #b7b6b6;-moz-box-shadow: 0px 0px 0px 0px #7e7d7d;box-shadow: 0px 0px 0px 0px #7e7d7d; padding-bottom:13px;" >
                <ul>
                 <?php foreach($value as $key1=>$value1){
                     
                    if( strtolower($key1)!="buttonurl" && strtolower($key1)!="buttontext"){
                        
                        $ftr = strtolower($key1)!='detail'?$feature_name[$key1]:'';
                        if($data_des[$key][$key1]!=''){
                            $value1 = "<a class='wppttip' href='#' title='{$data_des[$key][$key1]}'>".$value1."</a>";
                        }
                        if($kc%2==0)
                        echo '<li >'.$value1.' <span class="feature-name">'.$ftr.'</span></li>';
                        else
                        echo '<li style="background:#f5f5f5; width:100%">'.$value1.' <span class="feature-name">'.$ftr.'</span></li>';
                    }
                    $kc++;
                }
                ?>
               
                </ul>                
                
                <a style="margin-top: 10px;" class="button green" href="<?php echo $value['ButtonURL']?>"  ><?php echo $value['ButtonText']?></a>
                
                
            </div>
           
        </div>
        
        <?php 
       $kc++;
       } 
       ?>       
        
    <div style="clear: both;"></div>            
    </div>  
        
        
    </div>
  
  
  </div>

