<?php
$data = get_post_meta($pid, 'pricing_table_opt',true);
$featured=  get_post_meta($pid, 'pricing_table_opt_feature',true);
$feature_description =  get_post_meta($pid, 'pricing_table_opt_feature_description',true);
$data_des = get_post_meta($pid, 'pricing_table_opt_description',true);
//print_r($data_des);
$feature_name=  get_post_meta($pid, 'pricing_table_opt_feature_name',true);
$package_name=  get_post_meta($pid, 'pricing_table_opt_package_name',true);
$pt = get_post($pid);
$alt_feature=get_post_meta($pid, 'alt_feature',true);
$alt_price=get_post_meta($pid, 'alt_price',true);
$alt_detail=get_post_meta($pid, 'alt_detail',true);
$style = $style>0&&$style<6?$style:2;
/*echo "<pre>";
print_r($data);
echo "</pre>";
 */
?>

<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro|Roboto|Open+Sans+Condensed:700,300,300italic' rel='stylesheet' type='text/css'>

<link href="<?php echo plugins_url('pricing-table/table-templates/table-6/css/bootstrap.css');?>" rel="stylesheet">
<link href="<?php echo plugins_url('pricing-table/table-templates/table-6/css/bootstrap-responsive.css');?>" rel="stylesheet">

<link href="<?php echo plugins_url('pricing-table/table-templates/table-6/style.css');?>" rel="stylesheet">

<link id="ptts" href="<?php echo plugins_url('pricing-table/table-templates/table-6/style'.$style.'.css');?>" rel="stylesheet">

<div class="w3eden">
<div class="container top50">
    <div class="row-fluid pricing-table">

        <div class="span12">
            <div class="row-fluid">
                <div class="span3">
                    <div class="free border-r-n">
                        <ul class="pricing-title">
                            <li class="price">
                                <div class="title">
                                    <h1>Item</h1>
                                    <div class="title-style"></div>
                                </div>

                                <h2>$0</h2>
                            </li>
                            <li class="list-top"><div class="bar-shadow"></div>
                                <div  class="title-row border-n">
                                    <p class="p-align-r">Detail</p><p class="pull-right">Detail</p>
                                </div>
                            </li>
                            <li>
                                <div class="title-row">
                                    <p class="p-align-r">Detail</p><p class="pull-right">Detail</p>
                                </div>
                            </li>
                            <li>
                                <div class="title-row">
                                    <p class="p-align-r">Detail</p><p class="pull-right">Not Available</p>
                                </div>
                            </li>
                            <li>
                                <div class="title-row">
                                    <p class="p-align-r">Detail</p><p class="pull-right">Detail</p>
                                </div>
                            </li>
                            <li>
                                <div class="title-row">
                                    <p class="p-align-r">Detail</p><p class="pull-right">Detail</p>
                                </div>
                            </li>
                            <li>
                                <div class="title-row">
                                    <p class="p-align-r">Detail</p><p class="pull-right">Detail</p>
                                </div>
                            </li>
                            <li>
                                <div class="title-row">
                                    <p class="p-align-r">Detail</p><p class="pull-right">Detail</p>
                                </div>
                            </li>
                            <li class="free-row-a footer-row">

                                <a href="#" class="btn btn-large btn-free">Download</a>
                            </li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
</div>