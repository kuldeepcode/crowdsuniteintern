
 <?php   
    $data = get_post_meta($pid, 'pricing_table_opt',true);
    $featured=  get_post_meta($pid, 'pricing_table_opt_feature',true); 
    $feature_description =  get_post_meta($pid, 'pricing_table_opt_feature_description',true);  
    $data_des = get_post_meta($pid, 'pricing_table_opt_description',true); 
    //print_r($data_des);
    $feature_name=  get_post_meta($pid, 'pricing_table_opt_feature_name',true);
    $package_name=  get_post_meta($pid, 'pricing_table_opt_package_name',true); 
    
     $alt_feature=get_post_meta($pid, 'alt_feature',true);
    $alt_price=get_post_meta($pid, 'alt_price',true);
    $alt_detail=get_post_meta($pid, 'alt_detail',true);
    
    $classes = array('basic','proffessional','business','unlimited','basic','proffessional','business','unlimited','basic','proffessional','business','unlimited');
    $kc = 0;
       
?>
<link href='http://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>

<link rel="stylesheet" type="text/css" href="<?php echo plugins_url(); ?>/pricing-table/table-templates/style3/css/style.css">
<link rel="stylesheet" type="text/css" href="<?php echo plugins_url(); ?>/pricing-table/table-templates/style3/css/960.css">  

<!--[if gte IE 9]>
  <style type="text/css">
    .gradient {
       filter: none;
    }
  </style>
<![endif]-->


  <div class="pt_container_12" id="shaon-pricing-table">
  
    <div class="pt_grid_12">
    <div class="pricing-table">
       
        <div class="col1">
            <div class="featureTitle">
            <span>&nbsp;</span></div>
            
            <div class="feature-content">
                <ul>
                <?php
     foreach($feature_name as $k=>$value1){
                     if(strtolower($value1)!="price" && strtolower($value1)!="buttonurl" && strtolower($value1)!="buttontext"){
                         if(strtolower($value1)=="price"){
                 if(!empty($alt_price))echo "<li>".$alt_price."</li>";else echo "<li>".$value1."</li>";
             }else if(strtolower($value1)=="detail"){
                 if(!empty($alt_detail))echo "<li>".$alt_detail."</li>";else echo "<li>".$value1."</li>";
             }else {  
                 if($feature_description[$k]=='')
                 echo "<li>".$value1."</li>"; 
                 else
                 echo "<li class='wppttip' title='{$feature_description[$k]}'>".$value1."</li>"; 
             }
                     }    
                     
                 }
?>
                
                </ul>
            </div>

                        
        </div>
        
        
         <?php
    foreach($data as $key=> $value){
?>
        
        <div class="col1">
            
            <div class="colheader ">
            <span class="packName <?php echo $classes[$kc];?>Pack <?php if($k==0) echo 'left-top-radious'; if($kc==count($data)-1) echo 'right-top-radious'; ?>"><?php echo $package_name[$key];?></span>
            <?php
             $k=@array_keys($value);
                    // echo  $value[$fkeys[0]];
                     if(strtolower($k[0])=="price"){
                     ?>
            <span class="price <?php echo $classes[$kc++];?>Price"><?php echo $value['Price'];?></span> 
            
            <?php } ?>  
            </div> 
           
              <div class="price-content left-bottom-radious">
                <ul>
                <?php foreach($value as $key1=>$value1){
                    if(strtolower($key1)!="price" && strtolower($key1)!="buttonurl" && strtolower($key1)!="buttontext"){
                     if($data_des[$key][$key1]=='')
                        echo "<li>".$value1."</li>";
                        else
                     echo "<li><a class='wppttip' href='#' title='{$data_des[$key][$key1]}'>".$value1."</a></li>";
                    }
                }
                 ?> 
                </ul>                
                <a class="signup" href="<?php echo $value['ButtonURL']?>"><?php echo $value['ButtonText']?></a>
                
              <div style="clear: both;"><br/></div>  
            </div>
            
        </div>
       <?php } ?> 
        
       
        
    </div>  
        
        
    </div>
  
  
  </div>
