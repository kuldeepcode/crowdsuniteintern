<?php
/*
Plugin Name: Pricing Table Pro
Plugin URI: http://wpeden.com/product/wordpress-pricing-table-plugin/
Description: WordPress Plugin to creating colorful pricing tables
Author: Shaon
Version: 2.7.6
Author URI: http://www.wpeden.com/
*/

error_reporting(0);

include("libs/class.plugin.php");

global $enque, $pt_plugin;

$pt_plugin = new ahm_plugin('pricing-table');

$enque = 1;

$plugindir = str_replace('\\','/',dirname(__FILE__));


define('PLUGINDIR',$plugindir);

if($_GET['post_type']=='pricing-table'){
    define('KEY_VAR_NAME','invoice_no');
    define('PAGE','/');
    define('SCRIPT_NAME','Pricing Table Pro');
    define('BUY_NOW_URL','http://wpeden.com/product/wordpress-pricing-table-plugin/');
    define('LICENSE_VALIDATOR','http://scriptlicense.net/');
    define('PRICE','16.00');


    if(!function_exists('_is_valid_license_key')){
        function _is_valid_license_key($key_name){
            $key = $_POST[$key_name]?$_POST[$key_name]:get_option($key_name);
            $domain = strtolower(str_replace("www.","",$_SERVER['HTTP_HOST']));

            if(!$key) { @unlink(dirname(__FILE__)."/wppt_{$domain}_{$key_name}"); return false;                }

            if(file_exists(dirname(__FILE__)."/wppt_{$domain}_{$key_name}")){
                $data = unserialize(base64_decode(file_get_contents(dirname(__FILE__)."/wppt_{$domain}_{$key_name}")));
                if($data[0]==md5($domain.$key)&&$data[1]>time())
                    return true;
                else
                    @unlink("wppt_{$domain}_{$key_name}");
            }
            $res = _remote_post(LICENSE_VALIDATOR,array('do'=>'verify_license','domain'=>$domain,'invoice_no'=>$key,'pid'=>PRODUCT_ID));

            if($res==='valid') {
                file_put_contents(dirname(__FILE__)."/wppt_{$domain}_{$key_name}",base64_encode(serialize(array(md5($domain.$key),strtotime("+15 days"))))) or die("can't write file");
                update_option($key_name,$key);
                return true;
            }

            return false;
        }

        function _remote_post($url, $data){

            foreach($data as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
            rtrim($fields_string,'&');
            //open connection
            $ch = curl_init();
            //set the url, number of POST vars, POST data
            curl_setopt($ch,CURLOPT_URL,$url);
            curl_setopt($ch,CURLOPT_POST,count($fields));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER  ,1);
            curl_setopt($ch,CURLOPT_POSTFIELDS,$fields_string);
            //execute post
            $result = curl_exec($ch);

            //close connection
            curl_close($ch);
            return $result;
        }

        if(!function_exists('get_option')){
            function get_option($opt){
                $opt = md5($opt);
                return @file_get_contents($opt);
            }

            function update_option($opt, $val){
                $opt = md5($opt);
                @file_put_contents($opt, $val);
            }

            function delete_option($opt, $val){
                $opt = md5($opt);
                @unlink($opt);
            }
        }
    }


    if($_SERVER['HTTP_X_REQUESTED_WITH']!='XMLHttpRequest'){
        if(!_is_valid_license_key(KEY_VAR_NAME)){

            ?>
            <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
                "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
            <html xmlns="http://www.w3.org/1999/xhtml">

            <head>
            <title>License Key for <?php
                echo SCRIPT_NAME;
                ?></title>

            <link href='http://fonts.googleapis.com/css?family=Wallpoet&v1' rel='stylesheet' type='text/css'>

            <style type="text/css">
                /* Reset */
            html, body, div, span, h1, h2, h3, h4, h5, h6, p, blockquote, pre,
            a, font, img, ul, li {
                margin: 0;
                padding: 0;
                border: 0;
                outline: 0;
                font-size: 100%;
                vertical-align: baseline;
                background: transparent;
            }
            body {
                line-height: 1;
            }
            .txt{
                font-size:12pt;
                border:1px solid #BAC1C4;
                -webkit-border-radius: 5px;
                -moz-border-radius: 5px;
                border-radius: 5px;
                padding:3px 10px;
                color: #555;
                font-family: 'Wallpoet', 'Segoe UI';
            }
            .button{
                background: #b8c6df;
                background: -moz-linear-gradient(top, #b8c6df 0%, #6d88b7 100%);
                background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#b8c6df), color-stop(100%,#6d88b7));
                background: -webkit-linear-gradient(top, #b8c6df 0%,#6d88b7 100%);
                background: -o-linear-gradient(top, #b8c6df 0%,#6d88b7 100%);
                background: -ms-linear-gradient(top, #b8c6df 0%,#6d88b7 100%);
                filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#b8c6df', endColorstr='#6d88b7',GradientType=0 );
                background: linear-gradient(top, #b8c6df 0%,#6d88b7 100%);
                color:#fff;
                border:1px solid #b8c6df;
                padding:4px 15px;
                font-size:10pt;
                -webkit-border-radius: 5px;
                -moz-border-radius: 5px;
                border-radius: 5px;
            }
            ol, ul {
                list-style: none;
            }

            :focus {
                outline: 0;
            }
                /* // Reset */

            body {
                background: #eee; /* image for body made with Photoshop using noise filter (gaussian monochromatic) on #ccc */
                font-family: Georgia, Verdana, �Lucida Sans Unicode�, sans-serif;
                font-size: 12px;
                color: #999;
            }

            h2 {
                font-style: italic;
                font-weight: normal;
                line-height: 1.2em;
            }

            div#container {
                margin: 50px auto 0px auto; /* centered */
                width: 400px;
            }

            .menu {
                position: relative;
                top:3px;
                left: 50px;
                z-index: 80; /* the stack order: displayed under bubble (90) */
            }

            .menu ul li {
                -webkit-transform: rotate(-45deg); /* rotate the list item */
                -moz-transform: rotate(-45deg); /* rotate the list item */
                width: 50px;
                overflow: hidden;
                margin: 10px 0px;
                padding: 5px 5px 5px 18px;
                float: left;
                background: #7f9db9;
                text-align: right;
            }

            .menu ul li a {
                color: #fff;
                text-decoration: none;
                display:block;
            }

            .menu ul li.l1 {
                background: rgba(131,178,51,0.65);
            }

            .menu ul li.l1:hover {
                background: rgb(131,178,51);
            }

            .menu ul li.l2 {
                background: rgba(196,89,30,0.65);
            }

            .menu ul li.l2:hover {
                background: rgb(196,89,30);
            }

            .menu ul li.l3 {
                background: rgba(65,117,160,0.65);
            }

            .menu ul li.l3:hover {
                background: rgb(65,117,160);
            }

            .menu span {
                margin: 15px 80px 0px 0px;
                float:right;
            }

            .bubble {
                clear: both;
                margin: 0px auto;
                width: 350px;
                background: #fff;
                -moz-border-radius: 10px;
                -khtml-border-radius: 10px;
                -webkit-border-radius: 10px;
                -moz-box-shadow: 0px 0px 8px rgba(0,0,0,0.3);
                -khtml-box-shadow: 0px 0px 8px rgba(0,0,0,0.3);
                -webkit-box-shadow: 0px 0px 8px rgba(0,0,0,0.3);
                position: relative;
                z-index: 90; /* the stack order: displayed under ribbon rectangle (100) */
            }

            .rectangle {
                background: #7f9db9;
                height: 50px;
                width: 380px;
                position: relative;
                left:-15px;
                top: 30px;
                float: left;
                -moz-box-shadow: 0px 0px 4px rgba(0,0,0,0.55);
                -khtml-box-shadow: 0px 0px 4px rgba(0,0,0,0.55);
                -webkit-box-shadow: 0px 0px 4px rgba(0,0,0,0.55);
                z-index: 100; /* the stack order: foreground */
            }

            .rectangle h2 {
                font-size: 30px;
                color: #fff;
                padding-top: 6px;
                text-shadow: 1px 1px 2px rgba(0,0,0,0.2);
                text-align: center;
            }

            .triangle-l {
                border-color: transparent #7d90a3 transparent transparent;
                border-style:solid;
                border-width:15px;
                height:0px;
                width:0px;
                position: relative;
                left: -30px;
                top: 65px;
                z-index: -1; /* displayed under bubble */
            }

            .triangle-r {
                border-color: transparent transparent transparent #7d90a3;
                border-style:solid;
                border-width:15px;
                height:0px;
                width:0px;
                position: relative;
                left: 350px;
                top: 35px;
                z-index: -1; /* displayed under bubble */
            }

            .info {
                padding: 40px 25px 35px 25px;
            }

            .info h2 {
                font-size: 20px;
            }

            .info p {
                padding-top: 10px;
                font-size: 14px;
                line-height: 22px;
            }

            .info p a {
                color: #c4591e;
                text-decoration: none;
            }

            .info p a:hover {
                text-decoration: underline;
            }

            </style>
            </head>

            <body>
            <form action="" method="post">

                <div id="container">

                    <div class="menu">
                        <ul>
                            <li class="l1"><a href="http://wpeden.com/forums/forum/plugin-support/quick-notice-bar-pro/">Support</a></li>
                            <li class="l2"><a href="mailto:contact@wpeden.com">Contact</a></li>

                            <li class="l3"><a href="<?php echo BUY_NOW_URL; ?>">BuyNow</a></li>
                        </ul>
                        <span style="font-style: italic">by <a href="http://quicknoticebar.com/" style="color: #777;">WPQNB Pro</a></span>
                    </div>

                    <div class="bubble">
                        <div class="rectangle"><h2>Support Key Verification</h2></div>
                        <div class="triangle-l"></div>
                        <div class="triangle-r"></div>
                        <div class="info">
                            <h2>Enter Support Key for:<Br/> <?php echo SCRIPT_NAME; ?></h2>
                            <p>
                                <?php  if($_POST[KEY_VAR_NAME]!='') echo "<i style='color:#cc0000;font-size:9pt;font-weight:normal;letter-spacing:1px;'>Invalid license number</i><br/>"; ?>
                                <input class="txt" type="text" style="width: 92%" value="<?php echo $_POST[KEY_VAR_NAME]?$_POST[KEY_VAR_NAME]:get_option(KEY_VAR_NAME); ?>" name="<?php echo KEY_VAR_NAME; ?>" /> <br>
                            </p>
                            <p align="right">
                                <input align="right" class="button" type="submit" value="Submit" />
                            </p>
                        </div>
                    </div>

                </div>
            </form>
            </body>
            </html>
            <?php
            die();
        } else if(_is_valid_license_key(KEY_VAR_NAME)&&$_POST[KEY_VAR_NAME]!=''){
            header("location: ".$_SERVER['HTTP_REFERER']);
            die();
        }
    }

}


function wppt_custom_init()
{
    $labels = array(
        'name' => _x('Pricing Tables', 'post type general name'),
        'singular_name' => _x('Pricing Table', 'post type singular name'),
        'add_new' => _x('Add New', 'pricing-table'),
        'add_new_item' => __('Add New Pricing Table'),
        'edit_item' => __('Edit Pricing Table'),
        'new_item' => __('New Pricing Table'),
        'all_items' => __('All Pricing Tables'),
        'view_item' => __('View Pricing Table'),
        'search_items' => __('Search Pricing Tables'),
        'not_found' =>  __('No Pricing Table found'),
        'not_found_in_trash' => __('No Pricing Tables found in Trash'),
        'parent_item_colon' => '',
        'menu_name' => 'Pricing Table'

    );
    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => null,
        'supports' => array('title'),
        'menu_icon' => plugins_url().'/pricing-table/images/table.gif'
    );
    register_post_type('pricing-table',$args);
}


function wppt_preview_table($content){
    global $wp_query;
    if(get_post_type()!='pricing-table') return $content;
    $pid = get_the_ID();
    $template = $_REQUEST['template']?$_REQUEST['template']:'metro';
    $responsive = $params['responsive']?'responsive':'';
    $styles = array('epic'=>9,'mega'=>9,'lucid'=>10,'override'=>9);

    ob_start();
    ?>
    <form action="">
        <input type="hidden" name="pricing-table" value="<?php echo $wp_query->query_vars['pricing-table']; ?>">
        <b>Select Template</b><br/>
        <select name="template" class="button input" id="ptt">
            <option value="">Select Template</option>
            <?php
            $directory = ABSPATH."/wp-content/plugins/pricing-table/table-templates";
            $directory_list = opendir($directory);
            while (FALSE !== ($file = readdir($directory_list))){
                // if the filepointer is not the current directory
                // or the parent directory
                if($file != '.' && $file != '..'){
                    $sel = $_GET['template']==$file?'selected=selected':'';
                    $path = $directory.'/'.$file;
                    if(is_dir($path)){
                        echo "<option $sel value='".$file."'>".strtoupper($file)."</option>";
                    }
                }
            }
            ?>

        </select> 
        <input type="submit" value="Preview"><br/>
        <?php if($styles[$template]>0){ ?>
            <b>Select Style</b><br/>
            <select onchange="jQuery('#ptts').attr('href','<?php echo plugins_url('pricing-table/table-templates/'.$template.'/style');?>'+this.value+'.css')">
                <option value="">Select Style</option>
                <?php for($dx = 1; $dx <= $styles[$template]; $dx++){ ?>
                    <option value="<?php echo $dx; ?>">Style <?php echo $dx; ?></option>

                <?php } ?>
            </select>
        <?php } ?>

    </form>

    <?php
    include("table-templates/{$template}/price_table.php");
    $data = ob_get_contents();
    ob_clean();
    $code[] = "[y]";
    $icons[] = "<img src='".plugins_url("pricing-table/images/yes.png")."' />";
    $code[] = "[n]";
    $icons[] = "<img src='".plugins_url("pricing-table/images/no.png")."' />";
    $code[] = "[na]";
    $icons[] = "<img src='".plugins_url("pricing-table/images/na.png")."' />";
    $data = str_replace($code, $icons, $data);
    return $content.$data;
}

function wppt_table($params){
    $pid = $params['id'];
    $style = $params['style'];
    extract($params);
    $template = $params['template']?$params['template']:'glossy';
    $responsive = $params['responsive']?'responsive':'';
    ob_start();
    include("table-templates/{$template}/price_table.php");
    $data = ob_get_contents();
    ob_clean();
    $shortcode = get_option("_wppt_shortcode",array());
    if(is_array($shortcode)){
        foreach($shortcode as $c){
            if(in_array(strtolower(end(explode('.',$c))),array('png','jpg','gif','jpeg')))
                $icons[] = "<img src='$c' />";
            else
                $icons[] = $c;
        }}

    $code=get_option("_wppt_code");
    $code = array_values($code);
    $code[] = "[y]";
    $icons[] = "<img src='".plugins_url("pricing-table/images/yes.png")."' />";
    $code[] = "[n]";
    $icons[] = "<img src='".plugins_url("pricing-table/images/no.png")."' />";
    $code[] = "[na]";
    $icons[] = "<img src='".plugins_url("pricing-table/images/na.png")."' />";
    $data = str_replace($code, $icons,$data);
    return $data;
}


function wppt_columns_struct( $columns ) {
    $column_shorcode = array( 'shortcode' => 'Embed Code' );
    $columns = array_slice( $columns, 0, 2, true ) + $column_shorcode + array_slice( $columns, 2, NULL, true );
    return $columns;
}

function wppt_column_obj( $column ) {
    global $post;
    switch ( $column ) {
        case 'shortcode':
            echo "<input type=text readonly=readonly value='[ahm-pricing-table id={$post->ID}]' size=25 style='font-weight:bold;text-align:Center;' onclick='this.select()' />";
            break;
    }
}



function wppt_help(){
    include(dirname(__FILE__)."/tpls/help.php");
}

function wppt_shortcodes(){
    include(dirname(__FILE__)."/tpls/short-codes.php");
}
function wppt_save_shortcode(){
    update_option('_wppt_shortcode', $_POST['shortcode']);
    update_option('_wppt_code', $_POST['code']);
}

function wppt_menu(){
    add_submenu_page('edit.php?post_type=pricing-table', 'Short Codes', 'Short Codes', 'administrator', 'short-codes', 'wppt_shortcodes');
    add_submenu_page('edit.php?post_type=pricing-table', 'Help', 'Help', 'administrator', 'help', 'wppt_help');

}


if(is_admin()){
    add_action("admin_menu","wppt_menu");
}

function wppt_admin_enqueue_scripts(){
    global $pt_plugin;
    wp_enqueue_script("jquery");
    wp_enqueue_script("jquery-form");
    $pt_plugin->load_scripts();
    $pt_plugin->load_styles();
}

function wppt_enqueue_scripts(){
    global $pt_plugin, $enque;
    if($enque==1){
        wp_enqueue_script("jquery");
        $pt_plugin->load_scripts();
        $pt_plugin->load_styles();
        wp_enqueue_script("tiptipjs", plugins_url()."/pricing-table/js/site/jquery.tipTip.minified.js",array('jquery'));
        wp_enqueue_style("tiptipcss", plugins_url()."/pricing-table/css/site/tipTip.css");
    }
}


function wppt_tiptip_init(){
    global $enque;

    if($enque==1){
        ?>
        <script language="JavaScript">
            jQuery(function(){
                jQuery(".wppttip").tipTip({defaultPosition:'bottom'});
            });

        </script>
    <?php
    }
}

$pt_plugin->load_modules();


function wppt_baseurl(){
    global $enque;

    if($enque==1){
        ?>
        <script language="JavaScript">
            var wppt_url = "<?php echo plugins_url('/pricing-table/'); ?>";
        </script>
    <?php
    }
}


function wppt_detect_shortcode()
{
    global $post, $enque;
    $pattern = get_shortcode_regex();

    if (   preg_match_all( '/'. $pattern .'/s', $post->post_content, $matches )
        && array_key_exists( 2, $matches )
        && in_array( 'ahm-pricing-table', $matches[2] ) )
    {
        $enque = 1;
    }
}

function wppt_post_row_actions($actions, $post){
    if($post->post_type=='pricing-table')
        $actions['clone'] = "<a style='color:#2D873F' href='".admin_url()."?task=wpptclone&clone={$post->ID}'>Clone</a>";
    return $actions;
}

function wppt_clone(){
    if($_GET['task']!='wpptclone'||!is_admin()) return false;
    $pid = $_GET['clone'];
    $data = get_post_meta($pid, 'pricing_table_opt',true);
    $featured=  get_post_meta($pid, 'pricing_table_opt_feature',true);
    $feature_description =  get_post_meta($pid, 'pricing_table_opt_feature_description',true);
    $data_des = get_post_meta($pid, 'pricing_table_opt_description',true);
    $feature_name=  get_post_meta($pid, 'pricing_table_opt_feature_name',true);
    $package_name=  get_post_meta($pid, 'pricing_table_opt_package_name',true);
    $npid = wp_insert_post(array("post_title"=>'New Pricing Table','post_status'=>'draft','post_type'=>'pricing-table'));

    update_post_meta($npid,'pricing_table_for_post',$featured);
    update_post_meta($npid,'pricing_table_opt',$data);
    update_post_meta($npid,'pricing_table_opt_description',$data_des);
    update_post_meta($npid,'pricing_table_opt_feature',$featured);
    update_post_meta($npid,'pricing_table_opt_feature_name',$feature_name);
    update_post_meta($npid,'pricing_table_opt_feature_description',$feature_description);
    update_post_meta($npid,'pricing_table_opt_package_name',$package_name);
    header("location: post.php?post={$npid}&action=edit");
    die();

}

add_action( 'wp', 'wppt_detect_shortcode' );

register_activation_hook(__FILE__,'wppt_install');

add_filter('post_row_actions', 'wppt_post_row_actions',10, 2);
add_action('wp_head', 'wppt_baseurl');
add_action('wp_footer', 'wppt_tiptip_init');
add_action('init', 'wppt_clone');
add_action('init', 'wppt_custom_init');
add_shortcode("ahm-pricing-table",'wppt_table');
add_filter("the_content",'wppt_preview_table');
add_filter( 'manage_edit-pricing-table_columns', 'wppt_columns_struct', 10, 1 );
add_action( 'manage_posts_custom_column', 'wppt_column_obj', 10, 1 );
add_action('wp_ajax_wppt_save_shortcode', 'wppt_save_shortcode');
add_action('wp_enqueue_scripts', 'wppt_enqueue_scripts');
add_action('admin_enqueue_scripts', 'wppt_admin_enqueue_scripts');   