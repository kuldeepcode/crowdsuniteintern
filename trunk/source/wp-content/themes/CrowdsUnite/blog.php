<?php
/*
Template Name: Blog
*/

global $helper,$wp_query,$posts,$post, $current_custom_page;

if(!isset($_GET['all'])) {

	$args = array('numberposts' => 1, 'orderby' => 'date', 'order' => 'DESC');
	$wp_query = new WP_Query($args);

	if($wp_query->have_posts()) {
		$wp_query->the_post();
		header('Location: '. get_permalink( $post->id ) );
		exit();
	}
}

get_header();
?>
    <!-- Body start here -->

    <div id="container">
        <div class="container_res">
            <div class="col_box" style="border-bottom:4px #eceeef solid;">
                <h1 class="blue2">

                    <?php echo __('Blog','re') ?> <div style="position:absolute; top:80px; margin-left: 980px; ">  <a href="http://feeds.feedburner.com/crowdsunite" title="Subscribe to my feed" rel="alternate" type="application/rss+xml"><img src="//feedburner.google.com/fb/images/pub/feed-icon16x16.png" alt="" style="border:0"/>

                </h1><a href="http://feeds.feedburner.com/crowdsunite" title=

            </div>

                    <?php
                    //$page = $wp_query->query_vars['paged'];
                    //wp_reset_postdata();
                    //query_posts(array(
                    //						'post_type' => 'article',
                    //						'posts_per_page' => 1,
                    //						'paged' => $page ? $page : 1,
                    //						'order' => 'DESC',
                    //						'orderby' => 'date'
                    //					  ));
                    $wp_query = new WP_Query();

                    $wp_query->query('posts_per_page=9'.'&paged='.$paged.'&post_type=post&post_status=publish');

                    while ($wp_query->have_posts())
                        {
                            $wp_query->the_post();


                            ?>
                            <div class="col_box2">
                                <div class="text">
                                    <div class="title">
                                        <div class="title_left">
                                            <h1><a href="<?php the_permalink(); ?>">
                                                    <?php
                                                    //if(strlen($post->post_title) > 32)
                                                    //{
                                                    //	echo substr($post->post_title,0,31).'...';
                                                    //}else
                                                    echo $post->post_title;
                                                    ?>
                                                </a></h1>
                                        </div>
                                        <span class="widget-stat-time" style="display:inline;float:right;"> <?php echo the_time(get_option('date_format')); ?> </span>
                                    </div>

                                    <div class="content_text">
                                        <p><?php echo tgt_limit_content(get_the_content(),34); ?></p>
                                        <br/>

                                        <p><a style="color:#1793D3;" href="<?php the_permalink(); ?>"><?php _e('Comments','re'); ?>&nbsp;<?php if($post->comment_count>0) echo '('.$post->comment_count.')'; ?></a></p>
                                    </div>
                                </div>
                            </div>
                        <?php
                        }
                        ?>
                        <!--Pagination-->
                        <div class="col_box1">

				<?php

				//echo tgt_generate_pagination('paged', 4);

		         if(function_exists('wp_pagenavi')) { wp_pagenavi(); }                   //�Updated by Brill :: 21-June-2013�

				if ( $wp_query->max_num_pages > 1 ) {
				?>

				<!--<div class="pagination">                                             �Updated by Brill :: 21-June-2013�
					<b class="paginate_title">  <?php //_e('Pages') ?> :</b>             �Updated by Brill :: 21-June-2013�  
					<?php
						//echo paginate_links( get_pagination_args() );                  �Updated by Brill :: 21-June-2013�  
					?>	
				</div>-->                                                                <!--�Updated by Brill :: 21-June-2013� -->

				<?php  } ?>
			</div>

                </div>

    </div>

    <!--body end here-->

<?php
get_footer();
?>