<?php
global $user_ID;
$enable_fb_login = get_option('tgt_fb_login');
$is_fb_logout = 0;

//if(function_exists(session_is_registered) && session_is_registered('fb_logout'))
if(isset( $_SESSION['fb_logout'] ))
{
	$is_fb_logout = $_SESSION['fb_logout'];
	//session_unregister('fb_logout');
    unset( $_SESSION['fb_logout'] );
}
if($enable_fb_login && !$is_fb_logout)
{
	if($user_ID > 0)
	{
		$cookie = null;		
		$_COOKIE['fbsr_'.FACEBOOK_APP_ID] = '';
	}
	elseif( isset($_SESSION['auto_log_fb_']) and $user_ID < 1 )
	{
		if ( !($_GET['action'] == register && $_GET['fb_register'] == 1) ) unset( $_SESSION['auto_log_fb_'] ); ;
		$cookie = get_facebook_cookie(FACEBOOK_APP_ID, FACEBOOK_SECRET);

		if (!empty($cookie)) {
            $fb_user_contents = file_get_contents('https://graph.facebook.com/'.$cookie['user_id'].'?access_token=' . FACEBOOK_APP_ID . '|' . FACEBOOK_SECRET);
            		if(!empty($fb_user_contents))
		   	{
		   		tgt_login_facebook($fb_user_contents);
		   		exit;
		   	}
		}
	}
}
global $helper,$current_user; 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<?php
$helper->favicon();
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php if ( $_GET['action'] == register && $_GET['fb_register'] == 1 ) echo '<meta http-equiv="refresh" content="2;URL='.get_bloginfo('url').'" />';?>
<?php wp_head(); ?>
<?php
$args = array(
	'type'                     => 'post',
	'parent'                   => 0,
	'orderby'                  => 'slug',
	'order'                    => 'ASC',
	'hide_empty'               => 0,
	'exclude'                  => 1,
	'hierarchical'             => 1,
	'pad_counts'               => false );
$main_category = get_categories($args);
?>
<title><?php wp_title(); ?></title>
    <!-- start Mixpanel --><!--script type="text/javascript">(function(c,a){window.mixpanel=a;var b,d,h,e;b=c.createElement("script");b.type="text/javascript";b.async=!0;b.src=("https:"===c.location.protocol?"https:":"http:")+'//cdn.mxpnl.com/libs/mixpanel-2.1.min.js';d=c.getElementsByTagName("script")[0];d.parentNode.insertBefore(b,d);a._i=[];a.init=function(b,c,f){function d(a,b){var c=b.split(".");2==c.length&&(a=a[c[0]],b=c[1]);a[b]=function(){a.push([b].concat(Array.prototype.slice.call(arguments,0)))}}var g=a;"undefined"!==typeof f?
        g=a[f]=[]:f="mixpanel";g.people=g.people||[];h="disable track track_pageview track_links track_forms register register_once unregister identify name_tag set_config people.identify people.set people.increment".split(" ");for(e=0;e<h.length;e++)d(g,h[e]);a._i.push([b,c,f])};a.__SV=1.1})(document,window.mixpanel||[]);
mixpanel.init("51e34272db71753c31a79d7621cc37ce");</script--><!-- end Mixpanel -->
<!-- Begin Inspectlet Embed Code -->
<!--script type="text/javascript" id="inspectletjs">
	var __insp = __insp || [];
	__insp.push(['wid', 2039150879]);
	(function() {
		function __ldinsp(){var insp = document.createElement('script'); insp.type = 'text/javascript'; insp.async = true; insp.id = "inspsync"; insp.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://www.inspectlet.com/inspect/2039150879.js'; var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(insp, x); }
		if (window.attachEvent){
			window.attachEvent('onload', __ldinsp);
		}else{
			window.addEventListener('load', __ldinsp, false);
		}
	})();
</script-->
<!-- End Inspectlet Embed Code -->
</head>
<body <?php body_class(); ?>>
  <!-- ClickTale Top part -->
<!--script type="text/javascript">
var WRInitTime=(new Date()).getTime();
</script-->
<!-- ClickTale end of Top part -->

	<div id="header">
    	<div class="header_res">
        	<div class="header_main">
            <div class="top navbar">
            		<div class="logo">
						<a href="<?php echo HOME_URL; ?>">
						<?php
						$curr_logo = '/images/logo.png';
						$curr_logo = explode('/',get_option(SETTING_LOGO));
						?>
						<?php
						if(count($curr_logo) > 1 && $curr_logo[0] == 'images')
						{
						?>
							<img src="<?php echo TEMPLATE_URL.'/'.$curr_logo[0].'/'.$curr_logo[1]; ?>"  alt="CrowdsUnite Logo" style="height:34px;"/>
						<?php
						}else
						{
						?>
							<img src="<?php echo WP_CONTENT_URL .'/uploads/re/'.$curr_logo[0]; ?>"  alt="CrowdsUnite Logo" style="height:34px;"/>
						<?php
						}
						?>
						</a>
					</div>
					<div class="menu">
					<?php
						//echo $helper->link( $helper->image('icon_home.png', 'test') , HOME_URL , array('style' => 'float:left; margin:5px 20px 0 0;'));
						?>                       
							<div class="review">
								<ul>
								<?php
									$page_view = 'product';
									if ( get_query_var('post_type') == 'article' || (!empty( $_GET['action'] ) && $_GET['action'] == 'articles'  ) )
										$page_view = 'article';
								?>
								<?php
								wp_nav_menu(array( 'after'=>'<span class="naviline">&nbsp;</span>','menu' => '', 'container' => '','walker' => '' , 'fallback_cb' => '', 'theme_location' => 'header_menu','items_wrap' => '%3$s' ));
								?>
								<?php if(get_option(SETTING_FEED_LINK_MENU) == 1) { ?>
								<li>
									<a href="<?php echo get_feed_link('rss2') ?>" target="_blank">
										<?php echo $helper->image('feed.png', '', array('style' => ' margin-top:-3px;')) ?>
									</a>
								</li>
								<?php } 
								?>
							 </ul>
						</div>
					</div>
	                <div class="navi_list" style="max-width: 420px">
	                    <?php
	                    if($user_ID < 1)
	                    {
	                        if($enable_fb_login)
	                        {
	                            ?>
	                            <div id="fb-root"></div>

	                        <?php } ?>
	                        <!-- Don't require to login, users can log in when they review-->
	                        <ul>
	                            <li><?php echo $helper->link('<strong>'.__('Login', 're').'</strong>', tgt_get_permalink('login') );?>

	                            </li>
	                            <li><?php echo $helper->link('<strong>'.__('Register','re').'</strong>', tgt_get_permalink('register') )?>



	                        </ul>
	                    <?php
	                    }
	                    elseif ($user_ID > 0){
	                        ?>
	                        <ul>
	                            <li class="menu-item"><?php echo '<a href="'.get_author_posts_url($user_ID).'"><strong>'.get_userdata($user_ID)->display_name.'</strong></a>';?>
		                            <ul class="sub-menu">
		                            	<li><?php echo $helper->link( '<strong>' . __('Edit Profile', 're') . '</strong>', tgt_get_permalink('edit_profile') , array('style' => 'padding-right:7px;') ) ?></li>
										<li><?php echo $helper->link( '<strong>' . __('Logout', 're') . '</strong>',  tgt_get_permalink('logout'), array('style' => 'padding-right:7px;') ) ?></li>
									</ul>
	                            </li>
	                        </ul>
	                    <?php } ?>
	                </div>
	                <?php include PATH_PAGES . DS . 'search_form.php' ?>
					<div class="clear"></div>
				</div>
			</div>
		</div>
</div>
<script type="text/javascript">
	jQuery(document).ready(function($){
		$('.menu-item').on('mouseover',function(){		
			$(this).find('ul.sub-menu').first().stop(true, true).slideDown();
		});
		$('.menu-item').on('mouseleave',function(){
			$(this).find('ul.sub-menu').first().stop(true, true).hide();
		});
	});
</script>
<!-- Header end here -->
