<?php
	function woo_listings_content_install( $reset = false ){
		
		$woo_array_cpt_all = array();
		$woo_array_ctx_all = array();
		$woo_array_cmb_all = array();
		
		// Check if each option is present in the database and is an array.
		// If it isn't there or isn't an array, add it or set it to it's original state.
		
		$_existing_fields = array();
		$_fields = array(
						'woo_content_builder_cpt' => $woo_array_cpt_all, 
						'woo_content_builder_ctx' => $woo_array_ctx_all, 
						'woo_content_builder_cmb' => $woo_array_cmb_all
						);
		
		foreach ( $_fields as $k => $v ) {
		
			$_option = get_option( $k );
		
			if ( ! $_option || ! is_array( $_option ) ) {
			
				add_option( $k, $v );
			
			} else {
			
				$_existing_fields[] = $k;
				
				// Assign the current value to the appropriate array variable.
				// ${$v} = $_option;
						
			} // End IF Statement
		
		} // End FOREACH Loop
		
		/*
		add_option('woo_content_builder_cpt',$woo_array_cpt_all);
		add_option('woo_content_builder_ctx',$woo_array_ctx_all);
		add_option('woo_content_builder_cmb',$woo_array_cmb_all);
		*/
		
		/* CUSTOM POST TYPES */
		
		$woo_array_cpt = array();
		
		$woo_array_cpt['name'] = 'book';
		$woo_array_cpt['args']['label'] = 'Books';
		$woo_array_cpt['args']['labels']['name'] = __('Books');
		$woo_array_cpt['args']['labels']['singular_name'] = __('Book');
		$woo_array_cpt['args']['labels']['add_new'] =  __('Add New');
		$woo_array_cpt['args']['labels']['add_new_item'] = __('Add New '.$woo_array_cpt['args']['label']);
		$woo_array_cpt['args']['labels']['edit_item'] = __('Edit '.$woo_array_cpt['args']['label']);
		$woo_array_cpt['args']['labels']['new_item'] = __('New '.$woo_array_cpt['args']['label']);
		$woo_array_cpt['args']['labels']['view_item'] = !__('View '.$woo_array_cpt['args']['label']);
		$woo_array_cpt['args']['labels']['search_item'] = __('Search '.$woo_array_cpt['args']['label']);
		$woo_array_cpt['args']['labels']['not_found'] = __('No '.$woo_array_cpt['args']['label'].' found');
		$woo_array_cpt['args']['labels']['not_found_in_trash'] = __('No '.$woo_array_cpt['args']['label'].' found in Thrash');
		$woo_array_cpt['args']['labels']['parent_item_colon'] = __('Parent '.$woo_array_cpt['args']['label']);
		
		$woo_array_cpt['args']['description'] = 'A collection of books';
		$woo_array_cpt['args']['public'] = 1;
		$woo_array_cpt['args']['publicly_queryable'] = 1;
		$woo_array_cpt['args']['exclude_from_search'] = 0;
		$woo_array_cpt['args']['show_ui'] = 1;
		$woo_array_cpt['args']['capability_type'] =  'post';
		$woo_array_cpt['args']['hierarchical'] = 0;
		$woo_array_cpt['args']['supports'] = array();
		    array_push($woo_array_cpt['args']['supports'],'title');
		    array_push($woo_array_cpt['args']['supports'],'editor');
		    array_push($woo_array_cpt['args']['supports'],'author');
		    array_push($woo_array_cpt['args']['supports'],'thumbnail');
		    array_push($woo_array_cpt['args']['supports'],'excerpt');
		    array_push($woo_array_cpt['args']['supports'],'trackbacks');
		    // array_push($woo_array_cpt['args']['supports'],'custom-fields');
		    array_push($woo_array_cpt['args']['supports'],'comments');
		    array_push($woo_array_cpt['args']['supports'],'revisions');
		    // array_push($woo_array_cpt['args']['supports'],'page-attributes');
		$woo_array_cpt['args']['register_meta_box_cb'] = '';
		
		$woo_array_cpt['args']['taxonomies'] = array();
		    array_push($woo_array_cpt['args']['taxonomies'],'book_authors');
		    array_push($woo_array_cpt['args']['taxonomies'],'book_genres');
		    array_push($woo_array_cpt['args']['taxonomies'],'book_publishers');
		    array_push($woo_array_cpt['args']['taxonomies'],'post_tag');
		    // add more here
		    	
		$woo_array_cpt['args']['menu_position'] = 20;
		$woo_array_cpt['args']['menu_icon'] = null;
		$woo_array_cpt['args']['permalink_epmask'] = 'EP_PERMALINK';
		
		$woo_array_cpt['args']['rewrite'] =array();
			$woo_array_cpt['args']['rewrite']['slug'] = $woo_array_cpt['name'];
			$woo_array_cpt['args']['rewrite']['with_front'] = 1;
		
		$woo_array_cpt['args']['query_var'] = 1;
		$woo_array_cpt['args']['can_export'] = 1;
		$woo_array_cpt['args']['show_in_nav_menus'] = 1;
		
		array_push($woo_array_cpt_all,$woo_array_cpt);
		
		// If we're trying to reset the data, reset the data.
		
		if ( $reset ) {
			
			update_option( 'woo_content_builder_cpt', $woo_array_cpt_all );
		
		} else {
		
			// If there is data present, don't add the default data.
		
			// Get the existing CPTs.
			$existing_cpt_data = get_option( 'woo_content_builder_cpt' );
			
			if ( is_array( $existing_cpt_data ) && count( $existing_cpt_data ) ) {
			
				$cpt_tokens = array();
				
				foreach ( $existing_cpt_data as $e ) {
				
					$cpt_tokens[] = $e['name'];
				
				} // End FOREACH Loop
				
				// Check the CPT names we want to add and, if they're not there, add them.
				
				foreach ( $woo_array_cpt_all as $cpt ) {
				
					if ( in_array( $cpt['name'], $cpt_tokens ) ) {} else {
					
						$existing_cpt_data[] = $cpt;
					
					} // End IF Statement
				
				} // End FOREACH Loop
				
				$updated_cpt_data = $existing_cpt_data; // Fresh variable name to preserve code understanding.
				
			} else {
			
				// Check the CPT names we want to add and, if they're not there, add them.
				
				foreach ( $woo_array_cpt_all as $cpt ) {
				
					if ( in_array( $cpt['name'], $cpt_tokens ) ) {} else {
					
						$existing_cpt_data[] = $cpt;
					
					} // End IF Statement
				
				} // End FOREACH Loop
				
				$updated_cpt_data = $existing_cpt_data; // Fresh variable name to preserve code understanding.
			
			} // End IF Statement
		
			update_option('woo_content_builder_cpt',$updated_cpt_data);
		
			/*
			if ( in_array( 'woo_content_builder_cpt', $_existing_fields ) ) {} else {
			
				update_option('woo_content_builder_cpt',$woo_array_cpt_all);
			
			} // End IF Statement
			*/
		
		} // End IF Statement
		
		/* CUSTOM TAXONOMIES */
		
		$woo_array_ctx = array();
		
		// Locations
		$woo_array_ctx['name'] = 'book_authors';
		
		$woo_array_ctx['object_type'] = array();
			$ctx_item_name = 'book';
		    array_push($woo_array_ctx['object_type'],$ctx_item_name);
		
		$woo_array_ctx['args']['label'] = __('Book - Author');
		$woo_array_ctx['args']['labels']['name'] =  __('Book - Authors');
		$woo_array_ctx['args']['labels']['singular_name'] = __('Book Author');
		
		
		$woo_array_ctx['args']['labels']['search_items'] = __('Search ').$woo_array_ctx['args']['label'];
		
		$woo_array_ctx['args']['labels']['popular_items'] = __('Popular ').$woo_array_ctx['args']['label'];
		$woo_array_ctx['args']['labels']['all_items'] = __('All ').$woo_array_ctx['args']['label'];
		$woo_array_ctx['args']['labels']['parent_item'] = __('Parent ').$woo_array_ctx['args']['labels']['singular_name'];
		$woo_array_ctx['args']['labels']['parent_item_colon'] = __('Parent '.$woo_array_ctx['args']['labels']['singular_name'].':');
		$woo_array_ctx['args']['labels']['edit_item'] = __('Edit ').$woo_array_ctx['args']['labels']['singular_name'];
		$woo_array_ctx['args']['labels']['update_item'] = __('Update ').$woo_array_ctx['args']['labels']['singular_name'];
		$woo_array_ctx['args']['labels']['add_new_item'] = __('Add New ').$woo_array_ctx['args']['labels']['singular_name'];
		$woo_array_ctx['args']['labels']['new_item_name'] = __('New '.$woo_array_ctx['args']['labels']['singular_name'].' Name');
		$woo_array_ctx['args']['labels']['separate_items_with_commas'] = __('Separate locations with commas');
		$woo_array_ctx['args']['labels']['add_or_remove_items'] = __('Add or remove authors');
		$woo_array_ctx['args']['labels']['choose_from_most_used'] = __('Choose from the most used authors');
		
		
		$woo_array_ctx['args']['public'] = 1;
		$woo_array_ctx['args']['hierarchical'] = 1;
		$woo_array_ctx['args']['show_ui'] = 1;
		$woo_array_ctx['args']['show_in_nav_menus'] = 1;
		$woo_array_ctx['args']['show_tagcloud'] = 1;
	
		$woo_array_ctx['args']['rewrite'] =array();
			$woo_array_ctx['args']['rewrite']['slug'] = $woo_array_ctx['name'];
			$woo_array_ctx['args']['rewrite']['with_front'] = 1;
		
		$woo_array_ctx['args']['query_var'] = 1;
		
		$woo_array_ctx['args']['update_count_callback'] = '';
		
		array_push($woo_array_ctx_all,$woo_array_ctx);
		
		// Listing Types
		$woo_array_ctx['name'] = 'book_genres';
		
		$woo_array_ctx['object_type'] = array();
			$ctx_item_name = 'book';
		    array_push($woo_array_ctx['object_type'],$ctx_item_name);
		
		$woo_array_ctx['args']['label'] = __('Book - Genres');
		$woo_array_ctx['args']['labels']['name'] =  __('Book - Genres');
		$woo_array_ctx['args']['labels']['singular_name'] = __('Book - Genre');
		
		
		$woo_array_ctx['args']['labels']['search_items'] = __('Search ').$woo_array_ctx['args']['label'];
		
		$woo_array_ctx['args']['labels']['popular_items'] = __('Popular ').$woo_array_ctx['args']['label'];
		$woo_array_ctx['args']['labels']['all_items'] = __('All ').$woo_array_ctx['args']['label'];
		$woo_array_ctx['args']['labels']['parent_item'] = __('Parent ').$woo_array_ctx['args']['labels']['singular_name'];
		$woo_array_ctx['args']['labels']['parent_item_colon'] = __('Parent '.$woo_array_ctx['args']['labels']['singular_name'].':');
		$woo_array_ctx['args']['labels']['edit_item'] = __('Edit ').$woo_array_ctx['args']['labels']['singular_name'];
		$woo_array_ctx['args']['labels']['update_item'] = __('Update ').$woo_array_ctx['args']['labels']['singular_name'];
		$woo_array_ctx['args']['labels']['add_new_item'] = __('Add New ').$woo_array_ctx['args']['labels']['singular_name'];
		$woo_array_ctx['args']['labels']['new_item_name'] = __('New '.$woo_array_ctx['args']['labels']['singular_name'].' Name');
		$woo_array_ctx['args']['labels']['separate_items_with_commas'] = __('Separate listings types with commas');
		$woo_array_ctx['args']['labels']['add_or_remove_items'] = __('Add or remove genres');
		$woo_array_ctx['args']['labels']['choose_from_most_used'] = __('Choose from the most used genres');
		
		
		$woo_array_ctx['args']['public'] = 1;
		$woo_array_ctx['args']['hierarchical'] = 0;
		$woo_array_ctx['args']['show_ui'] = 1;
		$woo_array_ctx['args']['show_in_nav_menus'] = 1;
		$woo_array_ctx['args']['show_tagcloud'] = 1;
	
		$woo_array_ctx['args']['rewrite'] =array();
			$woo_array_ctx['args']['rewrite']['slug'] = $woo_array_ctx['name'];
			$woo_array_ctx['args']['rewrite']['with_front'] = 1;
		
		$woo_array_ctx['args']['query_var'] = 1;
		
		$woo_array_ctx['args']['update_count_callback'] = '';
		
		array_push($woo_array_ctx_all,$woo_array_ctx);
		
		// Listing Features
		$woo_array_ctx['name'] = 'book_publishers';
		
		$woo_array_ctx['object_type'] = array();
			$ctx_item_name = 'book';
		    array_push($woo_array_ctx['object_type'],$ctx_item_name);
		
		$woo_array_ctx['args']['label'] = __('Book - Publishers');
		$woo_array_ctx['args']['labels']['name'] =  __('Book - Publishers');
		$woo_array_ctx['args']['labels']['singular_name'] = __('Book - Publisher');
		
		
		$woo_array_ctx['args']['labels']['search_items'] = __('Search ').$woo_array_ctx['args']['label'];
		
		$woo_array_ctx['args']['labels']['popular_items'] = __('Popular ').$woo_array_ctx['args']['label'];
		$woo_array_ctx['args']['labels']['all_items'] = __('All ').$woo_array_ctx['args']['label'];
		$woo_array_ctx['args']['labels']['parent_item'] = __('Parent ').$woo_array_ctx['args']['labels']['singular_name'];
		$woo_array_ctx['args']['labels']['parent_item_colon'] = __('Parent '.$woo_array_ctx['args']['labels']['singular_name'].':');
		$woo_array_ctx['args']['labels']['edit_item'] = __('Edit ').$woo_array_ctx['args']['labels']['singular_name'];
		$woo_array_ctx['args']['labels']['update_item'] = __('Update ').$woo_array_ctx['args']['labels']['singular_name'];
		$woo_array_ctx['args']['labels']['add_new_item'] = __('Add New ').$woo_array_ctx['args']['labels']['singular_name'];
		$woo_array_ctx['args']['labels']['new_item_name'] = __('New '.$woo_array_ctx['args']['labels']['singular_name'].' Name');
		$woo_array_ctx['args']['labels']['separate_items_with_commas'] = __('Separate listings features with commas');
		$woo_array_ctx['args']['labels']['add_or_remove_items'] = __('Add or remove listing publishers');
		$woo_array_ctx['args']['labels']['choose_from_most_used'] = __('Choose from the most used publishers');
		
		
		$woo_array_ctx['args']['public'] = 1;
		$woo_array_ctx['args']['hierarchical'] = 0;
		$woo_array_ctx['args']['show_ui'] = 1;
		$woo_array_ctx['args']['show_in_nav_menus'] = 1;
		$woo_array_ctx['args']['show_tagcloud'] = 1;
	
		$woo_array_ctx['args']['rewrite'] =array();
			$woo_array_ctx['args']['rewrite']['slug'] = $woo_array_ctx['name'];
			$woo_array_ctx['args']['rewrite']['with_front'] = 1;
		
		$woo_array_ctx['args']['query_var'] = 1;
		
		$woo_array_ctx['args']['update_count_callback'] = '';
		
		array_push($woo_array_ctx_all,$woo_array_ctx);
		
		// If we're trying to reset the data, reset the data.
		
		if ( $reset ) {
		
		 	update_option( 'woo_content_builder_ctx', $woo_array_ctx_all );
		
		} else {
		
			// If there is data present, don't add the default data.
			
			// Get the existing CTXs.
			$existing_ctx_data = get_option( 'woo_content_builder_ctx' );
			
			if ( is_array( $existing_ctx_data ) && count( $existing_ctx_data ) ) {
			
				$ctx_tokens = array();
				
				foreach ( $existing_ctx_data as $e ) {
				
					$ctx_tokens[] = $e['name'];
				
				} // End FOREACH Loop
				
				// Check the CPT names we want to add and, if they're not there, add them.
				
				foreach ( $woo_array_ctx_all as $ctx ) {
				
					if ( in_array( $ctx['name'], $ctx_tokens ) ) {} else {
					
						$existing_ctx_data[] = $ctx;
					
					} // End IF Statement
				
				} // End FOREACH Loop
				
				$updated_ctx_data = $existing_ctx_data; // Fresh variable name to preserve code understanding.
				
			} else {
			
				// Check the CPT names we want to add and, if they're not there, add them.
				
				foreach ( $woo_array_ctx_all as $ctx ) {
				
					if ( in_array( $ctx['name'], $ctx_tokens ) ) {} else {
					
						$existing_ctx_data[] = $ctx;
					
					} // End IF Statement
				
				} // End FOREACH Loop
				
				$updated_ctx_data = $existing_ctx_data; // Fresh variable name to preserve code understanding.
			
			} // End IF Statement
			
			update_option('woo_content_builder_ctx',$updated_ctx_data);
			
			/*
			if ( in_array( 'woo_content_builder_ctx', $_existing_fields ) ) {} else {
			
				update_option('woo_content_builder_ctx',$woo_array_ctx_all);
			
			} // End IF Statement
			*/
		
		} // End IF Statement
		
		// update_option('woo_content_builder_ctx',$woo_array_ctx_all);
		
		/* CUSTOM FIELDS */
		
		$woo_array_cmb = array();
		
		$options_array = array();
		
		//image
		$woo_array_cmb 	= array (	"name" 		=> 	"image",
									"std" 		=> 	"",
									"label" 	=> 	"Upload Image",
									"type" 		=> 	"upload",
									"desc" 		=> 	"Upload your listings image here",
									"options" 	=> 	$options_array,
									"cpt"		=>	array( "book"	=>	"true" )
									);
		
		array_push($woo_array_cmb_all,$woo_array_cmb);
		
		//price
		$woo_array_cmb 	= array (	"name" 		=> 	"price",
									"std" 		=> 	"",
									"label" 	=> 	"Price in $",
									"type" 		=> 	"text",
									"desc" 		=> 	"Enter the price of the listing excluding the currency symbol.",
									"options" 	=> 	$options_array,
									"cpt"		=>	array( "book" => "true" )
									);
		
		array_push($woo_array_cmb_all,$woo_array_cmb);
		
		// Profile Image
		$woo_array_cmb 	= array (	"name" 		=> 	"profile-photo",
									"std" 		=> 	"",
									"label" 	=> 	"Author Profile Photo",
									"type" 		=> 	"upload",
									"desc" 		=> 	"A profile image",
									"options" 	=> 	$options_array,
									"cpt"		=>	array( "TAXONOMY" => "true" ), 
									"ctx" 		=> 	array(
															"book_authors" => "true", 
															"book_publishers" => "true"
														 )
									);
		
		array_push($woo_array_cmb_all,$woo_array_cmb);
		
		// Bio
		$woo_array_cmb 	= array (	"name" 		=> 	"author-bio",
									"std" 		=> 	"",
									"label" 	=> 	"Biography",
									"type" 		=> 	"textarea",
									"desc" 		=> 	"A brief biography",
									"options" 	=> 	$options_array,
									"cpt"		=>	array( "TAXONOMY" => "true" ), 
									"ctx" 		=> 	array(
															"book_authors" => "true", 
															"book_publishers" => "true"
														 )
									);
		
		array_push($woo_array_cmb_all,$woo_array_cmb);
		
		// Website URL
		$woo_array_cmb 	= array (	"name" 		=> 	"website-url",
									"std" 		=> 	"",
									"label" 	=> 	"Website URL",
									"type" 		=> 	"text",
									"desc" 		=> 	"The website URL (don't forget the <code>http://</code>)",
									"options" 	=> 	$options_array,
									"cpt"		=>	array( "TAXONOMY" => "true" ), 
									"ctx" 		=> 	array(
															"book_authors" => "true", 
															"book_publishers" => "true"
														 )
									);
		
		array_push($woo_array_cmb_all,$woo_array_cmb);
		
		// Affiliate Link
		$woo_array_cmb 	= array (	"name" 		=> 	"affiliate-link",
									"std" 		=> 	"",
									"label" 	=> 	"Affiliate Link",
									"type" 		=> 	"text",
									"desc" 		=> 	"The affiliate link (don't forget the <code>http://</code>)",
									"options" 	=> 	$options_array,
									"cpt"		=>	array( "book" => "true" )
									);
		
		array_push($woo_array_cmb_all,$woo_array_cmb);
		
		// Add filter so external sources can add fields.
		$woo_array_cmb_all = apply_filters( 'woo_content_builder_fields_install', $woo_array_cmb_all );
		
		// If we're trying to reset the data, reset the data.
		
		if ( $reset ) {
		
			update_option( 'woo_content_builder_cmb', $woo_array_cmb_all );
			update_option( 'woo_custom_template', $woo_array_cmb_all );
		
		} else {
		
			// $woo_content_builder_cmb = get_option( 'woo_content_builder_cmb' );
			$woo_custom_template = get_option( 'woo_custom_template' );
		
			$woo_custom_template_backup = get_option( 'woo_content_builder_cmb' );
		
			/*
			// Always favour the backup, if one exists.
			if ( $woo_custom_template_backup ) {
			
				$woo_custom_template = $woo_custom_template_backup;
			
			} // End IF Statement
			*/
			
			// If there is data present, attempt to merge new data with existing data.
			
			if ( count( $woo_array_cmb_all ) && is_array( $woo_array_cmb_all ) ) {
			
				// If no custom fields have been created with the Content Builder, we can safely add all of ours to the array.
				if ( count( $woo_custom_template ) == 0 ) {
				
					$woo_custom_template = $woo_array_cmb_all;
				
				} else {
			
					// Get the existing CMBs.
					$existing_cmb_data = $woo_custom_template;
					
					if ( is_array( $existing_cmb_data ) && count( $existing_cmb_data ) ) {
					
						$cmb_tokens = array();
						
						foreach ( $existing_cmb_data as $e ) {
						
							$cmb_tokens[] = $e['name'];
						
						} // End FOREACH Loop
						
						// Check the CMB names we want to add and, if they're not there, add them.
						
						foreach ( $woo_array_cmb_all as $cmb ) {
						
							if ( in_array( $cmb['name'], $cmb_tokens ) ) {
							
								// Check if all the necessary CPTs are present for this custom field.
								
								foreach ( $existing_cmb_data as $k => $e ) {
								
									foreach ( $cmb['cpt'] as $c => $v ) {
									
										if ( ! in_array( $c, array_keys( $e['cpt'] ) ) && $e['name'] == $cmb['name'] ) {
										
											$existing_cmb_data[$k]['cpt'][$c] = 'true';
										
										} // End IF Statement
									
									} // End FOREACH Loop
								
								} // End FOREACH Loop
								
								// Check if all the necessary CTXs are present for this custom field.
								
								foreach ( $existing_cmb_data as $k => $e ) {
								
									foreach ( $cmb['ctx'] as $c => $v ) {
									
										if ( ! in_array( $c, array_keys( $e['ctx'] ) ) && $e['name'] == $cmb['name'] ) {
										
											$existing_cmb_data[$k]['ctx'][$c] = 'true';
										
										} // End IF Statement
									
									} // End FOREACH Loop
								
								} // End FOREACH Loop
							
							} else {
							
								$existing_cmb_data[] = $cmb;
							
							} // End IF Statement
						
						} // End FOREACH Loop
						
						$updated_cmb_data = $existing_cmb_data; // Fresh variable name to preserve code understanding.
						
						$woo_custom_template = $updated_cmb_data;
						
						/*
						// DEBUG
						print_r( '<xmp>' );
							print_r( $woo_custom_template );
						print_r( '</xmp>' );
						
						die;
						*/
						
						update_option('woo_custom_template',$updated_cmb_data);
						update_option('woo_content_builder_cmb',$updated_cmb_data);
					
					} // End IF Statement
			
					/*
			
					$existing_fields = array();
			
					foreach ( $woo_custom_template as $i => $j ) {
					
						$existing_fields[] = $j['name'];
					
					} // End FOREACH Loop
			
					foreach ( $woo_array_cmb_all as $k => $v ) {
					
						// If the `name` exists and is equal to the name of the current item in the $woo_array_cmb_all,
						// don't do anything. Otherwise, add $j to the $woo_array_cmb_all array.
						
						// if ( in_array( $v['name'], $existing_fields ) ) {} else {
						
						// 	$woo_content_builder_cmb[] = $v;
						
						// } // End IF Statement
						
						if ( in_array( $v['name'], array_keys( $woo_custom_template ) ) ) {} else {
						
							$woo_custom_template[$v['name']] = $v;
						
						} // End IF Statement
					
					} // End FOREACH Loop
					
					*/
					
				} // End IF Statement
				
				update_option( 'woo_custom_template', $woo_custom_template ); // Content Builder-generated custom fields.
			
			} // End IF Statement
			
		} // End IF Statement
		
	} // End woo_listings_content_install()
?>