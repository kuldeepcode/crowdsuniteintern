jQuery(document).ready(function() {

	if ( jQuery( '.jCarousel .slides' ).length ) {
	
		jQuery( '.jCarousel .slides' ).jcarousel({
		
			auto: woo_jcarousel_settings.auto, 
			speed: woo_jcarousel_settings.speed, 
			visible: woo_jcarousel_settings.visible, 
			scroll: woo_jcarousel_settings.scroll
		
		});
	
	} // End IF Statement

});