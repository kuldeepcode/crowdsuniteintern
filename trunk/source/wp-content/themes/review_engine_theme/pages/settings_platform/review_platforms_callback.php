<?php
global $helper;
$reviews = get_comments( array( 'post_id'=>$platform_id ) );
foreach ( $reviews as $key => $review ) {
	if ( $review->comment_type != 'review' ) unset( $reviews[ $key ] );
}
usort( $reviews, "usort_coments");

$view_num_reviews = 7;
$pagination = (int)$_POST['pagination'];
$pagination = ( empty( $pagination ) )?1:$pagination;
$max_num_reviews = count( $reviews );

?>
<?php for ( $i = ( $pagination - 1 ) * $view_num_reviews; $i < $view_num_reviews*$pagination && $i < $max_num_reviews; ++$i ): $review=$reviews[ $i ];?>
<?php 
	$meta_review = get_comment_meta($review->comment_ID, 'tgt_review_data', true);
	$meta_rating = get_comment_meta($review->comment_ID, 'tgt_review_rating', true);
	$respond_review_id = (int)get_comment_meta($review->comment_ID, 'respond_review', true);
	if ( $respond_review_id > 0 ) {
		$respond_review = get_post( $respond_review_id );
		$respond_author = get_userdata( $respond_review->post_author );
		$respond_author_avatar = get_user_meta( $respond_review->post_author, 'avatar', true );
	}
	$meta_review['bottomline'] = $review->comment_content;
	$ratings = get_option('tgt_rating');
	$author_avatar = get_user_meta($review->user_id, 'avatar', true);
	$enable_like = get_option(SETTING_ENABLE_LIKE);
?>
<div class="row-fluid">
	<div class="row-fluid">
		<div class="span2">
			<div class="overall">
				<div class="orange_rate">
					<p class="rate"> <?php _e("User's Rating:", "re") ?> </p>
					<div class="vote_star" style="background:none; border:none; height:auto; margin:5px 0 5px 10px">
						<div class="star">
							<?php tgt_display_rating( $meta_rating['sum'], 'editor_rating_'.$review->comment_ID, true, 'star-disabled' );?>
						</div>
					</div>
				</div>
				<?php foreach( $meta_rating['rates'] as $rate_ID => $rate ):?>
				<div class="orange_rate2">
					<p class="rate"><?php echo $ratings[$rate_ID] ?></p>
					<div class="vote_star" style="background:none; border:none; height:auto; margin:5px 0 5px 10px">
						<div class="star">
							<?php tgt_display_rating( $rate, 'editor_rating_'.$review->comment_ID.'_'.$rate_ID, true, 'star-disabled' );?>
						</div>
					</div>
				</div>
				<?php endforeach;?>
			</div>
		</div>
		<div class="span8">
			<h3 class="review-title">  <?php echo $meta_review['title'] ?>  </h3>
			<p class="sum"><?php _e('WHAT DID YOU LIKE ABOUT THE WEBSITE:','re'); ?></p>
			<p class="review-content"><?php echo $meta_review['pro'] ?></p>
			<p class="sum"><?php _e("WHAT DIDN'T YOU LIKE ABOUT THE WEBSITE:",'re'); ?></p>
			<p class="review-content"><?php echo $meta_review['con'] ?></p>
			<?php if (!empty($review->comment_content)):?>
			<p class="sum"><?php _e('MY CROWDFUNDING CAMPAIGN:','re'); ?></p>
			<p class="review-content"><?php echo $helper->link($meta_review['bottomline'],$meta_review['bottomline'],array('target' => '_blank'));?></p>
			<?php endif;?>
			<?php if ( $enable_like ):?>
			<div class="likediv" id="vote_review_<?php echo $review->comment_ID ?>">
				Do you think this review is helpful?
				<span class="like-count"><?php echo get_like_count( $review->comment_ID, 1 ); ?></span>
				<div class="thumb-up" style="display: inline">
					<?php echo $helper->image('thumb_up.png', 'like', array('title' => the_like_count( get_like_count( $review->comment_ID, 1 ) ) ) );?>
				</div>
				<span class="dislike-count"><?php echo get_like_count( $review->comment_ID, 0 ); ?></span>
				<div class="thumb-down" style="display: inline">
					<?php echo $helper->image('thumb_down.png', 'like', array('title' => the_dislike_count( get_like_count( $review->comment_ID, 0 ) ) ) );?>
				</div>
			</div>
			<?php endif;?>
		</div>
		<div class="span2">
			<div class="overall2">
				<?php
				if (!empty($author_avatar)){
					echo $helper->link(  $helper->image(URL_UPLOAD . '/' . $author_avatar, 'dailywp.com', array('style' => "height:80px")) , get_author_posts_url($review->user_id));
				} elseif($review->user_id > 1)
						echo $helper->link( $helper->image(TEMPLATE_URL . '/images/no_avatar.gif', 'dailywp.com', array('style' => "height:80px")), get_author_posts_url($review->user_id) );
				else
						echo $helper->image(TEMPLATE_URL . '/images/no_avatar.gif', 'dailywp.com', array('style' => "height:80px"));
				?>
				<p><?php _e('REVIEWED BY:','re') ?><br/>
					<?php
					if($review->user_id > 1)
						echo $helper->link( $review->comment_author, get_author_posts_url( $review->user_id ) );
					else
						echo $review->comment_author;
					?><br />
					<span> <?php echo mysql2date( 'M j,Y', $review->comment_date );?> </span>
				</p>
			</div>
		</div>
	</div>
	<div class="row-fluid">
		<?php if ( $respond_review_id ):?>
		<div class="span10 offset2 respond_review">
			<div class="row-fluid">
				<div class="span2">
					<div class="overall2">
						<?php
						if (!empty($respond_author_avatar)){
							echo $helper->link(  $helper->image(URL_UPLOAD . '/' . $respond_author_avatar, 'dailywp.com', array('style' => "height:80px")) , get_author_posts_url($respond_review->post_author));
						} elseif($review->user_id > 1)
								echo $helper->link( $helper->image(TEMPLATE_URL . '/images/no_avatar.gif', 'dailywp.com', array('style' => "height:80px")), get_author_posts_url($respond_review->post_author) );
						else
								echo $helper->image(TEMPLATE_URL . '/images/no_avatar.gif', 'dailywp.com', array('style' => "height:80px"));
						?>
						<p><?php _e('COMMENT BY:','re') ?><br/>
							<?php
							if( $respond_review->post_author > 1 )
								echo $helper->link( $respond_author->display_name, get_author_posts_url( $respond_review->post_author ) );
							else
								echo $respond_author->display_name;
							?><br />
							<span> <?php echo mysql2date( 'M j,Y', $respond_review->post_date );?> </span>
						</p>
					</div>
				</div>
				<div class="span8 respond_review_des">
					<?php echo $respond_review->post_content ?>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span12 text-right">
					<a class="btn" href="#respondReview" data-toggle="modal" data-review-id="<?php echo $review->comment_ID?>">Edit Respond</a>
				</div>
			</div>
		</div>
		<?php else:?>
		<div class="span10 offset2 text-right">
			<a class="btn" href="#respondReview" data-toggle="modal" data-review-id="<?php echo $review->comment_ID?>">Respond to Review</a>
		</div>
		<?php endif;?>
	</div>
</div>
<?php endfor;?>
<?php ajax_platforms_manage::pagination( $pagination, ceil( $max_num_reviews/$view_num_reviews ) );?>