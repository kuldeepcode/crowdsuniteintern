<?php
$rating = array(
	PRODUCT_RATING 					=> get_post_meta( $platform_id , PRODUCT_RATING, true) 				? get_post_meta( $platform_id , PRODUCT_RATING, true) : 0 ,
	PRODUCT_RATING_COUNT 			=> get_post_meta( $platform_id , PRODUCT_RATING_COUNT, true) 			? get_post_meta( $platform_id , PRODUCT_RATING_COUNT, true) : 0,
);
$ratings_ = get_option('tgt_rating');
if ( !is_array( $sum_ar = get_post_meta( $platform_id, 'tgt_review_rating_average',true ) ) ) {
	$sum_ar = update_post_rating_average( $platform_id, true );
}
?>
<?php 
if ( class_exists('CrowdsuniteRatingAPI') ){
	$ZeroClipboardSwfUlr = CrowdsuniteRatingAPI::ZeroClipboard(false);
}
?>
<div class="row-fluid">
	<div class="span6">
		<table class="ratings_product">
			<?php foreach( $sum_ar as $rate_ID => $rate ): ?>
			<tr>
				<td width="50%"><label><?php echo $ratings_[$rate_ID] ?>:</label></td>
				<td width="40%">
					<input type="text" value="<?php echo $rate?>" readonly="readonly"/>
				</td>
			</tr>
			<?php endforeach;?>
			<tr>
				<td width="50%"><label><?php _e("Overall Rating", "re") ?>:</label></td>
				<td width="40%">
					<input type="text" value="<?php echo $rating[PRODUCT_RATING];?>" readonly="readonly"/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="rating_count"><?php _e('Rating Count') ?>:</label>
				</td>
				<td width="70%">
					<input type="text" name="rating_info[<?php echo PRODUCT_RATING_COUNT ?>]" id="<?php echo PRODUCT_RATING_COUNT ?>" value="<?php echo $rating[PRODUCT_RATING_COUNT] ?>" readonly="readonly"/>
				</td>
			</tr>
		</table>
	</div>
	<div class="span6">
<?php if ( class_exists('CrowdsuniteRatingAPI') ):?>
		<p>To display rating on your page click on the button below to get started</p>
		<a class="btn" href="#ratingAPI" data-toggle="modal">Rating Widget</a>
<?php endif;?>
	</div>
</div>
<div class="container_reviews"></div>
<div id="respondReview" class="modal hide fade">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3>Respond Review</h3>
	</div>
	<form id="submit_respondReview" action="" method="post" style="margin-bottom: 0px;">
		<div class="modal-body">
			<input type="hidden" name="review_id"/>
			<textarea name="artDescription" id="respondReviewDescription" class="input-block-level" rows="5" placeholder="Respond Review" required></textarea>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn" data-dismiss="modal">Close</button>
			<input type="submit" name="submit" class="btn btn-primary" value="Post" />
		</div>
	</form>
</div>
<?php if ( class_exists('CrowdsuniteRatingAPI') ):?>
<div id="ratingAPI" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myModalLabel">Rating Widget</h3>
	</div>
	<div class="modal-body">
		<div class="box_reply" style="margin-top:0; width: 96%;">
			<div class="box_reply" style="margin-top:0;width: 43%;">
				<form id="ratingAPIOptions">
					<p style="font-weight: bold;">Select Option</p>
					Rating type:
					<div style="padding: 0 0 10px 10px;">
						<input type="radio" name="rating_types" checked value="0">Overall Rating<br>
						<input type="radio" name="rating_types" value="1">Customer Support<br>
						<input type="radio" name="rating_types" value="2">Funding from Strangers<br>
						<input type="radio" name="rating_types" value="3">Ease of Use
					</div>
					<input type="checkbox" name="display_text_rating" value="4">Display the digit  value of the rating<br>
					<input type="checkbox" name="display_text_reviews" value="8">Display the reviews amount
				</form>
			</div>
			<div class="box_reply code_script" style="margin:0 0 0 10px;width:46%; overflow-y: auto;height: 80px">
				<p style="font-weight: bold;">You need to include this anywhere on the page only once, even if showing multiple ratings:(<a id="copy-button-script" data-clipboard-text="">copy</a>)<p>
				<code style="white-space: pre;">
				</code>
			</div>
			<div class="box_reply code" style="margin:0 0 0 10px;width:46%; overflow-y: auto;height: 81px">
				<p style="font-weight: bold;">Paste this code where you want the rating to be displayed:(<a id="copy-button" data-clipboard-text="">copy</a>)<p>
				<code style="white-space: pre;">
				</code>
			</div>
			<div class="box_reply preview" style="margin-top:20px;">
			</div>
		</div>
	</div>
	<script type="text/javascript">
	jQuery(document).ready(function($){
		var clip_script = new ZeroClipboard(document.getElementById("copy-button-script"), { moviePath: "<?php echo $ZeroClipboardSwfUlr;?>" });
		clip_script.on("load", function(client) {
			client.on("complete", function(client, args) {
				alert("Copy complete");
			});
		});
		var clip = new ZeroClipboard(document.getElementById("copy-button"), { moviePath: "<?php echo $ZeroClipboardSwfUlr;?>" });
		clip.on("load", function(client) {
			client.on("complete", function(client, args) {
				alert("Copy complete");
			});
		});
		$('#ratingAPIOptions input').on('change',function(){
			var option = 0;
			$('#ratingAPIOptions input:checked').each(function(){
				option = option + parseInt($(this).val());
			});
			var url = '<?php bloginfo('url')?>/wp-admin/admin-ajax.php?action=apiRating&api_key=<?php echo urlencode( base64_encode( get_current_user_id() ) );?>&m=1'
			var codeApi_script = '\<script type="text/javascript" src="'+url+'">\<\/script>';
			var codeApi =  '<span class="crowdsunite_rating_product" data-pid="<?php echo $platform_id;?>" data-option="'+option+'"></span>';

			$('#ratingAPI .box_reply.code_script code').text(codeApi_script);
			$('#copy-button-script').data('clipboard-text',codeApi_script);
			$('#copy-button-script').attr('data-clipboard-text',codeApi_script);

			$('#ratingAPI .box_reply.code code').text(codeApi);
			$('#copy-button').data('clipboard-text',codeApi);
			$('#copy-button').attr('data-clipboard-text',codeApi);

			var script=document.createElement('script');
			script.type='text/javascript';
			script.src=url+'&d=1';
			$('#ratingAPI .box_reply.preview').html('Preview:<br/>'+codeApi);
			$('#ratingAPI .box_reply.preview').append(script);
		});
		$('a[href=#ratingAPI]').on('click',function(){
			$('#ratingAPIOptions input').first().trigger('change');
		});
	});
	</script>
</div>
<?php endif;?>
<script type="text/javascript">
gets_review_platforms(1);
</script>