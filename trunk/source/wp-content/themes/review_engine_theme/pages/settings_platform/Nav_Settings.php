<?php 
$can_CrowdsUnite = get_option( 'can_CrowdsUnite', array() );
?>
<div class="for_mesage_alert clearfix"></div>
<div class="row-fluid settings_content">
	<div class="span12">
		<label><input type="checkbox" id="notify_reviews" <?checked( get_user_meta( $user_id, 'tgt_mailing_new_review_notif_'.$platform_id, true ), 1 );?> />
		Notify about new Reviews.
		</label>
		<label><input type="checkbox" id="notify_article" <?checked( get_user_meta( $user_id, 'tgt_mailing_new_article_notif_'.$platform_id, true ), 1 );?> />
		Notify about new Articles.
		</label>
		<label>
			<select id="can_CrowdsUnite">
				<option value="0" <?php selected( !isset( $can_CrowdsUnite[ $platform_id ] ) );?> >No</option>
				<option value="1" <?php selected( isset( $can_CrowdsUnite[ $platform_id ] ) );?> >Yes</option>
			</select>
			Can CrowdsUnite get in touch with campaigns on your site
		</label>
		<label>
			<select id="newsletter_mailchimp">
				<option value="0">Unsubcribed</option>
				<option value="1" <?php selected( get_user_meta( $user_id, 'newsletter_mailchimp', true ), 1 );?> >Subscribed</option>
			</select>
		Subscribe/Unsubscribe to newsletter. (We will send at most once a month)
		</label>
		<p class="muted">* Please check your email after subscription. A confirmation email will be sent to your email account.</p>
	</div>
</div>
<div class="row-fluid btn_container">
	<div class="span12 text-right">
		<a class="btn save-changes" data-action="edit_notify_settings">Save Changes</a>
	</div>
</div>