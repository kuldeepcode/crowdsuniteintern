<?php
global $wp_query, $post;
$post = get_post( $platform_id );

$wp_query = new WP_Query();

add_action('posts_where', 'where_article_of_product');
add_action('posts_join', 'join_article_of_product' );
add_action('posts_orderby', 'order_article_of_product');

$wp_query = new WP_Query(array( 'posts_per_page' => (int)$_POST['article_view'], 'paged' => (int)$_POST['pagination'] ) );

remove_action('posts_where', 'where_article_of_product');
remove_action('posts_join', 'join_article_of_product' );
remove_action('posts_orderby', 'order_article_of_product');

?>
<a class="btn" href="#postArticle" data-toggle="modal"> Post Article </a>
<?php
if ( have_posts() ):
	while(have_posts()):
		the_post();
		$art_id = get_the_ID();
		$art_url = get_post_meta( $art_id, 'AUTO_CREATE_ARTICLE_HTTP_REFERER', true );
		$art_url_host = parse_url( $art_url );
		$user_info = get_userdata($post->post_author);
		?>
		<div class="articlebox">
			<div class="row-fluid">
				<div class="span7">
					<a href="<?php the_permalink(); ?>"><h4 style="display:inline;"><?php echo $post->post_title ?></h4></a>
				</div>
				<div class="span5 text-right">
				<?php if ( !empty( $art_url_host['host'] ) ):?>
					<a href="<?php echo $art_url?>" target="_blank"><?php echo $art_url_host['host']?></a>
				<?php endif;?>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span12">
					<p><?php echo get_the_content() ?></p>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span4">
					<a style="color:#1793D3;" href="<?php the_permalink(); ?>"><?php _e('Comments','re'); ?>  (<?php echo $post->comment_count; ?>)</a>
				</div>
				<div class="span4 text-center">Added by: <?php echo $user_info->display_name;?></div>
				<div class="span4 text-right"><?php echo the_time(get_option('date_format')); ?></div>
			</div>
		</div>
		<?php
	endwhile;
else:?>
	<div class="articlebox">
		<?php _e('This product has no article yet','re'); ?>
	</div>
<?php
endif;?>
<?php ajax_platforms_manage::pagination( $_POST['pagination'], $wp_query->max_num_pages );?>
