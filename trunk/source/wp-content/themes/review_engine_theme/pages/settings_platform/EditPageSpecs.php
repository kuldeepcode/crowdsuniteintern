<?php
$specs_arr = get_all_data_spec_by_cat_id_tgt( $platform_cat_id );
$specs = get_post_meta( $platform_id, 'tgt_product_spec', true );
$fields_specification = get_option( 'additional_fields_specification', array() );
?>
<div class="for_mesage_alert clearfix"></div>
<?php foreach ($specs_arr as $spec):?>
<?php $odd_row = '';?>

<h2><?php echo $spec['group_name'];?></h2>

<?php foreach ($spec['value'] as $spec_fields):?>

<?php  
	$selector_spec_field = 'g_' .$spec['spec_id']. '_' .$spec_fields['spec_value_id'];
	$readonly = ( isset($fields_specification[ $spec_fields['spec_value_id'] ]['readonly'] ) && $fields_specification[ $spec_fields['spec_value_id'] ]['readonly'] == $selector_spec_field )?true:false;
	$odd_row = empty( $odd_row )?' odd-row':'';
?>

<div class="row-fluid<?php echo $odd_row;?>">
	<div class="span5 spec_field_name">
		<label for="<?php echo $selector_spec_field;?>">
			<?php echo  str_replace( array('[a]','[/a]'),array('<a href="'.@$fields_specification[ $spec_fields["spec_value_id"] ]["value_url"].'" target="_blank">','</a>'), $spec_fields['value_name'] ); ?>
			<?php if ( !empty( $fields_specification[ $spec_fields['spec_value_id'] ]['value_tooltip'] ) ):?>
			<a href="#" rel="tooltip" data-placement="right" data-title="<?php echo $fields_specification[ $spec_fields["spec_value_id"] ]["value_tooltip"]?>" data-original-title="" title="">
				<i class=" icon-question-sign"></i>
			</a>
			<?php endif;?>
		</label>
	</div>
	<div class="span7 spec_field_value">
		<textarea type="text" id="<?php echo $selector_spec_field;?>" name="<?php echo $selector_spec_field;?>" <?php echo ( $readonly )?'readonly':'';?>><?php echo ( isset( $specs[$selector_spec_field] ) )?$specs[$selector_spec_field]:''; ?></textarea>
	</div>
</div>
<?php endforeach;endforeach;?>
<div class="row-fluid btn_container">
	<div class="offset2 span10 text-right">
		<a class="btn save-changes" data-action="edit_page_specs">Save Changes</a>
	</div>
</div>