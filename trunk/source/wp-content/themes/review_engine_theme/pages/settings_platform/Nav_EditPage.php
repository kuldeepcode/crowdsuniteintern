<div class="tabbable tabs-left">
<ul class="nav nav-tabs">
	<li class="active"><a href="#EditPageGeneral" data-toggle="tab">General</a></li>
	<li><a href="#EditPageMedia" data-toggle="tab">Media</a></li>
	<li><a href="#EditPageFilter" data-toggle="tab">Filter</a></li>
	<li><a href="#EditPageSpecs" data-toggle="tab">Specs</a></li>
	<li><a href="#EditPageArticles" data-toggle="tab">Articles</a></li>
</ul>
<div class="tab-content">
	<div class="tab-pane active" id="EditPageGeneral">
		<?php include 'EditPageGeneral.php'; ?>
	</div>
	<div class="tab-pane" id="EditPageMedia">
		<?php include 'EditPageMedia.php'; ?>
	</div>
	<div class="tab-pane" id="EditPageFilter">
		<?php include 'EditPageFilter.php'; ?>
	</div>
	<div class="tab-pane" id="EditPageSpecs">
		<?php include 'EditPageSpecs.php'; ?>
	</div>
	<div class="tab-pane" id="EditPageArticles">
		<?php include 'EditPageArticles.php'; ?>
	</div>
</div>
</div>
<script type="text/javascript">
	gets_articles_platforms(1);
	edit_page_filter_select();
</script>