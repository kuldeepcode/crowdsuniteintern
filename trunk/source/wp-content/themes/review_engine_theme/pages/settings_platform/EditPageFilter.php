<?php
$toolTip = array(
	   "Select if you are raising money for yourself or on behave of a corporation. ",
		"Select what country you are from",
		"Select the industry you are in or what you will use the money for",
		"If you are taking a loan select 'Debt'. If you are giving ownership of your corporation select 'Equity'.
		If you are giving back anything for the money select 'Reward', if you are not giving anything back then select 'Donation'",
		"Campaign type can be either 'All or Nothing,' the set goal of the campaign must be met to receive the money, or 'Keep what you raised', you keep whatever amount you raise. "
	);
$groups = get_all_data_filter_by_cat_id_tgt( $platform_cat_id );
$optin_filter_manager = get_option( 'optin_filter_manager', array() );
$i = 0;
?>
<div class="for_mesage_alert clearfix"></div>
<?php foreach($groups as $group ):?>
<?php if ( ( $i % 3 ) == 0 ):?>
<div class="row-fluid">
<?php endif;?>
	<div class="span4">
		<div class="scrollbar_cat_filter">
			<h3 style="margin-top: -5px;font-size: 17px;">
				<?php echo $group['group_name']  ?>
				<a href="#" rel="tooltip"  data-placement="right" data-title="<?php echo $toolTip[$i++];?>" >
					<i class=" icon-question-sign"></i>
				</a>
			</h3>
			<ul class="nav nav-list">
				<?php if (  isset( $optin_filter_manager[ $group['filter_id'] ]['all_field_enable'] ) &&  $optin_filter_manager[ $group['filter_id'] ]['all_field_enable'] == 'yes'  ):?>
				<li onmouseover="this.style.backgroundColor='lightgrey';" onmouseout="this.style.backgroundColor='white';">
					<label class="checkbox" style="font-size: 17px;cursor: pointer;"><input type="checkbox" name="all_fild___" value="Yes" /><?php echo ( ( empty ( $optin_filter_manager[ $group['filter_id'] ]['all_field_label'] ) )?'All':$optin_filter_manager[ $group['filter_id'] ]['all_field_label'] );?></label>
				</li>
				<li>
					<hr/>
				</li>
				<?php endif;?>
				<?php foreach( (array)$group['value'] as $value  ):?>
				<li onmouseover="this.style.backgroundColor='lightgrey';" onmouseout="this.style.backgroundColor='white';">
					<label class="checkbox" style="font-size: 17px;cursor: pointer;">
						<input type="checkbox" name="filter_value[]" value="<?php echo $value['filter_value_id']?>"/>
						<?php echo $value['value_name'];?>
					</label>
				</li>
				<?php endforeach;?>
			</ul>
		</div>
	</div>
<?php if ( ( $i % 3 ) == 0 ):?>
</div>
<?php endif;?>
<?php endforeach;?>
<?php if ( ( $i % 3 ) != 0 ):?>
	<div class="span<?php echo ( ( 3 - $i % 3 ) * 4 );?>"></div>
</div>
<?php endif;?>
<div class="row-fluid btn_container">
	<div class="offset2 span10 text-right">
		<a class="btn save-changes" data-action="edit_page_filter">Save Changes</a>
	</div>
</div>