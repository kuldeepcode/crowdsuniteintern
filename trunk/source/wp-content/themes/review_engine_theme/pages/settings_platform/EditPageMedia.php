<?php 
$logo_id = get_post_meta( $platform_id, 'product_logo_attachment_id',true );
$snapshots_id = get_post_meta( $platform_id, 'product_snapshots_attachment_id', true );
$embed_cust = new WP_Embed();
$embed_cust->post_ID = true;
?>
<div class="for_mesage_alert clearfix"></div>
<h3>Logo</h3>
<div class="row-fluid">
	<div class="span4 text-center">
		<?php if ( $ulr = wp_get_attachment_url($logo_id) ): ?>
		<img src="<?php echo $ulr;?>" class="logo_"  data-attach-id="<?php echo $logo_id;?>"/><br/>
		<?php else:?>
		<img src="" class="logo_" data-attach-id="<?php echo $logo_id;?>"/><br/>
		<?php endif;?>
		<a class="btn image-media">Change Logo</a>
	</div>
</div>
<h3>SnapShots</h3>
<ul class="media_main">
<?php foreach($snapshots_id as $snapshot ):?>
	<?php if ( $snapshot['type'] == 'img' && $ulr = wp_get_attachment_url( $snapshot['attach'] ) ):?>
	<li class="media_container" data-attach="<?php echo $snapshot['attach']?>" data-type="img">
		<div class="sortable_box clearfix">
			<div class="pull-left">
				<i class="icon-move" title="Move Block"></i>
			</div>
			<div class="pull-right">
				<i class="icon-pencil image-media" title="Edit"  data-action="change"></i>
				<i class="icon-remove remove-image" title="Remove Block"></i>
			</div>
		</div>
		<div class="media_box">
			<img src="<?php echo $ulr;?>" class="logo_"/>
		</div>
	</li>
	<?php elseif ( $snapshot['type'] == 'video' ):?>
	<li class="media_container" data-attach="<?php echo $snapshot['attach']?>" data-type="video">
		<div class="sortable_box clearfix">
			<div class="pull-left">
				<i class="icon-move" title="Move Block"></i>
			</div>
			<div class="pull-right">
				<i class="icon-pencil video-media" title="Edit"  data-action="change"></i>
				<i class="icon-remove remove-image" title="Remove Block"></i>
			</div>
		</div>
		<div class="media_box">
			<?php echo $embed_cust->run_shortcode('[embed]'.$snapshot['attach'].'[/embed]');?>
		</div>
	</li>
	<?php endif;?>
<?php endforeach;?>
</ul>
<div class="row-fluid">
	<div class="span6 text-left">
		<a class="btn image-media" data-action="add_snapshots">Add Image</a>
		<a class="btn video-media" data-action="add_snapshots"=>Add Video</a>
	</div>
	<div class="span6 text-right"><a class="btn save-changes" data-action="edit_page_image">Save Changes</a></div>
</div>
<script type="text/javascript">
	$( ".media_main" ).sortable({
		handle:'.sortable_box'
	});
</script>