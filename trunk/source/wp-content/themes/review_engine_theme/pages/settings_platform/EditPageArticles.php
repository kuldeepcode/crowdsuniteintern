<div class="for_mesage_alert clearfix"></div>
<div class="container_articles clearfix"></div>
<div id="postArticle" class="modal hide fade">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3>Post Article</h3>
	</div>
	<form id="submit_article" action="" method="post" style="margin-bottom: 0px;">
		<div class="modal-body">
			<div>
				<h4 style="float:left;">Title</h4>
				<div style="float: right;">
					Characters left:
					<span id="artTitle_count" class="counter">70</span>
				</div>
			</div>
			<input name="artTitle" id="artTitle" type="text" class="input-block-level" placeholder="Article Title" maxlength="70" required>
			<h4>Description</h4>
			<textarea name="artDescription" id="artDescription" class="input-block-level" rows="5" placeholder="Article Description" required></textarea>
			<h4>Link to the Article</h4>
			<input name="artLink" id="artLink" type="text" class="input-block-level" placeholder="http://" required>
			<div style="margin-top:15px; float:left;"></div>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn" data-dismiss="modal">Close</button>
			<input type="submit" name="submit" class="btn btn-primary" value="Post" />
		</div>
	</form>
</div>