<?php
global $helper,$wp_query,$posts,$post, $current_custom_page,$wpdb;
wp_enqueue_script('chosen.jquery.min.js');
wp_enqueue_style('chosen.min.css');
get_header();
/*
**James: Get all articles
*/
?>
	 <!-- Body start here -->
<style type="text/css">
	.col_box2 .fix_div{
		float:inherit;
		display:block;
	}
	.col_box2 .fix_div .span4{
		min-height: 1px;
	}
	.col_box2 .fix_div .row-fluid{
		padding-top: 18px;
	}
	#postArticle .chosen-container.chosen-container-multi ul li span{
		line-height:1;
	}
	#postArticle .chosen-container .chosen-results {
		max-height:150px;
	}
</style>
<div id="container">
	<div class="container_res">
		<div class="container_main">
			<div class="col_box" style="display:block">
				<h1 class="blue2">
					<?php echo __('Crowdfunding Articles','re') ?>
					<?php if ( is_user_logged_in() ):?>
						<a class="btn pull-right" href="#postArticle" data-toggle="modal"> Post Article </a>
					<?php else: ?>
						<a class="btn pull-right" href=" <?php echo tgt_get_permalink('register') ?> "> Post Article </a>
					<?php endif;?>
				</h1>
				
			</div>
			<?php
			//$page = $wp_query->query_vars['paged'];
			//wp_reset_postdata();
			//query_posts(array(
			//						'post_type' => 'article',
			//						'posts_per_page' => 1,
			//						'paged' => $page ? $page : 1,
			//						'order' => 'DESC',
			//						'orderby' => 'date'
			//					  ));
			if ( have_posts() )
			{
				while(have_posts())
				{
					the_post();
					$art_id = get_the_ID();
					$art_url = get_post_meta( $art_id, 'AUTO_CREATE_ARTICLE_HTTP_REFERER', true );
					$art_url_host = parse_url( $art_url );
					$user_info = get_userdata($post->post_author);

			?>
			<div class="col_box2 row-fluid">
				<div class="text fix_div">
					<div class="title clearfix">
						<div class="title_left">
							<h1>
								<a href="<?php the_permalink(); ?>"><?php echo $post->post_title;?></a>
							</h1>
						</div>
						<?php if ( !empty( $art_url_host['host'] ) ):?>
						<span class="widget-stat-time" style="display:inline;float:right;">
							<a href="<?php echo $art_url?>" target="_blank">
								<?php echo $art_url_host['host']?>
							</a>
						</span>
						<?php endif;?>
					</div>
					<?php
					$products_id = $wpdb->get_results("SELECT group_concat({$wpdb->prefix}ref_product_article.`product_id`) as product_id FROM {$wpdb->prefix}ref_product_article WHERE  {$wpdb->prefix}ref_product_article.`article_id` = {$art_id} group by {$wpdb->prefix}ref_product_article.`article_id`;");
					$products_id = ( empty( $products_id ) )?'':explode(',', $products_id[0]->product_id );
					if( !empty( $products_id ) ) {
						ob_start();
						foreach ($products_id as $key => $product_id) {
							$product_status = get_post_field('post_status',$product_id );
							if($product_status == 'publish') {
								
						?>
						<a style="color:#1793D3;" href="<?php echo get_permalink($product_id);?>">
							<?php echo get_the_title( $product_id );?>
						</a>,
						<?php
								}
						}
						$products_title = trim( trim(ob_get_contents()), ',' );
						ob_end_clean();
						if ( !empty( $products_title ) ) echo '<div class="article_container">CrowdFunding Website:'.$products_title.'</div>';
					}
					?>
					<div class="content_text fix_div clearfix">
						<p><?php echo tgt_limit_content(get_the_content(),34); ?> </p>
						<div class="row-fluid">
							<div class="span4">
								<a style="color:#1793D3;" href="<?php the_permalink(); ?>"><?php _e('Comments','re'); ?>  (<?php echo $post->comment_count; ?>)</a>
							</div>
							<div class="span4 text-center">Added by: <?php echo $user_info->display_name;?></div>
							<div class="span4 text-right"><?php echo the_time(get_option('date_format')); ?></div>
						</div>
					</div>
				</div>
			</div>
		<?php
			}
		?>
		<!--Pagination-->
		<div class="col_box1">

			<?php

			//echo tgt_generate_pagination('paged', 4);

			 if(function_exists('wp_pagenavi')) { wp_pagenavi(); }				   //�Updated by Brill :: 21-June-2013�

			if ( $wp_query->max_num_pages > 1 ) {
			?>

			<!--<div class="pagination">											 �Updated by Brill :: 21-June-2013�
				<b class="paginate_title">  <?php //_e('Pages') ?> :</b>			 �Updated by Brill :: 21-June-2013�  
				<?php
					//echo paginate_links( get_pagination_args() );				  �Updated by Brill :: 21-June-2013�  
				?>	
			</div>-->																<!--�Updated by Brill :: 21-June-2013� -->

			<?php  } ?>
		</div>
		<?php
		}else{ ?>
			<p class="red">
			  <?php _e("Unfortunally, there is not article written yet.", 're') ?>
			</p>
		<?php
		}
		?>



		</div>
	</div>
</div>
<?php if ( is_user_logged_in() ):?>
<?php
$args = array(
	'posts_per_page'   => -1,
	'post_type'=>'product',
	'post_status'=>'publish',
	'orderby' => 'title',
	'order' => 'ASC'
	);
$list_product = get_posts( $args );
?>
<div id="postArticle" class="modal hide fade">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3>Post Article</h3>
	</div>
	<form id="submit_article" action="" method="post" style="margin-bottom: 0px;">
		<div class="modal-body">
			<h4>Platforms mentioned in the article</h4>
			 <select data-placeholder="Choose a Platform..." class="chosen-select" multiple style="width:100%;">
			<?php foreach ($list_product as $product_):?>
				<option value="<?php echo $product_->ID;?>"><?php echo $product_->post_title;?></option>
			<?php endforeach;?>
			</select>
			<div class="clearfix">
				<h4 style="float:left;">Title</h4>
				<div style="float: right;padding-top: 10px;">
					Characters left:
					<span id="artTitle_count" class="counter">70</span>
				</div>
			</div>
			<input name="artTitle" id="artTitle" type="text" class="input-block-level" placeholder="Article Title" maxlength="70" required>
			<h4>Description</h4>
			<textarea name="artDescription" id="artDescription" class="input-block-level" rows="5" placeholder="Article Description" required></textarea>
			<h4>Link to the Article</h4>
			<input name="artLink" id="artLink" type="text" class="input-block-level" placeholder="http://" required>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn" data-dismiss="modal">Close</button>
			<input type="submit" name="submit" class="btn btn-primary" value="Post" />
		</div>
	</form>
	<script type="text/javascript">
	jQuery(document).ready(function($){
		$('#submit_article').on('submit',function(){
			if ( jQuery('.chosen-select').val() ) {
				$.post(
					'/wp-admin/admin-ajax.php', 
					{
					'action':'add_user_article',
					'title':$('#artTitle').val(),
					'description':$('#artDescription').val(),
					'url':$('#artLink').val(),
					'product_id': $('.chosen-select').val()
					}, 
					function(response){
						if ( response.success ){
							if ( response.para == 1 ) {
								alert('The article was created. After the approval by the administrator the article will be displayed on the site');
								location.reload();
							} else if ( response.para == 2 ) {
								alert('Unfortunately the article with this URL has already been created for the given product');
							} else if ( response.para == 3 ) {
								alert('The article was created.');
								location.reload();
							}
						}
					}
				);
			} else {
				alert('Select at least one platform');
			}
			return false;
		});
		$( "#postArticle .chosen-select" ).chosen({width:"100%"});
		$('#artTitle').on("keyup", function(){
        	$('#artTitle_count').html( 70 - jQuery('#artTitle').val().length );
    	});
	});
	</script>
</div><!-- /.modal -->
<?php endif;?>
<!--body end here-->
<?php
	get_footer();
?>