<?php
// get editor comment
global $helper, $wp_query, $post, $wp_rewrite;
usort( $wp_query->comments, "usort_coments");

$ratings = get_option('tgt_rating');

if ( !is_array( $sum_ar = get_post_meta( $post->ID, 'tgt_review_rating_average',true ) ) ) {
    $sum_ar = update_post_rating_average( $post->ID, true );
}

if ( !empty($sum_ar) ):?>
<div class="review_left overall_average">
    <div class="overall">
        <?php foreach( $sum_ar as $rate_ID => $rate ): ?>
        <div class="orange_rate2">
            <p class="rate"><?php echo $ratings[$rate_ID] ?></p>
            <div class="vote_star" style="background:none; border:none; height:auto; margin:5px 0 5px 10px">
                <div class="star">
                    <?php
                    tgt_display_rating( $rate, '', true, 'star-disabled' );
                    ?>
                </div>
            </div>
        </div>
        <?php endforeach;?>
        <div class="orange_rate">
            <p class="rate"> <?php _e("Overall Rating:", "re") ?> </p>
            <div class="vote_star" style="background:none; border:none; height:auto; margin:5px 0 5px 10px">
                <div class="star">
                    <?php
                    tgt_display_rating( get_post_meta( $post->ID, PRODUCT_RATING, true ), 'product[rating][user]' );
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endif;
echo '<ul class="reviews-list">';

wp_list_comments(array('type'=>'review',                                   //“Updated by Brill :: 20-June-2013”
                        'callback'=>'show_user_reviews',                   //“Updated by Brill :: 20-June-2013”
                        'reverse_top_level'=>false,
                        //'page'=>1,
                        //'reverse_top_level'=> true,
                        //'per_page' => 10
                        ),$wp_query->comments);
echo '</ul>';
$review_count = absint( get_post_meta( $post->ID, 'tgt_rating_count', true ) );
$review_count += $review_count +  absint( get_post_meta( $post->ID, 'tgt_edior_rating_count', true ) );
if ($review_count == 0){?>
    <div class="content_review">
        <div class="revieww" styles="padding-bottom: 20px;">
            <p class="no-review" style="margin: 10px 20px; font-size: 14px;">
                <?php _e('This product has no review yet. Be the first to review, click ','re'); ?>
                <a data-toggle="modal" href="#reviewModal"><?php  _e('here', 're') ?></a>
            </p>
        </div>
    </div>
<?php }

$total_page = $wp_query->max_num_comment_pages ;
if ( $total_page > 1 )
{
    echo '<div class="wp-pagenavi" style="width: 100%; text-align:center;">';
    //echo '<p>' . __('Pages:') . '</p>';
    //paginate_comments_links( array('add_fragment' => '#reviews', 'type' => 'list', 'prev_text' => '«', 'next_text' => '»') );
    if(function_exists('wp_commentnavi')) { wp_commentnavi(); }
    echo '</div>';
}
?>
<script type="text/javascript"><!--
    var ajaxURL = '<?php echo HOME_URL . '/?do=ajax' ?>';
    function vote(review_id, user_id, type, div_id){
        jQuery.ajax({
            type : 'post',
            url: reviewAjax.ajaxurl,
            data: {
                action: 'like_review',
                review_id: review_id,
                user_id: user_id,
                type: type
            },
            beforeSend:function(){
                if (type == 1)
                    jQuery('#' + div_id).find('.like-count').html( '<?php echo $helper->image('loading.gif', '') ?>' );
                if (type == 0)
                    jQuery('#' + div_id).find('.dislike-count').html( '<?php echo $helper->image('loading.gif' ) ?>' );
            },
            success: function(data){
                if (data.success){
                    if (type == 1){
                        jQuery('#' + div_id).find('.like-count').html( data.count );
                    }
                    if (type == 0){
                        jQuery('#' + div_id).find('.dislike-count').html( data.count );
                    }
                    jQuery('#' + div_id).find('.thumb-up').html( '<?php echo $helper->image('thumb_up.png', 'like' ); ?>' );
                    jQuery('#' + div_id).find('.thumb-down').html( '<?php echo $helper->image('thumb_down.png', 'dislike' ); ?>' );
                }
            }
        });
    }
    -->
</script>