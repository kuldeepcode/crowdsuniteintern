<?php
/*
Template Name: test_page
*/
?>

<?php
$platform_id = (int)base64_decode( urldecode( $_GET['pl'] ) );
$platform_cat_id = 22;
$user_id = get_current_user_id();
$platform_post = get_post( $platform_id );
$platform_name = $_GET['platform'];

if ( !is_object( $platform_post ) || $platform_post->post_type != 'product' || $platform_post->post_name != $platform_name|| ( !current_user_can('administrator') && !get_user_meta( $user_id, 'admin_platform_'.$platform_id, true ) ) ) {
	include( get_query_template( '404' ) );
	header('HTTP/1.0 404 Not Found');
	exit;
}
wp_enqueue_media();
?>
<?php 
if ( class_exists('CrowdsuniteRatingAPI') ){
	CrowdsuniteRatingAPI::ZeroClipboard();
}
?>
<?php wp_enqueue_script('jquery-ui-sortable');?>
<?php wp_enqueue_script('settings_platform.js', get_bloginfo('template_url').'/js/settings_platform.js');?>
<?php wp_enqueue_style('settings_platform.js', get_bloginfo('template_url').'/css/settings_platform.css');?>
<?php get_header(); ?>
<div id="container">
	<div class="container_res">
		<div class="container_main">
			<input type="hidden" name="_nonce" value="<?php echo wp_create_nonce( 'edit_page_platform_'.$platform_id.$user_id );?>"/>
			<input type="hidden" name="platform_id" value="<?php echo $platform_id;?>"/>
			<input type="hidden" name="platform_cat_id" value="<?php echo $platform_cat_id;?>"/>
			<input type="hidden" name="user_id" value="<?php echo $user_id;?>;"/>
			<div class="navbar">
				<div class="navbar-inner">
					<a class="brand" href="<?php echo get_permalink($platform_id);?>" target="_blank"><?php echo get_the_title($platform_id);?></a>
					<ul class="nav">
						<li><a href="#EditPage">Edit Page</a></li>
						<li><a href="#Settings">Settings</a></li>
						<li><a href="#Reviews">Reviews</a></li>
						<li><a href="#Promotions">Promotions</a></li>
					</ul>
				</div>
			</div>
			<div class="hide mesage_alert">
				<div class="alert alert-success text-center">
					<button type="button" class="close" data-dismiss="alert">×</button>
				</div>
				<div class="alert alert-error text-center">
					<button type="button" class="close" data-dismiss="alert">×</button>
				</div>
			</div>
			<div class="container_page clearfix">
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>