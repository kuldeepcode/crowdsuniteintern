<?php
global $helper,$wp_query,$posts, $post, $sorting_pages, $current_custom_page;
get_header();
?>
	<!-- Body start here -->
    <div id="container">
			<div class="container_res">
                <div class="container_main">
                    <div class="notice-widget">
                        Thanks to a new partnership between CrowdsUnite and <a href="http://CF4ALL.com" target="_blank">CrowdFunding4All</a> , the CrowdsUnite’s <a href="http://crowdsunite.com/crowdfunding-projects/">Projects</a> page now feature CF4ALL’s cross-platform project search bar and project feed.
                        <script src="//cf4all.com/cf4all/plugins/all.js#plat_id=2a170658-81f4-11e3-b46c-00155d062a12"></script>
                        <div class="crowdxsearch" text-width="220" search-style="search1"></div>

                    </div>
                    <div class="col_right">
					  <?php
					$top_products = query_top_products();
					if ( !empty( $top_products ) ) {

                        $title = array("Featured Donation Platform","Featured Reward Platform","Featured Debt Platform","Featured Equity Platform","Featured Royalty Platform");


                        $i = 0;
                        foreach( $top_products as $post ) {
                            the_post( $post );
                            $post_id = $post->ID;
                            ?>

                            <div class="section-title">
                                <h1 class="blue2"><?php _e($title[$i],'re'); ?></h1>
                            </div>

                            <button class='btn' onclick="window.location='<?php the_permalink($post_id); ?>';return false;" style="float:right;margin-bottom: 20px;">
                            <div>
                                <div class="row" style="margin-left: -25px">
                                    <div class="span2">
                                        <?php
                                        if ( has_post_thumbnail($post_id) ) {

                                            echo '<div class="index-thumb" style="padding-top: 10px; padding-left: 10px;" >';
                                            echo get_the_post_thumbnail( $post_id, array(104, 84), array( 'alt' => $post->post_title ) );
                                            echo '</div>';
                                        } else
                                            echo '<img src="' . TEMPLATE_URL . '/images/no_image.jpg" style="width:104px;height:84px;" alt=""/>';
                                        ?>

                                        <div style="height:8px;"></div>
                                        <div class="vote_star">
                                            <div class="star" style="width:120px;">
                                                <?php
                                                $rating = get_post_meta($post_id, get_showing_rating(), true);
                                                tgt_display_rating($rating, 'recent_rating_' . $post_id);
                                                ?>
                                                <div> <?php echo '('.(int)get_post_meta( $post_id, PRODUCT_RATING_COUNT, true ).')' ?> </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="span7" style="float:left;display: inline;margin-top: 10px;">
                                        <div>
                                            <p style="position: absolute;font-size:25px;">
                                                <?php
                                                if ( strlen( $post->post_title ) > 32 ) {
                                                    echo substr( $post->post_title, 0, 31 ) . '...';
                                                } else
                                                    echo $post->post_title;
                                                ?>
                                            </p>
                                        </div>
                                        <div style="height:122px";>
                                            <hr/>
                                            <p style="text-align:left; font-size: 13px; height:70px; overflow:hidden;"><?php echo tgt_limit_content( get_the_content(), 60 ); ?></p>
                                            <p style="text-align: left;font-size: 13px; margin-bottom: 0px;"><b>Monthly Visitors:
                                                    <?php
                                                    $ratingwpar = '('.get_post_meta( $post->ID, 'tgt_monthly_visitors', true ).')';
                                                    $ratingwopar = substr($ratingwpar, 1, -1);

                                                    $ratingwopar = number_format($ratingwopar);

                                                    if ($ratingwopar == 0){
                                                        echo "Not Available";
                                                    }

                                                    else {
                                                    echo $ratingwopar;
                                                    }


                                                    ?>
                                                </b> </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </button>

						<?php $i++;
						} ?>

					<?php } ?>

				</div>
					<!-- SIDEBAR GOES HERE -->
					<?php
						get_sidebar();
					?>
					<!-- SIDEBAR END HERE -->
				<div class="clear"></div>
			</div>
		</div>
	</div>

	<!--body end here-->
<script type="text/javascript">
jQuery(document).ready(function(){
	 jQuery('.star-disabled').rating();
	 jQuery('.rating input[type=radio]').rating();
	 jQuery('#sorting_form').each(function(){
		  var current = jQuery(this),
				select = current.find('select[name=sort_type]');
		  select.change(function(){
				current.submit();
		  });
	 });

    // bootstrap enabling tooltip
    jQuery("[rel=tooltip]").tooltip();
});


</script>
<?php
	get_footer();
?>