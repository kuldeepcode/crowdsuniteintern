<?php

if(!class_exists('WP_List_Table')){
	require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

if (!class_exists('admin_page_stats')){
	class admin_page_stats extends WP_List_Table {
	
		private  $columns = array();
		private  $sortable_columns = array();
		private  $perpage;
		private  $query;
		private  $is_search;
		private  $def_order_by;
		private  $order;
			/**
			* Constructor, we override the parent to pass our own arguments
			* We usually focus on three parameters: singular and plural labels, as well as whether the class supports AJAX.
			*/
			function __construct($columns, $sortable, $perpage, $query, $is_search=false, $def_order_by = 'ASC', $order = '') {
				

					$this->columns = $columns;
					$this->sortable_columns = $sortable;
					$this->perpage = $perpage;
					$this->query = $query;
					$this->is_search=$is_search;
					$this->def_order_by = $def_order_by;
					$this->order = $order;
			}
	
			/**
			* Add extra markup in the toolbars before or after the list
			* @param string $which, helps you decide if you add the markup after (bottom) or before (top) the list
			*/
			function extra_tablenav( $which ) {
					if ( $which == "top" ){
							//The code that goes before the table is here
						if($this->is_search)
					{
						?>
						<form action="" method='GET'>
							<p class="search-box">
								<label class="screen-reader-text" for="post-search-input">Search:</label>
								<input type="search" id="post-search-input" name="s" value="<?php echo $_GET['s']?>" />
								<input type="submit" name="" id="search-submit" class="button" value="Search" />
								<input type="hidden" name='page' value='ai-credits-system/ai-credits-system.php' />
							</p>
						</form>
						<div style='clear:both;'></div>
						<?php 
					}

					}
					if ( $which == "bottom" ){
							//The code that goes after the table is there
					}
			}
	
	
			/**
			* Prepare the table with different parameters, pagination, columns and table elements
			*/
			function prepare_items() {
					global $wpdb, $_wp_column_headers;
					$screen = get_current_screen();
				if(!empty($_GET['s']))
				{
					$search = $_GET['s'];
					$array_search =explode(" ",  $_GET['s']); 
					$search_string='';
					foreach($array_search as $key)
					{
						$search_string.="transactions.comment LIKE '%$key%' OR ";
					}
					$search_string = substr_replace($search_string, "", -4);
						/* -- Preparing your query -- */
					$query = $this->query." and ($search_string)";
				}
				else{
						/* -- Preparing your query -- */
				$query = $this->query;
				}
				
					/* -- Ordering parameters -- */
				//Parameters that are going to be used to order the result
				$orderby = !empty($_GET["orderby"]) ? mysql_real_escape_string($_GET["orderby"]) : $this->def_order_by;
				$order = !empty($_GET["order"]) ? mysql_real_escape_string($_GET["order"]) : $this->order;
				if(!empty($orderby) & !empty($order)){ $query.=' ORDER BY '.$orderby.' '.$order; }
	
					/* -- Pagination parameters -- */
			//Number of elements in your table?
			$totalitems = $wpdb->query($query); //return the total number of affected rows
			//How many to display per page?
			$perpage = $this->perpage;
			//Which page is this?
			$paged = !empty($_GET["paged"]) ? mysql_real_escape_string($_GET["paged"]) : '';
			//Page Number
			if(empty($paged) || !is_numeric($paged) || $paged<=0 ){ $paged=1; }
			//How many pages do we have in total?
			$totalpages = ceil($totalitems/$perpage);
			//adjust the query to take pagination into account
					if(!empty($paged) && !empty($perpage)){
							$offset=($paged-1)*$perpage;
					$query.=' LIMIT '.(int)$offset.','.(int)$perpage;
					}
	
					/* -- Register the pagination -- */
					$this->set_pagination_args( array(
							"total_items" => $totalitems,
							"total_pages" => $totalpages,
							"per_page" => $perpage,
					) );
					//The pagination links are automatically built according to those parameters

					/* — Register the Columns — */
					$columns = $this->columns;
					$hidden = array();
					$sortable = $this->sortable_columns;
					$this->_column_headers = array($columns, $hidden, $sortable);
	
					/* -- Fetch the items -- */
					$this->items = $wpdb->get_results($query);
			}

			/**
			* Display the rows of records in the table
			* @return string, echo the markup of the rows
			*/
			function display_rows() {
				//Get the records registered in the prepare_items method
				$records = $this->items;
				//print_r($records);die;
				//Get the columns registered in the get_columns and get_sortable_columns methods
				list( $columns, $hidden ) = $this->get_column_info();
				//Loop for each record
				if(!empty($records)){
					foreach($records as $rec){
						$id_details = '';
						$details_name = '';
						echo '<tr id="record_'.$rec->id.'">';
							foreach ( $columns as $key=>$value ) {
								if ( $id_details == '' && $details_name == ''){
									$id_details = $rec->$key;
									$details_name = $key;
								}
								switch ($key) {
									case 'user_id':
										$user_login_ = ( $rec->$key == 0 )?'No name':get_userdata( $rec->$key )->user_login;
										echo '<td class="'.$key.'">'.$user_login_.'</td>'; 
										break;
									case 'product_id':
										$post_product = get_post( $rec->$key );
										echo '<td class="'.$key.'"><a href="'.get_bloginfo('url').'/settings_page/?platform='.$post_product->post_name.'&pl='.urlencode( base64_encode( $rec->$key ) ).'" target="_blank">'.get_the_title( $rec->$key ).'</a></td>'; 
										break;

									case 'action':
										echo '<td class="'.$key.'">'.ucwords( str_replace( '_', ' ', $rec->$key ) ).'</td>'; 
										break;

									case 'seconds':
										echo '<td class="'.$key.'">'.date( get_option('date_format').' \a\t '.get_option('time_format') ,$rec->$key).'</td>'; 
										break;
									case 'detal':
										echo '<td class="'.$key.'"><a class="details_stat" data-details-id="'.$id_details.'" data-details-name="'.$details_name.'">details</a></td>';
										break;
								}
							}
						echo'</tr>';
					}
				}
			}
	}
}

?>