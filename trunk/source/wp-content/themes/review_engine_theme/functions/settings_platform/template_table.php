<style type="text/css">
	.wp-pagenavi a:hover {
		text-shadow: 0px 1px rgb(56, 141, 190) !important;
		border-color: rgb(51, 144, 202) !important;
		background: -moz-linear-gradient(center top , rgb(180, 246, 255) 1px, rgb(99, 208, 254) 1px, rgb(88, 176, 231)) repeat scroll 0% 0% transparent !important;
		color: rgb(255, 255, 255) !important;
		box-shadow: 0px 1px rgb(231, 231, 231) !important;
	}
	.wp-pagenavi span.current {
		padding: 6px 9px !important;
		border: 1px solid rgb(51, 144, 202) !important;
		color: rgb(255, 255, 255) !important;
		box-shadow: 0px 1px rgb(231, 231, 231) !important;
		margin-right: 3px !important;
		text-shadow: 0px 1px rgb(56, 141, 190) !important;
		background: -moz-linear-gradient(center top , rgb(180, 246, 255) 1px, rgb(99, 208, 254) 1px, rgb(88, 176, 231)) repeat scroll 0% 0% transparent !important;
	}
	.wp-pagenavi a, .wp-pagenavi span.pages, .wp-pagenavi span.extend {
		color: rgb(51, 51, 51) !important;
		text-shadow: 0px 1px rgb(246, 246, 246) !important;
		padding: 6px 9px !important;
		border: 1px solid rgb(182, 182, 182) !important;
		box-shadow: 0px 1px rgb(239, 239, 239) !important;
		background: -moz-linear-gradient(center top , rgb(255, 255, 255) 1px, rgb(243, 243, 243) 1px, rgb(230, 230, 230)) repeat scroll 0% 0% transparent !important;
		font-size: 12px !important;
		margin-right: 3px !important;
		text-decoration: none !important;
	}
	th.column-rating_type,.rating_type,
	th.column-rating_value,.rating_value,
	th.column-metod,.metod,
	th.column-count,.count,
	th.column-reviews_amount,.reviews_amount{
		text-align: center;
	}
	.tstat tbody tr:nth-child(odd){
		background-color: #F9F9F9;
	}
	th.sorted a {
		display: inline-block;
	}
	.popup_bg{
		display: none;
		opacity: 0.8;
		position: fixed;
		top: 0;
		right: 0;
		bottom: 0;
		left: 0;
		z-index: 1040;
		background-color: #000;
	}
	.popup{
		display: none;
		min-width: 650px;
		position: fixed;
		top: 10%;
		right: 10%;
		bottom: 10%;
		left: 10%;
		z-index: 1050;
		background-color: #fff;
		border: 1px solid #999;
		-webkit-border-radius: 6px;
		-moz-border-radius: 6px;
		border-radius: 6px;
		-webkit-box-shadow: 0 3px 7px rgba(0,0,0,0.3);
		-moz-box-shadow: 0 3px 7px rgba(0,0,0,0.3);
		box-shadow: 0 3px 7px rgba(0,0,0,0.3);
		-webkit-background-clip: padding-box;
		-moz-background-clip: padding-box;
		background-clip: padding-box;
		outline: none;
	}
	.popup_close{
		position: absolute;
		right: 4px;
		top: 4px;
		background: url('<?php bloginfo('template_url')?>/images/remove.gif');
		display: block;
		width: 12px;
		height: 12px;
	}
	.popup .stat_table{
		padding: 20px 3% 3% 3%;
		width: 94%;
		height: 90%;
		overflow: hidden;
		overflow-y: auto;
	}
	.text-center{
		text-align: center;
	}
</style>
<div class="wrap">
	<h2><?php echo get_admin_page_title();?></h2>
	<div class="uc_body tstat">
		<?php
		@$wp_list_table->display();
		?>
	</div>
</div>	
<div class="popup">
	<div class="stat_table tstat"></div>
	<a class="popup_close"></a>
</div>
<div class="popup_bg"></div>
<script type="text/javascript">
var details_id;
var details_name;
function gets_details_stats(pagination){
	jQuery.ajax({
		url: ajaxurl,
		type: 'POST',
		data: {
			action: 'details_stats_page',
			id: details_id,
			details_name: details_name,
			pagination: pagination
		}
	})
	.done(function( msg ) {
		jQuery('.popup .stat_table').html(msg);
	});
}
jQuery(document).ready(function($) {
	$('.popup_bg, .popup_close').on('click', function(){
		$('.popup, .popup_bg').css('display','none');
	});
	$('.details_stat').on('click', function(){
		$('.popup .stat_table').html('');
		details_id = $(this).data('details-id')
		details_name = $(this).data('details-name')
		gets_details_stats(1);
		$('.popup, .popup_bg').css('display','block');
	});

	$('.stat_table').delegate('.wp-pagenavi a','click',function(){
		gets_details_stats($(this).data('pagination'));
		return false;
	});
});
</script>