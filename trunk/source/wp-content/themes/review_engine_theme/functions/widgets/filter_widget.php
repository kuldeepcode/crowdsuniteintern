<?php
/*
Hooks provide you with a way to insert your code directly into the Wordpress code. A hook is a PHP function call that occurs when a specific event occurs.

Action hooks execute a function when a specific event occurs. This is how you create an action hook: add_action( $actionToHookTo, $functionToCall, $priority, $howManyArguments );

A list of all the Wordpress actions can be seen here http://codex.wordpress.org/Plugin_API/Action_Reference

Here are the most common action hooks and when they occur:

* admin_head: Occur in the head for the dashboard

* admin_init: Occurs when the dashboard has loaded

* comment_post: Occurs when a new comment is created

* create_category: Occurs when a category is created

* init: Occurs when Wordpress has loaded the website

* publish_post: Occurs when a new post is published

* switch_theme: Occurs when the theme is changed

* user_register: Occurs when a new user registers

* wp_footer: Occurs in the footer 

* wp_head: Occurs in the header

Filter hooks change content in Wordpress before it is either displayed on the screen or when it is saved in the database. You create filters by passing identical arguments to add_filter(). Here is an example: add_filter( $actionToHookTo, $functionToCall, $priority, $howManyArguments );

Here are the most common filter hooks:

* comment_text: Changes comments before they are displayed

* get_categories: Changes category lists

* the_content: Changes the content of a post or page

* the_content_rss: Changes content of posts in RSS feeds

* the_permalink: Changes the permalink

* the_title: Changes the title of posts and pages

* wp_title: Changes the text in the title tag

You can retrieve the location of your plugin like this 
plugin_dir_path( __FILE__ ); 
__FILE__ is a reference to the file that is running

Here is the location of your images folder
plugins_url( 'images/YOUR_IMAGE.png' , __FILE__ );
*/
?>
<?php

// To create a widget you have to extend the WP_Widget classß
class filter_widget extends WP_Widget
{


	/**

	* This constructor function initializes the widget

	* This constructor function initializes the widget. It sets the class 
	* name for the tag that surrounds the widget. It sets the description 
	* that is found in the widget admin section of Wordpress. Calls the parent 
	* class constructor for further initialization

	* classname: class name added to the widgets li tag
	* description: describes the widget on the widget admin page
	* __() allows for translation of the text

	*/

	// Constructor
	function filter_widget()
	{
        $widget_ops = array('classname' => 'filters', 'description' => __( 'Display Filters', 're' ) );
        $this->WP_Widget('', __( 'Filters', 're' ), $widget_ops);
	
	}

	
	/** Outputs the contents of the widget
	 * arguments sent from the theme / instance of the class is sent
	 
	 * Default Values for special tags found below
	 * 'before_widget' => '<li id="%1$s" class="widget %2$s">',
 	 * 'after_widget' => "</li>n",
 	 * 'before_title' => '<h2 class="widgettitle">',
 	 * 'after_title' => "</h2>n"
 	 
 	 * $title : Contains the title that shows up in the sidebar
 	 
 	 **/
	function widget($args, $instance)
	{
         global $cat;

        $default_sort 			= get_option(SETTING_SORTING_DEFAULT) ? get_option(SETTING_SORTING_DEFAULT) : 'recent-products';
        $sort_type 				= !empty( $_GET['sort_type'] ) ? $_GET['sort_type'] : $default_sort;
        $filter 					= empty( $_GET['filter'] ) ? '' : $_GET['filter']; // filter value
        $filters 				= array();
        $optin_filter_manager = get_option( 'optin_filter_manager', array() );

        if ( !empty($_GET['filter']) ){
            $filters = explode(',', $_GET['filter'] );
            //var_dump($filters);
        }

       /* if ( !empty($_POST ) && !empty($_POST['filter_value']) ){
            $fvalues = array();
            foreach( $_POST['filter_value'] as $group ){
                foreach( $group as $value ){
                    $filters[] = $value;
                }
            }
        }*/

        // TODO: Hardcoded it for now, but it should be received from wp_tgt_spec table for category Platforms. ID is 22.
        $cat = 22;

        $param = array(
            'sort_type' => $sort_type,
            'filter' 	=> $filter
        );

        // before_widget is not adding the div element so I'm doing it manually
        //echo $before_widget;
        ?>
        <div class="sidebar-widget">
        <?php
        //echo $before_title;
        //echo 'Filter';
        //echo $after_title;
    		 $groups = get_all_data_filter_by_cat_id_tgt($cat);
				 if (is_array($groups) && !empty ($groups) ){
				?>

			<!--div class="col" -->
				<!-- div class="box_border" style="margin-bottom:20px; margin-top:0;" -->
					<!-- div class="col_box4" id="filter_product" -->
                     <!--div id="filter_page_1_filter" style="border-right:1px #B6C5C9 dotted;"-->
					<form id="filter_form_" action="<?php bloginfo('url');?>/category/platforms/" method="get" name="filter_form_">
						<input type="hidden" name="sort_type">
						<input type="hidden" name="filter">
					</form>
					  <form id="filter_form_filter" action="../../category/platforms" method="post" style="border-right:1px #B6C5C9 dotted;border-bottom: 1px solid #BFBFBF;padding-bottom: 10px;" >
					 	<!--div style="margin-bottom: 50px" class="title_left_filter">
						 	<h1 style="float:left;"><?php _e('Find a product:','re') ?>
						    </h1>
						</div-->

						<!--div id="filter_wrapper_filter" style="overflow: visible; position: relative;" -->

							
							<?php
                                // TODO: setting the tooltip here as a workaround, the best way is to be able to set it in the gui which will write it into DB
                                $toolTip = array(
                                   "Select if you are raising money for yourself or on behave of a corporation. ",
                                    "Select what country you are from",
                                    "Select the industry you are in or what you will use the money for",
                                    "If you are taking a loan select 'Debt'. If you are giving ownership of your corporation select 'Equity'.
                                    If you are giving back anything for the money select 'Reward', if you are not giving anything back then select 'Donation'",
                                    "Campaign type can be either 'All or Nothing,' the set goal of the campaign must be met to receive the money, or 'Keep what you raised', you keep whatever amount you raise. "
                                );
                                $i = 0;
								foreach($groups as $group ){
								 ?>
								<div class="find_filter find-list_filter" >
									<h3 style="margin-top: -5px;font-size: 17px;">
                                        <?php echo $group['group_name']  ?>
                                        <a href="#" rel="tooltip"  data-placement="right" data-title="<?php echo $toolTip[$i++];?>" >
                                            <i class=" icon-question-sign"></i>
                                        </a>
                                    </h3>
									<div class="scrollbar_cat_filter">
									  	<div class="viewport_filter">
											<div class="overview_filter">
											<ul class="nav nav-list">
											<?php if ( isset( $optin_filter_manager[ $group['filter_id'] ]['view_filtre'] ) &&  $optin_filter_manager[ $group['filter_id'] ]['view_filtre'] == 'selected' ):?>
												<li>
													<select name="filter_value[<?php echo $group['filter_id']?>]">
														<?php if (  isset( $optin_filter_manager[ $group['filter_id'] ]['all_field_enable'] ) &&  $optin_filter_manager[ $group['filter_id'] ]['all_field_enable'] == 'yes'  ):?>
															<option value="all"><?php echo ( empty ( $optin_filter_manager[ $group['filter_id'] ]['all_field_label'] ) )?'All':$optin_filter_manager[ $group['filter_id'] ]['all_field_label'];?></option>
														<?php endif;?>
											<?php endif;

												foreach( (array)$group['value'] as $value  ):
													// generate catgory filter link
													$param['filter'] = $value['filter_value_id'];
													//$url = http_build_query( $param );
													if ( isset( $optin_filter_manager[ $group['filter_id'] ]['view_filtre'] ) &&  $optin_filter_manager[ $group['filter_id'] ]['view_filtre'] == 'selected' ):?>
														<option value="<?php echo $value['filter_value_id']?>" <?php selected( in_array( $value['filter_value_id'], (array) $filters ) ); ?>><?php echo $value['value_name']?></option>	
													<?php else:?>
												<li onmouseover="this.style.backgroundColor='lightgrey';" onmouseout="this.style.backgroundColor='white';" >
                                                    <?php
                                                        $checkbox_name = str_replace(' ','',$value['value_name']);
                                                    ?>
                                                    <label class="checkbox" style="font-size: 17px;cursor: pointer;">
                                                        <input type="checkbox" name="filter_value[<?php echo $group['filter_id']?> ][]" value="<?php echo $value['filter_value_id']?>"
                                                            <?php
                                                            if ( in_array( $value['filter_value_id'], (array) $filters ) ){
                                                                echo 'checked="checked"';
                                                            }
                                                            ?>
                                                            >
                                                             <?php echo $value['value_name']?>
                                                    </label>
												</li>
												<?php
													endif;
												endforeach; // end foreach foreach( (array)$group['values'] as $value  ) 
											if ( isset( $optin_filter_manager[ $group['filter_id'] ]['view_filtre'] ) &&  $optin_filter_manager[ $group['filter_id'] ]['view_filtre'] == 'selected' ):?>
													</select>
												</li>
											<?php endif; ?>
										</ul>
										</div>
										</div>
									</div>		
									</div>
								<?php
								} ?>
								<!--input type="submit" style="margin-top: 40px;font-size: 20px;" value="<?php _e('Search', 're') ?>" -->

						<!-- /div --> <!--end #filter_wrapper-->
						</form>
						<script type="text/javascript">
						jQuery(document).ready(function($){
							function submit_filter_form_(){
								var filter = '';
								jQuery("input:checkbox:checked").each(function(e){
									if (  filter == '' ) filter = $(this).val(); else filter +=','+$(this).val();
								});
								jQuery("#filter_form_filter select").each(function(){
									if ( $(this).val() !== 'all' )if ( filter == '' ) filter = $(this).val(); else  filter += ','+$(this).val();
								});
								
								if ( jQuery('#cat_sort_type').length > 0 )
									jQuery('#filter_form_ input[type=hidden][name=sort_type]').val( jQuery('#cat_sort_type').val() );
								else
									jQuery('#filter_form_ input[type=hidden][name=sort_type]').val( '<? echo get_option(SETTING_SORTING_DEFAULT);?>' );

								if ( filter !== '' ) jQuery('#filter_form_ input[type=hidden][name=filter]').val(filter)
								jQuery('#filter_form_').submit();
							}
							$( "#filter_form_filter select" ).change(function(){ submit_filter_form_() })
							$('#filter_form_filter input[type=checkbox]').on('click',function(){ submit_filter_form_() });

						});
						</script>
                          <!--/div-->
					<!-- /div --> <!--end .col_box4-->
				<!-- /div --> <!--end .box_border-->
			<!--/div--> <!--end .col-->

      <div class="clear"></div>
      <?php }

      //echo $after_widget;
      ?>
        </div>
     <?php

     }

	// Pass the new widget values contained in $new_instance and update saves
	// everything for you
	function update($new_instance, $old_instance)
	{
   
	}
	
	// Displays options in the widget admin section of site
	function form($instance)
	{
   
	}
	
	
}


function filter_widget_init()
{
	// Registers a new widget to be used in your Wordpress theme
	//register_widget('filter_widget');
}

// Attaches a rule that tells wordpress to call my function when widgets are 
// initialized
//add_action('widgets_init', 'filter_widget_init');

?>
