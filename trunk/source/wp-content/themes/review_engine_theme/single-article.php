<?php 
global $helper,$wp_query, $artile_query, $wpdb;

require ( PATH_PROCESSING . DS. 'single_article_processing.php' );
get_header();
the_post();
$article_query[ 'id' ] = (int) $post->ID;
$article_query['post_name'] = $post->post_name;

$user_info = get_userdata($post->post_author);
$art_url = get_post_meta( $post->ID, 'AUTO_CREATE_ARTICLE_HTTP_REFERER', true );
$art_url_host = parse_url( $art_url );

$order = get_option( 'comment_order' );
?>
<div id="container">
    	<div class="container_res">
        	<div class="container_main">
            	<div class="col">
                    <div class="col_box" >
                        <h1 class="blue">
                            <a style="font-size:26px;"href="<?php echo ( $art_url )?$art_url:'#';?>" target="_blank">
                                <?php the_title(); ?>
                            </a>
                        </h1>
                    </div>
                    <div class="reply" style="border-bottom:1px #dce4e4 solid;">
                        <!--<img style="float:left; margin:0 10px;" src="images/icon_reply.png" alt=""/>-->
                        <!--?php echo $helper->image('icon_reply.png','', array('style' => 'float:left; margin:0 10px;')) ?-->
                        <a id="go_reply" style="float:right;"> <?php comments_number( __( 'no comments', 're' ), __( 'one comment', 're') , __( '% comments', 're' ) );?></a>
                    </div>
                    <div class="clear"></div>
                    <?php
					/**
					 * get the product info
					 */
                    $products_id = $wpdb->get_results("SELECT group_concat({$wpdb->prefix}ref_product_article.`product_id`) as product_id FROM {$wpdb->prefix}ref_product_article WHERE  {$wpdb->prefix}ref_product_article.`article_id` = {$post->ID} group by {$wpdb->prefix}ref_product_article.`article_id`;");
                    $products_id = ( empty( $products_id ) )?'':explode(',', $products_id[0]->product_id );
                    ?>
                    <ul style="float: right; list-style: none;">
                    <?php

                        foreach ($products_id as $product_ID) {

						if ( ! empty( $product_ID ) && is_object( $product = get_post($product_ID) ) ){
							$rating = get_post_meta($product_ID, 'tgt_rating', true);
						?>
                        <li>
                            <div class="box_border">
                                <div class="text2">
                                    <div class="title2">
                                        <div class="title_left">
                                            <h1> <?php echo $helper->link($product->post_title, get_permalink($product_ID) ) ?> </h1>
                                        </div>

                                        <div class="title_right" style="float:right;">
                                            <div class="vote_star">
                                                <div class="star">
                                                    <?php tgt_display_rating($rating, 'rating-item-'. $product_ID) ?>
                                                    <script type="text/javascript">
                                                        jQuery('.star-disabled').rating();
                                                    </script>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
						<?php }} ?>
                    </ul>
                    <div class="col_box post_content clearfix" style="border-bottom:4px #dce4e4 solid;">
                        <div style="padding:5px 0 5px 10px;"><em><?php echo the_time(get_option('date_format')); ?></em> by: <em><?php echo $user_info->display_name;?></em></div>
                        <?php echo the_content(); ?>
                        <?php
							$pre_post = get_previous_post();
							$next_post = get_next_post();
							if ( is_object( $pre_post ) || is_object( $next_post ) ) {
							?>                         
								<div class="col_box" style="border-bottom:1px #dce4e4 solid;">
									<?php
									if ( $pre_post ) {?>
										<div class="arrow">
											<?php echo $helper->image('arrow_left.png', '<<', array('style' => 'float:left; margin-right:20px;')) ?>                            	
											<p style="text-align:left;"><?php _e( 'Previous Article', 're');?><br/><?php previous_post_link( ); ?></p>
										</div>
									<?php }									
									if ( $next_post ) {?> 
										<div class="arrow" style="float:right;">
										<?php echo $helper->image('arrow_right.png', '<<', array('style' => 'float:right; margin-left:20px')) ?>
											 <p style="text-align:right;"><?php _e( 'Next Article', 're');?><br/><?php next_post_link( ); ?></p>
										</div>
									<?php } ?>
								</div>
							<?php } ?>
                 </div>
                 <?php comments_template(); ?> 
            </div>
        </div>
    </div>
<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery('#go_reply').click(function(){			
			jQuery('html, body').animate({'scrollTop' : jQuery('#commentform').offset().top }, 'slow');
		});
	});
</script>
<?php 
get_footer();
