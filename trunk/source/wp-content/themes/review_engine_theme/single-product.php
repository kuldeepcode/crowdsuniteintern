<?php
global $helper, $wp_query, $post, $allowedtags, $allowedposttags, $current_user, $more, $wp_embed;
include PATH_PROCESSING . DS . 'single_product_processing.php';
count_view_product($post->ID);
get_header();

$product_website = '';
$product_images = '';
$product_price = '';

if (get_post_meta($post->ID, 'tgt_product_url', true) != '')
    $product_website = get_post_meta($post->ID, 'tgt_product_url', true);
// Get product website
/*if(get_post_meta($post->ID,'tgt_product_images',true) != '')
    $product_images = get_post_meta($post->ID,'tgt_product_images',true);// Get product images*/
if (get_post_meta($post->ID, 'tgt_product_price', true) != '')
    $product_price = get_post_meta($post->ID, 'tgt_product_price', true);

// Get product price
$post_date = explode(' ', $post->post_date);
$view_post = get_post_meta($post->ID, PRODUCT_VIEW_COUNT, true);
if (empty($view_post))
    $view_post = 0;
$tab_content = get_post_meta($post->ID, 'tgt_tab', true);
$open_comment = comments_open();

if (get_option(PRODUCT_SHOW_RATING, 1) == 1){
    $count = (int)get_post_meta($post->ID, PRODUCT_EDITOR_RATING_COUNT, true);
} else {
    $count = (int)get_post_meta($post->ID, PRODUCT_RATING_COUNT, true);
}

$logo_id = get_post_meta( $post->ID, 'product_logo_attachment_id',true );
$snapshots_id = get_post_meta( $post->ID, 'product_snapshots_attachment_id', true );
wp_enqueue_script( 'jquery.anythingslider.min.js', get_bloginfo('template_url')."/js/jquery.anythingslider.min.js" );
wp_enqueue_style( 'theme-metallic.css', get_bloginfo('template_url')."/css/theme-metallic.css" );
wp_enqueue_script( 'jquery.fancybox-1.3.4.pack.js', get_bloginfo('template_url')."/lib/fancybox/jquery.fancybox-1.3.4.pack.js" );
wp_enqueue_style( 'jquery.fancybox-1.3.4.css', get_bloginfo('template_url')."/lib/fancybox/jquery.fancybox-1.3.4.css" );


$platform_id = $post->ID;
$args = array(
    'meta_key'     => 'admin_platform_'.$platform_id,
    'meta_value'   => true,
);

$all_users = (array)get_users();
$admin_users = (array)get_users($args);
$admin_user_id = array();
foreach ( $admin_users as $admin_user ) {
    $admin_user_id[] = $admin_user->ID;
}
wp_enqueue_script('chosen.jquery.min.js');
wp_enqueue_style('chosen.min.css');
?>

<div id="container">
    <div class="container_res">
        <div class="container_main">
            <!-- Notification Sidebar -->
            <?php
            if (is_active_sidebar('homepage-widget-area')) {
                dynamic_sidebar('homepage-widget-area');
            }
            ?>
            <!-- End Notification Sidebar -->
            <div class="content_product">
                <div class="col_box clearfix" style="width:998px;">
                    <div class="text3" style="margin-left:0px;">
                        <div class="title3" itemscope="itemscope" itemtype="http://schema.org/Product" style="width:998px;">
                            <meta itemprop="name" content="<?php echo $post->post_title; ?>" />
                            <div class="title_left">

                                    <span class="item" style="display:inline-block;">
                                        <span class="fn">
                                            <h2>
                                                <a itemprop="url" href="<?php echo get_post_meta($post->ID, 'tgt_product_url', true); ?>" target="_blank"><?php
                                                    if(  empty( $logo_id ) )
                                                        echo $post->post_title;
                                                    else{
                                                        $temp = wp_get_attachment_image_src( $logo_id, "medium" );
                                                        echo '<img src="'.$temp[0].'" alt="'.$post->post_title.'" />';
                                                        }
                                                    ?>
                                                </a>
                                                <?php if (current_user_can('edit_others_posts')):?>
                                                <a href="<?php echo get_edit_post_link( $post->ID )?>"><img class="edit_link" src="<?php echo TEMPLATE_URL . '/images/pencil.png'; ?>" alt="edit" title="<?php _e('Edit', 're') ?>" /></a>
                                                <?php endif?>
                                            </h2>
                                        </span>
                                    </span>
                                   <div style="display:inline;margin-left: 15px;">
                                       <i class="icon-share"></i>
                                       <a itemprop="url" href="<?php echo get_post_meta($post->ID, 'tgt_product_url', true); ?>" target="_blank">Visit Website</a>
                                   </div>
                            </div>

                            <div class="title_right" style="float:right;">
                                <?php if ( is_user_logged_in() ):?>
                                    <a class="btn" href="#postArticle" data-toggle="modal" style="float:left;margin:15px;"> Post Article </a>
                                <?php else: ?>
                                    <a class="btn" href=" <?php echo tgt_get_permalink('register') ?> " style="float:left;margin:15px;"> Post Article </a>
                                <?php endif;?>

                                <div id="postArticle" class="modal hide fade">


                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                                                <h3>Post Article</h3>
                                            </div>
                                            <form id="submit_article" action="" method="post" style="margin-bottom: 0px;">
                                                <div class="modal-body">
                                                    
                                                    <div>
                                                    <h4 style="float:left;">Title</h4>
                                                        <div style="float: right;">
                                                            Characters left:
                                                            <span id="artTitle_count" class="counter">70</span>
                                                        </div>
                                                    </div>
                                                    <input name="artTitle" id="artTitle" type="text" class="input-block-level" placeholder="Article Title" maxlength="70" required>
                                                    <h4>Description</h4>
                                                    <textarea name="artDescription" id="artDescription" class="input-block-level" rows="5" placeholder="Article Description" required></textarea>
                                                    <h4>Link to the Article</h4>
                                                    <input name="artLink" id="artLink" type="text" class="input-block-level" placeholder="http://" required>

                                                    <div style="margin-top:15px; float:left;">

                                                        

                                                    </div>
                                                    
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn" data-dismiss="modal">Close</button>
                                                    <input type="submit" name="submit" class="btn btn-primary" value="Post" />
                                                </div>
                                            </form>
                                            <script type="text/javascript">
                                            jQuery(document).ready(function($){
                                                $('#submit_article').on('submit',function(){
                                                    $.post(
                                                        '/wp-admin/admin-ajax.php', 
                                                        {
                                                        'action':'add_user_article',
                                                        'title':$('#artTitle').val(),
                                                        'description':$('#artDescription').val(),
                                                        'url':$('#artLink').val(),
                                                        'product_id':<?php the_id();?>
                                                        }, 
                                                        function(response){
                                                            if ( response.success ){
                                                                if ( response.para == 1 ) {
                                                                    alert('The article was created. After the approval by the administrator the article will be displayed on the site');
                                                                    location.reload();
                                                                } else if ( response.para == 2 ) {
                                                                    alert('Unfortunately the article with this URL has already been created for the given product');
                                                                } else if ( response.para == 3 ) {
                                                                    alert('The article was created.');
                                                                    location.reload();
                                                                }
                                                            }
                                                        }
                                                    );
                                                    return false;
                                                });
                                            });
                                            </script>




                                </div><!-- /.modal -->


                                <?php if ( is_user_logged_in() || get_option(SETTING_SUBMIT_WITHOUT_LOGIN) ):?>
                                <a class="btn" href="#reviewModal" data-toggle="modal" style="float:left;margin:15px;"> Write Review </a>
                                <?php else: ?>
                                <a class="btn" href=" <?php echo tgt_get_permalink('register') ?> " style="float:left;margin:15px;"> Write Review </a>
                                <?php endif;?>

                                <?php if ( class_exists('CrowdsuniteRatingAPI') ):?>
                                <a class="btn" href="#ratingAPI" data-toggle="modal" style="float:left; margin:15px;">Rating Widget</a>

                                <div id="ratingAPI" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        <h3 id="myModalLabel">Rating Widget</h3>
                                    </div>
                                    <div class="modal-body">
                                        <div class="box_reply" style="margin-top:0; width: 96%;">
                                            <div class="box_reply" style="margin-top:0;width: 43%;">
                                                <form id="ratingAPIOptions">
                                                    <p style="font-weight: bold;">Select Option</p>
                                                    Rating type:
                                                    <div style="padding: 0 0 10px 10px;">
                                                        <input type="radio" name="rating_types" checked value="0">Overall Rating<br>
                                                        <input type="radio" name="rating_types" value="1">Customer Support<br>
                                                        <input type="radio" name="rating_types" value="2">Funding from Strangers<br>
                                                        <input type="radio" name="rating_types" value="3">Ease of Use
                                                    </div>
                                                    <input type="checkbox" name="display_text_rating" value="4">Display the digit  value of the rating<br>
                                                    <input type="checkbox" name="display_text_reviews" value="8">Display the reviews amount
                                                </form>
                                            </div>
                                            <div class="box_reply code_script" style="margin:0 0 0 10px;width:46%; overflow-y: auto;height: 80px">
                                                <p style="font-weight: bold;">You need to include this anywhere on the page only once, even if showing multiple ratings:(<a id="copy-button-script" data-clipboard-text="">copy</a>)<p>
                                                <code style="white-space: pre;">
                                                </code>
                                            </div>
                                            <div class="box_reply code" style="margin:0 0 0 10px;width:46%; overflow-y: auto;height: 81px">
                                                <p style="font-weight: bold;">Paste this code where you want the rating to be displayed:(<a id="copy-button" data-clipboard-text="">copy</a>)<p>
                                                <code style="white-space: pre;">
                                                </code>
                                            </div>
                                            <div class="box_reply preview" style="margin-top:20px;">
                                            </div>
                                        </div>
                                    </div>
                                    <?php $ZeroClipboardSwfUlr = CrowdsuniteRatingAPI::ZeroClipboard();?>
                                    <script type="text/javascript">
                                    jQuery(document).ready(function($){
                                        var clip_script = new ZeroClipboard(document.getElementById("copy-button-script"), { moviePath: "<?php echo $ZeroClipboardSwfUlr;?>" });
                                        clip_script.on("load", function(client) {
                                            client.on("complete", function(client, args) {
                                                alert("Copy complete");
                                            });
                                        });
                                        var clip = new ZeroClipboard(document.getElementById("copy-button"), { moviePath: "<?php echo $ZeroClipboardSwfUlr;?>" });
                                        clip.on("load", function(client) {
                                            client.on("complete", function(client, args) {
                                                alert("Copy complete");
                                            });
                                        });
                                        $('#ratingAPIOptions input').on('change',function(){
                                            var option = 0;
                                            $('#ratingAPIOptions input:checked').each(function(){
                                                option = option + parseInt($(this).val());
                                            });
                                            var url = '<?php bloginfo('url')?>/wp-admin/admin-ajax.php?action=apiRating&api_key=<?php echo urlencode( base64_encode( get_current_user_id() ) );?>&m=1'
                                            var codeApi_script = '\<script type="text/javascript" src="'+url+'">\<\/script>';
                                            var codeApi =  '<span class="crowdsunite_rating_product" data-pid="<?php the_id()?>" data-option="'+option+'"></span>';

                                            $('#ratingAPI .box_reply.code_script code').text(codeApi_script);
                                            $('#copy-button-script').data('clipboard-text',codeApi_script);
                                            $('#copy-button-script').attr('data-clipboard-text',codeApi_script);

                                            $('#ratingAPI .box_reply.code code').text(codeApi);
                                            $('#copy-button').data('clipboard-text',codeApi);
                                            $('#copy-button').attr('data-clipboard-text',codeApi);

                                            var script=document.createElement('script');
                                            script.type='text/javascript';
                                            script.src=url+'&d=1';
                                            $('#ratingAPI .box_reply.preview').html('Preview:<br/>'+codeApi);
                                            $('#ratingAPI .box_reply.preview').append(script);
                                        });
                                        $('a[href=#ratingAPI]').on('click',function(){
                                            $('#ratingAPIOptions input').first().trigger('change');
                                        });
                                    });
                                    </script>
                                </div>
                                <?php endif;?>

                                <div class="vote_star" style="float:right;">
                                    <div class="star">
                                        <p class="date">
                                            <span>
                                                <a href="#tc_review"><?php
                                                        if ($count == 0)
                                                            echo __('0 Review', 're');
                                                        else if ($count == 1)
                                                            echo __('1 Review', 're');
                                                        else
                                                            echo sprintf(__(' %s Reviews', 're'), $count);

                                                        tgt_display_rating($product['rating']['user']['rating'], 'product[rating][user]');
                                                ?></a>
                                            </span>
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <script type="text/javascript">
                                jQuery('.star-disabled').rating();
                            </script>

                            <!-- Display rating of product -->
                            <div itemprop="aggregateRating" itemscope="itemscope" itemtype="http://schema.org/AggregateRating">
                                <meta itemprop="reviewCount" content="<?php echo $count ?>" />

                                <?php $rating = get_post_meta( $post->ID, get_showing_rating(), true ); ?>
                                <meta itemprop="ratingValue" content="<?php echo $rating ?>" />
                                <meta itemprop="bestRating" content="10"/>
                                <meta itemprop="worstRating" content = "0"/>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="content">
                    <?php if ( current_user_can('administrator') ):?>
                    <div class="row-fluid">
                        <div class="span5">
                            <h3>Platform moderators:</h3>
                            <select data-placeholder="Choose a Admin..." id="chosen-select" multiple>
                            <?php foreach ($all_users as $admin_user):?>
                                <option value="<?php echo $admin_user->ID;?>" <?php selected( in_array( $admin_user->ID, $admin_user_id ) )?>><?php echo $admin_user->display_name;?></option>
                            <?php endforeach;?>
                            </select>
                        </div>
                    </div>
                    <?php endif; ?>
                    <div class="row-fluid">
                        <div class="span5 general">
                            <style type="text/css">
                                #slider1{
                                    width: 350px;
                                    height:370px;
                                }
                                .fancybox_img, .box_video{
                                    display: block;
                                }
                                .box_video iframe{
                                    width: 100%;
                                }
                            </style>
                            <ul id="slider1"><!-- AnythingSlider #1 -->
                            <?php if ( !empty( $snapshots_id ) ):?>
                            <?php foreach($snapshots_id as $snapshot ):?>
                                <?php if ( $snapshot['type'] == 'img' && $ulr = wp_get_attachment_url( $snapshot['attach'] ) ):?>
                                <li><a class="fancybox_img" href="<?php echo $ulr;?>"><img src="<?php echo $ulr;?>"/></a></li>
                                <?php elseif ( $snapshot['type'] == 'video' ):?>
                                <li><a class="box_video"><?php echo $wp_embed->run_shortcode('[embed]'.$snapshot['attach'].'[/embed]');?></a></li>
                                <?php endif;?>
                            <?php endforeach;?>
                            <?php else:?>
                            <li><?php echo $helper->image('no_image.jpg', 'No Image');?></li>
                            <?php endif;?>
                            </ul>  <!-- END AnythingSlider #1 -->
                            <script>
                                jQuery(document).ready(function($){
                                    $('#slider1').anythingSlider({
                                        theme : 'metallic',
                                        resizeContents: false,
                                        buildNavigation: false,
                                        buildStartStop: false
                                    });
                                    $(".fancybox_img").fancybox();
                                });
                            </script>
                            <div id="product_intro">
                                <?php
                                setup_postdata($post);
                                $more = 0;
                                the_content("<p><strong>" . __('More info', 're') . "</strong></p>");
                                ?>
                            </div>
                            <div id="product_desc" style="display: none">
                                <?php
                                $more = 1;
                                the_content();
                                ?>
                                <p>
                                    <a class="hide-desc" href="#product"><strong><?php _e('Hide description', 're') ?></strong></a>
                                </p>
                            </div>
                        </div>

                        <div class="span5">
                            <div>
                                <?php include dirname( __file__ ).'/pages/product_spec.php'; ?>
                                <div class="content_text" style="margin-top:25px;"></div>
                            </div>
                        </div>
                        <div class="span2 general" style="border-left-style:dotted; color:grey; border-width:1px;">
                            <p style="font-size:20px; margin-bottom: 1em;text-align: center;">Campaign Help</p></a>
                            <a href="http://crowdsunite.com/crowdfunding-campaign-promotion/" style="text-decoration: none;">
                            <div class="btn" class="sideButtons" style="margin-left: 1em;">
                                    <p style="font-size:20px;text-align: center;">Promotions</p>
                                <p style="text-align:left;">Campaign Building, Reach more people, Media Contacts</p>
                            </div>
                            </a>
                            <br><br>
                            <a href="http://crowdsunite.com/crowdfunding-videographers/" style="text-decoration: none;">
                            <div class="btn" class="sideButtons" style="margin-left: 1em;">
                                    <p style="font-size:20px;text-align: center;">Video</p>
                                <p style="text-align: left;">Make an amazing video</p>
                            </div>
                            </a>
                            <br><br>
                            <a href="http://crowdsunite.com/crowdfunding-lawyers/" style="text-decoration: none;">
                            <div class="btn" class="sideButtons" style="margin-left: 1em;">
                                    <p style="font-size:20px; text-align: center;">Lawyers</p>
                                <p style="text-align: left;">Legal help, security questions, IP, Patent </p>
                            </div>
                            </a>
                            <br><br>
                            <div style="margin-left: 1em;">
                                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                                <!-- Platform profile ad -->
                                <ins class="adsbygoogle"
                                     style="display:inline-block;width:125px;height:125px"
                                     data-ad-client="ca-pub-0609704365388972"
                                     data-ad-slot="1890731210"></ins>
                                <script>
                                    (adsbygoogle = window.adsbygoogle || []).push({});
                                </script>
                            </div>
                        </div>


                    </div>
                </div>

                <div class="clear"></div>

                <div class="content">
                    <div class="tab">
                        <div class="tab_link">
                            <!--ul class="nav nav-tabs"-->
                            <ul>
                                <!--li id="tab_spec" class="tab-item"><a href="#tc_spec"><?php _e('Specifications','re'); ?></a></li-->
                                <li id="tab_review" class="tab-item select"><a href="#tc_review"> 
                                	<!-- Review Tab -->
                                	<?php 
										if ($post->comment_count == 0)
											echo __('Review', 're');
										else
											echo sprintf(__(' Reviews (%s)', 're'), $post->comment_count); 
									?> </a></li>
									<!-- Review Tab End -->

                                <!--li id="tab_write" class="tab-item"><a href="#tc_write"><?php _e('Write review','re'); ?> </a></li-->

                                <?php if (get_option(SETTING_ENABLE_ARTICLE)): ?>
                                <li id="tab_articles" class="tab-item"><a href="#tc_article">
									<!-- Articles Tap  -->
									<?php 
									$tips = new WP_Query( );
									
									add_action('posts_where', 'where_article_of_product');
									add_action('posts_join', 'join_article_of_product' );
									add_action('post_limits', 'article_of_product_limits' );
									add_action('posts_orderby', 'order_article_of_product');
									
									$tips->get_posts();
									
									if( $tips->have_posts() ) 
									{
										while ( $tips->have_posts() ) 
										{
											$article_num = $article_num + 1;
											$tips->the_post();
											//echo 'article_num';
										};
									}
									
									if ( $article_num == 0 )
									{
										echo __(' Articles', 're');
									}else
										echo sprintf(__(' Articles (%s)', 're'), $article_num );
									
									//restore to previous post
									wp_reset_postdata(); 
									?>
									<!-- Article Tab End -->
									</a></li>
                                <?php endif; ?>
                                

                                <?php
                                    $result = get_tab_data_tgt();
                                    if (!empty($result)) {
                                        foreach ($result as $k_g => $v_g) {
                                            $content = '';
                                            if (!empty($tab_content['tab_' . $v_g['ID']])) {
                                ?>
                                <li id="additional_tab_<?php echo $v_g['ID']; ?>" class="tab-item additional-tab">
                                    <a href="#tc_addition_<?php echo $v_g['ID']; ?>">
                                        <?php echo $v_g['name']; ?>
                                    </a>
                                </li>
                                <?php
                                            }
                                        }
                                    }
                                ?>
                            </ul>
                        </div>
                    </div>
                    <div class="clear"></div>
                    <div id="tc_review" class="review_tab_content" style="border: medium none;">
                        <?php add_filter('comments_template','__return_false',100);?>
                        <?php comments_template('/pages/product_reviews.php', true );?>
                        <?php remove_filter('comments_template','__return_false',100);?>
                    </div>

                    <?php if ($open_comment == true): ?>
                    <div class="review_tab_content" id="tc_comment" style="display:none;">
                        <?php comments_template('/pages/product_writecomment.php', true); ?>
                    </div>
                    <?php endif; ?>

                    <?php if (get_option(SETTING_ENABLE_ARTICLE)): ?>
                    <div class="review_tab_content" id="tc_article" style="display:none;">
                        <?php include PATH_PAGES . DS . 'product_articles.php' ?>
                    </div>
                    <?php endif; ?>

                    <?php include PATH_PAGES . DS . 'product_writereview.php' ?>

                    <?php
                        if (!empty($result)) {
                            foreach ($result as $k_g => $v_g) {
                                $content = get_post_meta($post->ID, 'tgt_tab', true);
                                $content = $tab_content;

                                if (!empty($content)) {
                                    $content = $content['tab_' . $v_g['ID']];
                                    if (!empty($content)) {
                    ?>
                    <div class="review_tab_content" id="tc_addition_<?php echo $v_g['ID']; ?>"
                         style="border: medium none;display:none; padding: 10px 0 0 5px;">
                        <div class="content_review">
                            <div class="revieww post_content" style="padding-bottom: 20px;">
                                <?php echo '<p>' . preg_replace('/<br\s*\/>/', '</p><p>', $content) . '</p>'; ?>
                            </div>
                        </div>
                    </div>
                    <?php
                                    }
                                }
                            }
                        }
                    ?>

                </div>
            </div>
        </div>
    </div>
</div>
<style type="text/css">
.content .chosen-container.chosen-container-multi ul li span{
    line-height:1;
}
</style>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $( "#chosen-select" ).chosen({width:"100%"});
        var timeoutId;
        $('#chosen-select').on('change',function(){
            clearTimeout(timeoutId)
            timeoutId = setTimeout(function(){
                $.ajax({
                    url: reviewAjax.ajaxurl,
                    type: 'POST',
                    data: {
                        action: 'set_admin_platform',
                        platform_id: '<?php echo $platform_id;?>',
                        users_id: jQuery('#chosen-select').val()
                    }
                });
            }, 1000)

        });
    });

    jQuery(document).ready(function () {
        // open comment if has #comment-* in url
        pattern = /^#comment-/;
        if (pattern.test(window.location.hash))
            jQuery('#tab_comment > a').trigger('click');

        jQuery('.more-link').click(function () {
            //var target = jQuery(this).attr('href');
            jQuery('#product_intro').hide();
            jQuery('#product_desc').show();
            return false;
        });
        jQuery('.hide-desc').click(function () {
            //var target = jQuery(this).attr('href');
            jQuery('#product_desc').hide();
            jQuery('#product_intro').show();
            return false;
        });

        //
        jQuery('.star-disabled').rating();

        if (jQuery('.no-review').length != 0) {
            jQuery('.review_tab_content').hide();
            jQuery('.tab-item').removeClass('select');
            jQuery('.additional-tab').removeClass('select');
            jQuery('#tab_articles').addClass('select');
            jQuery('#tc_article').show();
        }

        jQuery('.product-small-thumbnail').click(function () {
            var current = jQuery(this),
                id = current.attr('href'),
                target = jQuery(id),
                big_photos = jQuery('.product-big-photo'),
                selected = jQuery('.big-photos .selected');

            selected.fadeOut(500, function () {
                selected.removeClass('selected');
                target.fadeIn(500, function () {
                    jQuery(this).addClass('selected')
                });
            });
            return false;
        });

        jQuery('#thumb_slide_list').each(function () {
            var current = jQuery(this),
                container = jQuery('.slide-wrapper'),
                elements = current.find('.thumb-item'),
                next = container.find('#slide_next'),
                prev = container.find('#slide_prev'),
                nav = container.find('.slide-nav'),
                width = 100,
                count = elements.length;
            slide_count = 3;

            if (count > 3) {
                next.click(function () {
                    current.stop(true, true);
                    current.animate({left: '-=100'}, 'normal', function () {
                        var elements = container.find('.thumb-item'),
                            first = elements.first(),
                            left = current.offset().left;
                        current.append(first);
                        current.offset({left: left + 100});
                    });
                    return false;
                })

                prev.click(function () {
                    var elements = container.find('.thumb-item'),
                        last = elements.last(),
                        left = current.offset().left;
                    current.prepend(last);
                    current.offset({left: left - 100});
                    current.stop(true, true);
                    current.animate({left: '+=100'}, 'normal', function () {
                    });
                    return false;
                })
            }
            else {
                nav.hide();
            }
        });
        var images_url = '<?php echo get_bloginfo('template_url') . '/images/' ?>';
        var lightbox_args = {
            imageLoading: images_url + 'lightbox-ico-loading.gif',
            imageBtnClose: images_url + 'lightbox-btn-close.gif',
            imageBtnPrev: images_url + 'lightbox-btn-prev.gif',
            imageBtnNext: images_url + 'lightbox-btn-next.gif',
            imageBlank: images_url + 'lightbox-blank.gif'
        }

        jQuery('a.product-big-photo').lightBox(lightbox_args);

    });


    function tgt_show_image(id) {
        var count_images = <?php if(!empty($product_images)) echo count($product_images); else echo 0; ?>;
        for (var i = 0; i < count_images; i++) {
            document.getElementById('big_image_' + i).style.display = "none";
        }
        document.getElementById('big_image_' + id).style.display = '';
    }

    jQuery('#artTitle').keyup(function(){
        jQuery('#artTitle_count').html( 70 - countCharaters(jQuery('#artTitle').val()));
    });
</script>
<?php
	get_footer();
?>
<?php
function count_view_product($post_id)
{
    $cookie_views = array();
    if( isset( $_COOKIE['view_product'] ) )
        $cookie_views = $_COOKIE['view_product'];
    else
        $cookie_views = '';
    $count = 1;
    if (!empty($cookie_views)) {
        $cookie_views = explode(',', $cookie_views);
        foreach ($cookie_views as $cookie_view) {
            if ($cookie_view == $post_id) {
                $count = 0;
            }
        }
    }
    if ($count == 1) {
        $cookie_views[] = $post_id;
        $cookie_views = implode(',', $cookie_views);
        setcookie("view_product", $cookie_views, time() + 2 * 3600, "/");
    }
    $view_post = 0;
    $view_post = get_post_meta($post_id, PRODUCT_VIEW_COUNT, true);
    $view_post += $count;
    update_post_meta($post_id, PRODUCT_VIEW_COUNT, $view_post);
}
?>